﻿CREATE TABLE t_features
(
  id serial NOT NULL,
  name character varying,
  CONSTRAINT t_features_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE t_items
(
  id serial NOT NULL,
  name character varying,
  feature_id bigint,
  cz_print_name character varying,
  en_print_name character varying,
  CONSTRAINT items_pkey PRIMARY KEY (id),
  CONSTRAINT t_items_feature_id_fkey FOREIGN KEY (feature_id)
      REFERENCES t_features (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE t_rules
(
  id serial NOT NULL,
  support real,
  confidence real,
  cs_hmean real,
  left_side bigint,
  right_side bigint,
  CONSTRAINT rules_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);



INSERT INTO t_features(id, name) VALUES (1, 'insolvency_state');
INSERT INTO t_features(id, name) VALUES (2, 'subject_type');
INSERT INTO t_features(id, name) VALUES (3, 'title');
INSERT INTO t_features(id, name) VALUES (4, 'gender');
INSERT INTO t_features(id, name) VALUES (5, 'age_group');
INSERT INTO t_features(id, name) VALUES (6, 'region');
INSERT INTO t_features(id, name) VALUES (7, 'creditors');

INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (1,'unresolved',1,'Nevyřízená', 'Unresolved');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (2,'discharge',1,'Oddlužení', 'Discharge');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (3,'bankruptcy_order',1,'Konkurs', 'Bankruptcy Order');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (4,'discharge_after_bankruptcy_order',1,'Oddlužení po konkursu', 'Discharge After;Bankruptcy Order');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (5,'bankruptcy_order_after_discharge',1,'Konkurs po oddlužení', 'Bankruptcy Order;After Discharge');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (6,'finished',1,'Vyřízená', 'Resolved');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (7,'revived',1,'Obživlá', 'Revived');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (13,'unresolved2',1,'Nevyřízená - 2', 'Unresolved - 2');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (14,'natural_person',2,'Fyzická osoba', 'Natural Person');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (15,'entrepreneur',2,'Podnikatel', 'Entrepreneur');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (16,'title',3,'Vysokoškolský titul', 'Title');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (17,'notitle',3,'Bez vysokoškolského titulu', 'No Title');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (18,'male',4,'Muž', 'Male');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (19,'female',4,'Žena', 'Female');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (20,'under30',5,'Do 30', 'Under 30');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (21,'31To40',5,'31 až 40', 'Between 31 and 40');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (22,'41To50',5,'41 až 50', 'Between 41 and 50');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (23,'51AndMore',5,'51 a víc', 'over 51');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (24,'Jihočeský kraj',6,'Jihočeský', 'Jihočeský');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (25,'Karlovarský kraj',6,'Karlovarský', 'Karlovarský');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (26,'Kraj Vysočina',6,'Vysočina', 'Vysočina');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (27,'Královéhradecký kraj',6,'Královéhradecký', 'Královéhradecký');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (28,'Liberecký kraj',6,'Liberecký', 'Liberecký');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (29,'Moravskoslezský kraj',6,'Moravskoslezský', 'Moravskoslezský');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (30,'Olomoucký kraj',6,'Olomoucký', 'Olomoucký');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (31,'Pardubický kraj',6,'Pardubický', 'Pardubický');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (32,'Plzeňský kraj',6,'Plzeňský', 'Plzeňský');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (33,'Praha',6,'Praha', 'Praha');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (34,'Středočeský kraj',6,'Středočeský', 'Středočeský');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (35,'Ústecký kraj',6,'Ústecký', 'Ústecký');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (36,'Zlínský kraj',6,'Zlínský', 'Zlínský');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (37,'Jihomoravský kraj',6,'Jihomoravský', 'Jihomoravský');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (38,'end',1,'Konec', 'End');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (41,'gemoneybank',7,'GE MONEY BANK a.s.','GE MONEY BANK a.s.');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (42,'českáspořitelna',7,'Česká spořitelna,a.s.','Česká spořitelna,a.s.');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (43,'cetelemčr',7,'CETELEM ČR, a.s.','CETELEM ČR, a.s.');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (45,'essox',7,'ESSOX s.r.o.','ESSOX s.r.o.');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (46,'telefónicaczechrepublic',7,'Telefónica Czech Republic,a.s.','Telefónica Czech Republic,a.s.');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (47,'providentfinancial',7,'Provident Financial, s.r.o.','Provident Financial, s.r.o.');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (48,'t-mobileczechrepublic',7,'T-Mobile Czech Republic, a.s.','T-Mobile Czech Republic, a.s.');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (49,'proficreditczech',7,'PROFI CREDIT Czech, a.s.','PROFI CREDIT Czech, a.s.');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (50,'všeobecnázdravotnípojišťovnačeskérepubliky',7,'VŠEOBECNÁ ZDRAVOTNÍ POJIŠŤOVNA ČESKÉ REPUBLIKY','VŠEOBECNÁ ZDRAVOTNÍ POJIŠŤOVNA ČESKÉ REPUBLIKY');
INSERT INTO t_items(id, name, feature_id, cz_print_name, en_print_name) VALUES (51,'cofidis',7,'COFIDIS s.r.o.','COFIDIS s.r.o.');
