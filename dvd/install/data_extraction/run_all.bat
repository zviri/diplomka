set ISIR_PROD_DB_HOST=localhost
set ISIR_PROD_DB_PORT=5432
set ISIR_PROD_DB_USER=postgres
set ISIR_PROD_DB_PASS=postgres

set IPREDICTOR_DB_HOST=localhost
set IPREDICTOR_DB_PORT=5432
set IPREDICTOR_DB_USER=postgres
set IPREDICTOR_DB_PASS=postgres

set OUT_DIR=.\data
if exist %OUT_DIR% rmdir /S /Q %OUT_DIR%
mkdir %OUT_DIR%
mkdir %OUT_DIR%\weka
mkdir %OUT_DIR%\spmf
mkdir %OUT_DIR%\gephi

echo "Extracting creditors !"
python creditors2arff.py --db_host %ISIR_PROD_DB_HOST% ^
                         --db_port %ISIR_PROD_DB_PORT% ^
                         --db_user %ISIR_PROD_DB_USER% ^
                         --db_password %ISIR_PROD_DB_PASS% ^
                         --output %OUT_DIR%\weka\creditors.arff

echo "Extracting modelB to arff!"
python modelB2arff.py --db_host %ISIR_PROD_DB_HOST% ^
                      --db_port %ISIR_PROD_DB_PORT% ^
                      --db_user %ISIR_PROD_DB_USER% ^
                      --db_password %ISIR_PROD_DB_PASS% ^
                      --output %OUT_DIR%\weka\modelB.arff

echo "Extracting modelC to arff!"
python modelC2arff.py --db_host %ISIR_PROD_DB_HOST% ^
                      --db_port %ISIR_PROD_DB_PORT% ^
                      --db_user %ISIR_PROD_DB_USER% ^
                      --db_password %ISIR_PROD_DB_PASS% ^
                      --output %OUT_DIR%\weka\modelC.arff
                      
echo "Extracting modelB to spmf!"
python modelB2spmf.py --db_host %ISIR_PROD_DB_HOST% ^
                      --db_port %ISIR_PROD_DB_PORT% ^
                      --db_user %ISIR_PROD_DB_USER% ^
                      --db_password %ISIR_PROD_DB_PASS% ^
                      --output %OUT_DIR%\spmf\modelB.spmf ^
                      --id2nameFile %OUT_DIR%\spmf\modelB_item2name

echo "Extracting modelD to spmf!"
python modelD2spmf.py --db_host %ISIR_PROD_DB_HOST% ^
                      --db_port %ISIR_PROD_DB_PORT% ^
                      --db_user %ISIR_PROD_DB_USER% ^
                      --db_password %ISIR_PROD_DB_PASS% ^
                      --ipredictor_db_host %IPREDICTOR_DB_HOST% ^
                      --ipredictor_db_port %IPREDICTOR_DB_PORT% ^
                      --ipredictor_db_user %IPREDICTOR_DB_USER% ^
                      --ipredictor_db_password %IPREDICTOR_DB_PASS% ^
                      --output %OUT_DIR%\spmf\modelD.spmf ^
                      --id2nameFile %OUT_DIR%\spmf\modelD_item2name

echo "Extracting networkA for gephi!"
python networkA2gephi.py --db_host %ISIR_PROD_DB_HOST% ^
                         --db_port %ISIR_PROD_DB_PORT% ^
                         --db_user %ISIR_PROD_DB_USER% ^
                         --db_password %ISIR_PROD_DB_PASS% ^
                         --outputNodes %OUT_DIR%/gephi/networkA_nodes.tsv ^
                         --outputEdges %OUT_DIR%/gephi/networkA_edges.tsv 