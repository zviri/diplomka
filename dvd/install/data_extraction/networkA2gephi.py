# -*- coding: utf-8 -*-
import codecs
import isirutil as iu
import argparse
import datetime as dt
import psycopg2
import psycopg2.extras
from sets import Set
                                                                 
parser = argparse.ArgumentParser(description='Exports data for building network A from the database..', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--outputNodes', help="Output file containing network's nodes.", default = "networkA_nodes.tsv", nargs="?")
parser.add_argument('--outputEdges', help="Output file containing network's edges.", default = "networkA_edges.tsv", nargs="?")
iu.add_db_parms(parser);
args = parser.parse_args()

legal_form_removal_regexp='(sro|as|ks|spol|spolsro|družstvo|sp|splikv|vos|ops|soukromáspolečnostsručenímomezeným)$'
char_removal_regexp='[,."-&+\s]*'

conn = iu.connect_to_isir_prod_db(args)
cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

print "Extracting nodes..."
print "==================="
cur.execute("SELECT regexp_replace(regexp_replace(lower(creditor), %s, '', 'g'), %s, '', 'g') as creditor_id, max(creditor) as creditor_name, count(*) as c  \
             FROM file_tab ft \
             WHERE creditor is not null \
             GROUP BY creditor_id \
             ORDER BY c DESC \
             LIMIT 1000", (char_removal_regexp, legal_form_removal_regexp))
creditors = cur.fetchall()

cur.execute("SELECT at.id, at.administrator \
                      FROM administrators_tab at \
                      JOIN insolvencies_administrators_tab iat ON iat.administrator_id=at.id \
                      JOIN (SELECT it.id, it.debtor_name \
                             FROM insolvency_tab it \
                                 JOIN file_tab ft2 ON it.id=ft2.insolvency_id \
                                 JOIN (SELECT regexp_replace(regexp_replace(lower(ft.creditor), %s, '', 'g'), %s, '', 'g') as creditor_id, count(*) as c \
                                      FROM file_tab ft \
                                      WHERE ft.creditor is not null \
                                      GROUP BY creditor_id \
                                      ORDER BY c DESC \
                                      LIMIT 1000) as ct ON regexp_replace(regexp_replace(lower(ft2.creditor), %s, '', 'g'), %s, '', 'g') = ct.creditor_id \
                            WHERE it.proposal_timestamp > '2011-10-01 00:00:00' \
                                  AND ft2.creditor is not null \
                                  AND it.ico is null \
                                  AND it.region_id IN (12) \
                            GROUP BY it.id, it.debtor_name) as its \
                      ON iat.insolvency_id=its.id \
                      GROUP BY at.id, at.administrator", (char_removal_regexp, legal_form_removal_regexp, char_removal_regexp, legal_form_removal_regexp))
administrators = cur.fetchall()

cur.execute("SELECT it.id, it.debtor_name \
                FROM insolvency_tab it \
                     JOIN file_tab ft2 ON it.id=ft2.insolvency_id \
                     JOIN (SELECT regexp_replace(regexp_replace(lower(ft.creditor), %s, '', 'g'), %s, '', 'g') as creditor_id, count(*) as c \
                          FROM file_tab ft \
                          WHERE ft.creditor is not null \
                          GROUP BY creditor_id \
                          ORDER BY c DESC \
                          LIMIT 1000) as ct ON regexp_replace(regexp_replace(lower(ft2.creditor), %s, '', 'g'), %s, '', 'g') = ct.creditor_id \
                WHERE it.proposal_timestamp > '2011-10-01 00:00:00' \
                      AND ft2.creditor is not null \
                      AND it.ico is null \
                      AND it.region_id IN (12) \
                GROUP BY it.id, it.debtor_name", (char_removal_regexp, legal_form_removal_regexp, char_removal_regexp, legal_form_removal_regexp))
insolvencies = cur.fetchall()

with codecs.open(args.outputNodes, "w", encoding="utf-8") as nodes_file:
    print >> nodes_file, "id\ttype\tname"
    for creditor in creditors:
        print >> nodes_file, unicode("%s\t%s\t%s" % (creditor['creditor_id'], "creditor", creditor['creditor_name']), 'utf-8')
    for administrator in administrators:
        print >> nodes_file, unicode("%s\t%s\t%s" % (administrator['id'], "administrator", administrator['administrator']), 'utf-8')
    for insolvency in insolvencies:
        print >> nodes_file, unicode("%s\t%s\t%s" % (insolvency['id'], "debtor", insolvency['debtor_name']), 'utf-8')

print "Extracting edges..."
print "==================="

cur.execute("SELECT * FROM insolvencies_administrators_tab")
administrator_debtor_edges = cur.fetchall()

cur.execute("SELECT insolvency_id, regexp_replace(regexp_replace(lower(creditor), %s, '', 'g'), %s, '', 'g') as creditor_id \
                             FROM file_tab WHERE creditor is not null", (char_removal_regexp, legal_form_removal_regexp))
creditor_debtor_edges = cur.fetchall()

cur.execute("SELECT it1.id as insolvency_id1, it2.id as insolvency_id2 FROM insolvency_tab it1 JOIN insolvency_tab it2 ON it1.debtor_address = it2.debtor_address \
             WHERE it1.debtor_address is not null  \
                   AND it2.debtor_address is not null \
                   AND it1.debtor_name <> it2.debtor_name \
                   AND it1.region_id IN (12) \
                   AND it2.region_id IN (12) \
                   AND it1.ico is null \
                   AND it2.ico is null \
                   AND it1.id <> it2.id")
addresses = cur.fetchall()

node_ids = Set()
for creditor in creditors:
    node_ids.add(creditor['creditor_id'])
for administrator in administrators:
    node_ids.add(administrator['id'])
for insolvency in insolvencies:
    node_ids.add(insolvency['id'])
    
with codecs.open(args.outputEdges, "w", encoding="utf-8") as edges_file:
    print >> edges_file, "Source\tTarget\trelation_type"
    for administrator_debtor in administrator_debtor_edges:
        if administrator_debtor['administrator_id'] in node_ids and administrator_debtor['insolvency_id'] in node_ids:
            print >> edges_file, unicode("%s\t%s\tadministers" % (administrator_debtor['administrator_id'], administrator_debtor['insolvency_id']), 'utf-8')
    for creditor_debtor in creditor_debtor_edges:
        if creditor_debtor['creditor_id'] in node_ids and creditor_debtor['insolvency_id'] in node_ids:
            print >> edges_file, unicode("%s\t%s\towes" % (creditor_debtor['insolvency_id'], creditor_debtor['creditor_id']), 'utf-8')
    for address in addresses:
        if address['insolvency_id1'] in node_ids and address['insolvency_id2'] in node_ids:
            print >> edges_file, unicode("%s\t%s\tshare_address" % (address['insolvency_id1'], address['insolvency_id2']), 'utf-8')
            
print "Extraction of network A FINISHED!"     

   