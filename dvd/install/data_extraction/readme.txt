This folder contains scripts used for extracting data from the database in a 
format that can be imported to the appropriate tool.
We use the following three file types and tools:
* Tool: Weka, Files: .arff
* Tool: SPMF  Files: .spmf
* Tool: Weka, Files: .tsv

The help regarding each script can be obtained by calling it with -h parameter. 
Eg.: python creditors2arff.py -h

Each script contains the following 4 parameters specifying the locations of the isir_prod_db database:
--db_host, isir_prod_db hostname, default: localhost
--db_port, isir_prod_db port, default: 5432
--db_user, isir_prod_db isir_prod_db user name, default: postgres
--db_password, isir_prod_db password, default: postgres

If you are using the default PostgreSQL configuration, you can use the scripts with the default parameters.

!!! For information about how to use the extracted files see the User Manual Appendix in the thesis. !!!

*********************
* run_all(.sh|.bat) *
*********************
The included run_all script will run each data extraction script and store the extracted files to the folder ./data.
It assumes the default PostgreSQL server's configuration, if  you are using different ones, you will need to specify them by editing the script.

***********
* SCRIPTS *
***********

creditors2arff.py
  Extracts the participations of the 100 most frequent creditors in the insolvency proceedings in .arff format for association rules generation in Weka.

modelB2arff.py
  Extracts the insolvency proceedings transitions of model B in .arff format for using in Weka's Bayes Net Editor. 
  (Bayess network definition can be found in /data/weka/modelB.xml)
  
modelC2arff.py
  Extracts the insolvency proceedings transitions with time spent information of model C in .arff format for using in Weka's Bayes Net Editor.
  (Bayess network definition can be found in /data/weka/modelC.xml)

modelB2spmf.py
  Extracts the insolvency proceedings transitions of model B in .spmf format for using with SPMF.
  
modelD2spmf.py
  Extracts the insolvency proceedings transitions and details of model D in .spmf format for using with SPMF.
  
id2name.py
  The scripts which export data for SPMF also export a id2name mapping file, which is used for recovering the item names from their ids (required by SPMF)
  This script replaces the ids in the SPMF output file containing the generated rules by their names.
  
modelD2IPredictor.py
  Imports the generated rules for model D to the ipredictor database.
  
networkA2gephi.py
  Extracts the data for building network A in Gephi. The file outputs two .tsv files nodes.tsv and edges.tsv.