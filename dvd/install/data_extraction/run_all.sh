#!/bin/bash

ISIR_PROD_DB_HOST=localhost
ISIR_PROD_DB_PORT=5432
ISIR_PROD_DB_USER=postgres
ISIR_PROD_DB_PASS=postgres

IPREDICTOR_DB_HOST=localhost
IPREDICTOR_DB_PORT=5432
IPREDICTOR_DB_USER=postgres
IPREDICTOR_DB_PASS=postgres

OUT_DIR=./data
rm -rf $OUT_DIR
mkdir $OUT_DIR
mkdir $OUT_DIR/weka
mkdir $OUT_DIR/spmf
mkdir $OUT_DIR/gephi

echo "Extracting creditors to arff!"
python creditors2arff.py --db_host $ISIR_PROD_DB_HOST \
                         --db_port $ISIR_PROD_DB_PORT \
                         --db_user $ISIR_PROD_DB_USER \
                         --db_password $ISIR_PROD_DB_PASS \
                         --output $OUT_DIR/weka/creditors.arff

echo "Extracting model B to arff!"
python modelB2arff.py --db_host $ISIR_PROD_DB_HOST \
                      --db_port $ISIR_PROD_DB_PORT \
                      --db_user $ISIR_PROD_DB_USER \
                      --db_password $ISIR_PROD_DB_PASS \
                      --output $OUT_DIR/weka/modelB.arff

echo "Extracting model C to arff!"
python modelC2arff.py --db_host $ISIR_PROD_DB_HOST \
                      --db_port $ISIR_PROD_DB_PORT \
                      --db_user $ISIR_PROD_DB_USER \
                      --db_password $ISIR_PROD_DB_PASS \
                      --output $OUT_DIR/weka/modelC.arff

echo "Extracting model B to spmf!"
python modelB2spmf.py --db_host $ISIR_PROD_DB_HOST \
                      --db_port $ISIR_PROD_DB_PORT \
                      --db_user $ISIR_PROD_DB_USER \
                      --db_password $ISIR_PROD_DB_PASS \
                      --output $OUT_DIR/spmf/modelB.spmf \
                      --id2nameFile $OUT_DIR/spmf/modelB_item2name

echo "Extracting model D to spmf!"
python modelD2spmf.py --db_host $ISIR_PROD_DB_HOST \
                      --db_port $ISIR_PROD_DB_PORT \
                      --db_user $ISIR_PROD_DB_USER \
                      --db_password $ISIR_PROD_DB_PASS \
                      --ipredictor_db_host $IPREDICTOR_DB_HOST \
                      --ipredictor_db_port $IPREDICTOR_DB_PORT \
                      --ipredictor_db_user $IPREDICTOR_DB_USER \
                      --ipredictor_db_password $IPREDICTOR_DB_PASS \
                      --output $OUT_DIR/spmf/modelD.spmf \
                      --id2nameFile $OUT_DIR/spmf/modelD_item2name

echo "Extracting network A for gephi!"
python networkA2gephi.py --db_host $ISIR_PROD_DB_HOST \
                         --db_port $ISIR_PROD_DB_PORT \
                         --db_user $ISIR_PROD_DB_USER \
                         --db_password $ISIR_PROD_DB_PASS \
                         --outputNodes $OUT_DIR/gephi/networkA_nodes.tsv \
                         --outputEdges $OUT_DIR/gephi/networkA_edges.tsv 