# -*- coding: utf-8 -*-
import codecs
import isirutil as iu
import argparse
                                                                 
parser = argparse.ArgumentParser(description='Exports data for modelB from the database to .spmf format.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--output', help="Output file name.", default = "modelB.spmf", nargs="?")
parser.add_argument('--id2nameFile', help="File containing the mapping of item ids to item names", default = "modelB_item2name", nargs="?")
iu.add_db_parms(parser);
args = parser.parse_args()

conn = iu.connect_to_isir_prod_db(args)
cur = conn.cursor()

nodes = ["Unre", "BanOr", "Disch", "DiABO", "BOADi", "Finish", "End", "Revived", \
         "Unre2", "Finish2", "BanOr2", "Disch2", "DiABO2", "BOADi2"]
print "Generating output file..."
print "========================="
with codecs.open(args.output, "w", encoding="utf-8") as output:
    cur.execute("SELECT distinct(insolvency_id) FROM insolvency_state_tab")
    insolvency_ids = [insolvency_id[0] for insolvency_id in cur.fetchall()]
    for i, insolvency_id in enumerate(insolvency_ids):
        if i % 1000 == 0:
            print "Processing insolvency nr. %d" % i
        cur.execute("SELECT state FROM insolvency_state_tab WHERE insolvency_id=%s ORDER BY state_change_timestamp, action_id ASC", (insolvency_id,))   
        states = [int(state[0]) for state in cur.fetchall()]
        transaction = []
        for state in states:        
            
            revived = nodes.index("Revived") in transaction
            unresolved2 = nodes.index("Unre2") in transaction
            
            if (state == 12 and revived) or (state == 1 and unresolved2):
                break
            
            # first passing of insolvency
            if not revived and not unresolved2 and state == 1:
                transaction.append(nodes.index("Unre"))
            if not revived and not unresolved2 and state == 4 and not nodes.index("Disch") in transaction and not nodes.index("BanOr") in transaction:
                transaction.append(nodes.index("BanOr"))
            if not revived and not unresolved2 and state == 5 and not nodes.index("BanOr") in transaction and not nodes.index("Disch") in transaction:
                transaction.append(nodes.index("Disch"))
            if not revived and not unresolved2 and state == 5 and nodes.index("BanOr") in transaction:
                transaction.append(nodes.index("DiABO"))
            if not revived and not unresolved2 and state == 4 and nodes.index("Disch") in transaction:
                transaction.append(nodes.index("BOADi"))
            if not revived and not unresolved2 and not nodes.index("Finish") in transaction and state in [7, 8, 9]:
                transaction.append(nodes.index("Finish"))
            
            # revival of insolvency
            if state == 12:
                transaction.append(nodes.index("Revived"))           
            if revived and state == 4 and not nodes.index("Disch2") in transaction and not nodes.index("BanOr2") in transaction:
                transaction.append(nodes.index("BanOr2")) 
            if revived and state == 5 and not nodes.index("BanOr2") in transaction and not nodes.index("Disch2") in transaction:
                transaction.append(nodes.index("Disch2"))
            if revived and nodes.index("BanOr2") in transaction and state == 5:
                transaction.append(nodes.index("DiABO2"))
            if revived and nodes.index("Disch2") in transaction and state == 4:
                transaction.append(nodes.index("BOADi2"))
            if revived and state in [7, 8, 9] and not nodes.index("Finish2") in transaction:
                transaction.append(nodes.index("Finish2"))
                
            # restarting the insolvency proceeding
            if state == 1 and nodes.index("Finish") in transaction:
                transaction.append(nodes.index("Unre2"))     
            if unresolved2 and state == 4 and not nodes.index("Disch2") in transaction and not nodes.index("BanOr2") in transaction:
                transaction.append(nodes.index("BanOr2"))
            if unresolved2 and state == 5 and not nodes.index("BanOr2") in transaction and not nodes.index("Disch2") in transaction:
                transaction.append(nodes.index("Disch2"))
            if unresolved2 and nodes.index("BanOr2") in transaction and state == 5:
                transaction.append(nodes.index("DiABO2"))
            if unresolved2 and nodes.index("Disch2") in transaction and state == 4:
                transaction.append(nodes.index("BOADi2"))
            if unresolved2 and state in [7, 8, 9] and not nodes.index("Finish2") in transaction:
                transaction.append(nodes.index("Finish2"))
        
        if not nodes.index("Revived") in transaction and not nodes.index("Unre2") in transaction and nodes.index("Finish") in transaction:
            transaction.append(nodes.index("End"))
        
        transaction.sort()
        line = ""
        for i, b in enumerate(transaction):
            if i != 0:
                line +=" "
            line += str(b)
        print >> output, line    
        output.flush()
        
print "Generating mapping file..."
print "=========================="
with codecs.open(args.id2nameFile, "w", encoding="utf-8") as mapping:
    for node in nodes:
        print >> mapping, node

conn.close()
cur.close()        
print "Extraction of modelB FINISHED!"