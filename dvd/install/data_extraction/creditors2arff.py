# -*- coding: utf-8 -*-
import codecs
import isirutil as iu
import argparse
                                                                 
parser = argparse.ArgumentParser(description='Extracts creditors participation in insolvencies to .arff format.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--output', help="Output file name.", default = "creditors.arff", nargs="?")
iu.add_db_parms(parser);
args = parser.parse_args()

legal_form_removal_regexp='(sro|as|ks|spol|spolsro|družstvo|sp|splikv|vos|ops|soukromáspolečnostsručenímomezeným)$'
char_removal_regexp='[,."-&+\s]*'
conn = iu.connect_to_isir_prod_db(args)
cur = conn.cursor()

cur.execute("SELECT s.normalized_name, max(s.f_creditor), count(*) as count \
                FROM (SELECT max(creditor) as f_creditor, regexp_replace(regexp_replace(lower(creditor), %s, '', 'g'), %s, '', 'g') as normalized_name \
                      FROM file_tab WHERE creditor is not null \
                      GROUP BY normalized_name, insolvency_id) as s \
                GROUP BY s.normalized_name ORDER BY count DESC LIMIT 100", (char_removal_regexp, legal_form_removal_regexp))
creditor_ids = [creditor[0] for creditor in cur.fetchall()]

cur.execute("SELECT string_agg(regexp_replace(regexp_replace(lower(creditor), %s, '', 'g'), %s, '', 'g'), ';') \
                              FROM file_tab WHERE creditor is not null GROUP BY insolvency_id", (char_removal_regexp, legal_form_removal_regexp))
insolvencies = cur.fetchall()
    
with codecs.open(args.output, 'w', 'utf-8') as transactions_file:   
    print >> transactions_file, "@relation 'creditors'\n"
        
    for creditor in creditor_ids:
        print >> transactions_file, ("@attribute '%s' {1}" % unicode(creditor, 'utf-8'))
    print >> transactions_file,'\n@data'
    
    for i,insolvency in enumerate(insolvencies):
        if (i % 1000 == 0):
            print "Processing insolvency nr. %d" % i
        row = ["?"] * len(creditor_ids)
        insolvency_creditors = insolvency[0].split(';')
        for ic in insolvency_creditors:
            if ic in creditor_ids:
                row[creditor_ids.index(ic)] = "1"
        if not "1" in row:
            continue
        print >> transactions_file, ','.join(row)
        transactions_file.flush()
    print "Export of creditors FINISHED!"
cur.close()
conn.close()