#!/bin/bash

DATE_FROM="01/01/2012"
DATE_TO="01/07/2012"
# approximately denotes events started on 01/01/2012
MIN_ACTION_ID=3200000
MAX_ACTION_ID=4500000

#run HTML scraper first
cd html-scraper-0.0.1
./start.sh --dateFrom $DATE_FROM --dateTo $DATE_TO
cd ../

cd ws-cache-scraper-0.0.1
# scrape all events to cache
./start.sh --taskName scrapeToCache --actionStart $MIN_ACTION_ID --actionEnd $MAX_ACTION_ID --intervalBetweenDownloads 10000

# extract data from cached events
./start.sh --taskName runScrapers --startDate $DATE_FROM










