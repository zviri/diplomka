set DATE_FROM="01/01/2012"
set DATE_TO="01/07/2012"
REM  approximately denotes events started on 01/01/2012
set MIN_ACTION_ID=3200000
set MAX_ACTION_ID=4500000

REM run HTML scraper first
cd html-scraper-0.0.1
call .\start.bat --dateFrom %DATE_FROM% --dateTo %DATE_TO%
cd ..\

cd ws-cache-scraper-0.0.1
REM scrape all events to cache
call .\start.bat --taskName scrapeToCache --actionStart %MIN_ACTION_ID% --actionEnd %MAX_ACTION_ID% --intervalBetweenDownloads 10000

REM extract data from cached events (in month intervals to prevent large memory usage)
call .\start.bat --taskName runScrapers --startDate %DATE_FROM%