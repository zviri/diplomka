This directory contains both html-scraper and ws-cache-scraper.
They can be started with the included start(.sh|.bat) scripts.

To get information about their parameters use --help.
Eg.: ./start.sh --help

The scrapers assume default PostgreSQL configuration running on localhost.
If your PostgreSQL is not running on localhost or is using different configuration,
edit the database configuration in the config folder of each scraper.

*********************
* run_all(.sh|.bat) *
*********************

The included run_all(.sh|.bat) runs all scrapers on the insolvency proceedings started between 01/01/2012 and 01/07/2012.
The scraping should take few hours to finish.
