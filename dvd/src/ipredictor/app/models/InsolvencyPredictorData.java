package models;

import java.util.ArrayList;
import java.util.List;

public class InsolvencyPredictorData {
	private static final String NOTSELECTED = "notselected";
	
	public List<String> left_insolvency_state;
	public List<String> right_insolvency_state;
	public List<String> creditors;
	public String subject_type;
	public String title;
	public String gender;
	public String age_group;
	public String region;
	
	public List<String> getRuleLeftSide() {
		ArrayList<String> ruleLeftSide = new ArrayList<>();
		if (left_insolvency_state != null) {
			ruleLeftSide.addAll(left_insolvency_state);
		}
		if (creditors != null) {
			ruleLeftSide.addAll(creditors);
		}
		if (subject_type != null && subject_type != NOTSELECTED) {
			ruleLeftSide.add(subject_type);
		}
		if (title != null && title != NOTSELECTED) {
			ruleLeftSide.add(title);
		}
		if (gender != null && gender != NOTSELECTED) {
			ruleLeftSide.add(gender);
		}
		if (age_group != null && age_group != NOTSELECTED) {
			ruleLeftSide.add(age_group);
		}
		if (region != null && region != NOTSELECTED) {
			ruleLeftSide.add(region);
		}
		return ruleLeftSide;
	}
	
	public boolean isEmpty() {
		return getRuleLeftSide().isEmpty();
	}
}
