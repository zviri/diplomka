package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "t_features")
public class Feature extends Model {

	@Id
	public Long id;

	public String name;

	public static Finder<Long, Feature> find = new Finder<>(Long.class, Feature.class);
}
