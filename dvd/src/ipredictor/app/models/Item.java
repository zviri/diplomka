package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "t_items")
public class Item extends Model {

	@Id
	public Long id;

	public String name;

	public Long featureId;

	public String czPrintName;
	
	public String enPrintName;

	public static Finder<Long, Item> find = new Finder<>(Long.class, Item.class);
}
