package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "t_items_rules")
public class ItemRule extends Model {

	public Long itemId;

	public Long ruleId;

	public Boolean leftSide;

	public static Finder<Long, ItemRule> find = new Finder<>(Long.class,
			ItemRule.class);
}
