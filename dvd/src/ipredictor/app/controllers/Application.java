package controllers;

import static play.data.Form.form;
import static util.MyCollectionUtils.toMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import models.Feature;
import models.InsolvencyPredictorData;
import models.Item;
import models.Rule;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import scala.Tuple2;
import scala.collection.Seq;
import util.MyCollectionUtils;
import views.html.badbrowser;
import views.html.index;
import views.html.main;
import views.html.helper.options;

import com.avaje.ebean.Expr;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

public class Application extends Controller {

	private static String EMAIL = "U2FsdGVkX18+xQrosHJO9aBFsKTicAhF9GDB3Fn3B5Z9+InEDg5Tu7rbnujK2xXK";

	private static Form<InsolvencyPredictorData> predictionForm = form(InsolvencyPredictorData.class);

	private static final String DF = "%1$,.2f";

	public static Result index() throws Exception {
		if (request().headers().get("User-Agent")[0].toLowerCase().contains("msie")) {
			return ok(main.render(Messages.get("title"), badbrowser.render(), EMAIL));
		}
		InsolvencyPredictorData insolvencyPredictorFormData = predictionForm.bindFromRequest().get();
		HashMap<String, String> predictions = new HashMap<String, String>();
		List<List<String>> additionalRules = new ArrayList<>();
		if (!insolvencyPredictorFormData.isEmpty()) {
			ImmutableMap<String, Item> name2Item = Maps.uniqueIndex(Item.find.all(), new Function<Item, String>() {
				@Override
				public String apply(Item arg) {
					return arg.name;
				}
			});

			ArrayList<Long> leftRuleItemIds = new ArrayList<>();
			for (String itemName : insolvencyPredictorFormData.getRuleLeftSide()) {
				leftRuleItemIds.add(name2Item.get(itemName).id);
			}
			Collections.sort(leftRuleItemIds);
			long ruleLeft = 0;
			for (Long id : leftRuleItemIds) {
				ruleLeft |= pow2(id);
			}

			Map<String, Double> confidences = new HashMap<>();
			double confidenceSum = 0;
			for (String itemName : insolvencyPredictorFormData.right_insolvency_state) {
				long ruleRight = pow2(name2Item.get(itemName).id);
				List<Rule> rules = Rule.find.where().and(Expr.eq("left_side", ruleLeft), Expr.eq("right_side", ruleRight)).findList();
				if (rules.size() == 1) {
					Rule rule = rules.get(0);
					confidenceSum += rule.confidence;
					confidences.put(itemName, rule.confidence);
					predictions.put(itemName, Messages.get("support") + ": "
									+ String.format(DF, ((rule.support / 79825) * 100)) + "");
				} else {
					predictions.put(itemName, Messages.get("too_small"));
				}
			}

			for (String itemName : confidences.keySet()) {
				String label = predictions.get(itemName);
				label = Messages.get("probability") + ": " + String.format(DF, (confidences.get(itemName) / confidenceSum) * 100) + ";" + label;
				predictions.put(itemName, label);
			}
		}

		List<Feature> features = Feature.find.all();
		Map<String, Seq<Tuple2<String, String>>> formOptions = new LinkedHashMap<>();
		for (Feature feature : features) {
			List<Item> items = Item.find.where().eq("feature_id", feature.id).findList();
			Map<String, String> currentOptions = new LinkedHashMap<>();
			if (!feature.name.equals("insolvency_state") && !feature.name.equals("creditors")) {
				currentOptions.put("notselected", Messages.get("notselected"));
			}
			currentOptions.putAll(toMap(items, new MyCollectionUtils.KeyValueFunction<Item, String, String>() {
				@Override
				public String key(Item arg) {
					return arg.name;
				}

				@Override
				public String value(Item arg) {
					return "cs".equals(lang().code()) ? arg.czPrintName : arg.enPrintName;
				}
			}));
			formOptions.put(feature.name, options.apply(currentOptions));
		}

		return ok(main.render(Messages.get("title"),
				index.render(predictionForm.fill(insolvencyPredictorFormData), formOptions, predictions, additionalRules), EMAIL));
	}

	private static long pow2(Long num) {
		return Math.round(Math.pow(2, num));
	}

	public static Result changeLang_(String lang) {
		changeLang(lang);
		return redirect("/ipredictor");
	}
}
