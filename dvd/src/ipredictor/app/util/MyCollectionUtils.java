package util;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MyCollectionUtils {

	public static interface KeyValueFunction<K, V, T> {
		public V key(K arg);

		public T value(K arg);
	}

	public static <K, V, T> Map<V, T> toMap(List<K> list, KeyValueFunction<K, V, T> function) {
		Map<V, T> map = new LinkedHashMap<>();
		for (K element : list) {
			V key = function.key(element);
			T value = function.value(element);
			map.put(key, value);
		}
		return map;
	}
}
