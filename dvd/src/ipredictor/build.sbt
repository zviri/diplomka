name := "ipredictor"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  "postgresql" % "postgresql" % "9.1-901.jdbc4",
  "com.google.guava" % "guava" % "12.0",
  "org.apache.solr" % "solr-solrj" % "4.7.0"
)     

play.Project.playJavaSettings
