# -*- coding: utf-8 -*-
import codecs
import argparse
import psycopg2
import psycopg2.extras
import re

                                                                 
parser = argparse.ArgumentParser(description='Imports the rules generated for model D by SPMF to the ipredictor database.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('rules_file', help="File containing rules generated from modelD")
parser.add_argument('mapping_file', help="File containing mapping of item ids to item names in the rules file.")
parser.add_argument('--ipredictor_db_host', help="PostgreSQL(ipredictor) server's hostname.", default = "localhost", nargs="?")
parser.add_argument('--ipredictor_db_port', help="PostgreSQL(ipredictor) database port.", default = "5432", nargs="?")
parser.add_argument('--ipredictor_db_user', help="PostgreSQL(ipredictor) database username.", default = "postgres", nargs="?")
parser.add_argument('--ipredictor_db_password', help="IPredictor(ipredictor) database password.", default = "postgres", nargs="?")
args = parser.parse_args()

ipredictor_conn = psycopg2.connect(database="ipredictor", 
                                            user=args.ipredictor_db_user,
                                            password=args.ipredictor_db_password, 
                                            port=int(args.ipredictor_db_port), 
                                            host=args.ipredictor_db_host)  
ipredictor_cur = ipredictor_conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

nodes = []
with codecs.open(args.mapping_file, 'r', encoding="utf-8") as mapping_file:
    for node in mapping_file:
        nodes.append(node.strip())
        
rules_path=args.rules_file
ipredictor_cur.execute("SELECT * FROM t_items")
items = ipredictor_cur.fetchall()
        
name2id = {}
for item in items:
    name2id[item["name"]] = item["id"]
rules_file = open(rules_path, 'r')    
for i, line in enumerate(rules_file):
    line = line.strip()
    matches = re.search('^(([0-9]+ )+)==> (([0-9]+ )+)#SUP: ([0-9]+) #CONF: ([0-9,.]+)', line)
    rule_left_side = matches.group(1).strip().split(' ')
    rule_right_side = matches.group(3).strip().split(' ')
    support = float(matches.group(5).strip().replace(",","."))
    confidence = float(matches.group(6).strip().replace(",","."))   
    left_side_ids = [name2id[nodes[int(n)]] for n in rule_left_side]
    left_side = 0
    for l in left_side_ids:
        left_side = left_side | (2 ** l)
    right_side_ids = [name2id[nodes[int(n)]] for n in rule_right_side]
    right_side = 0
    for l in right_side_ids:
        right_side = right_side | (2 ** l)
    cs_hmean = (2 * confidence * support) / (confidence + support)
    ipredictor_cur.execute("INSERT INTO t_rules(support, confidence, left_side, right_side, cs_hmean) VALUES(%s, %s, %s, %s, %s)",
                (support, confidence, left_side, right_side, cs_hmean))
    if i % 10000 == 0:
        ipredictor_conn.commit()
ipredictor_conn.commit()
ipredictor_conn.close()
ipredictor_cur.close()        
print "Import of rules to IPredictor FINISHED!"     

   