# -*- coding: utf-8 -*-
import codecs
import isirutil as iu
import argparse

def get_bucket_num(val, buckets):
    for i, bucket in enumerate(buckets):
        if val <= bucket:
            return i + 1
    return len(buckets) + 1

def contains_state(name, bn):
    for i, item in enumerate(bn):        
        if nodes[i].startswith(name) and item == yes:            
            return True
    return False    
                                                                 
parser = argparse.ArgumentParser(description='Exports data for modelC from the database to .arff format.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--output', help="Output file name.", default="modelC.arff", nargs="?")
iu.add_db_parms(parser);
args = parser.parse_args()

nodes = ["Start", "Unres1", "Unres2", "Unres3", "BanOr1", "Disch1", "DiABO1", "BOADi1", "Finish1", "Revive1", \
         "BanOr2", "BanOr3", "Disch2", "Disch3", "DiABO2", "DiABO3", "BOADi2", \
         "BOADi3", "Finish2", "Finish3", "Revive2", "Revive3"]

conn = iu.connect_to_isir_prod_db(args)
cur = conn.cursor()

yes = "yes"
no = "no"

valid_transitions = [1, 4, 5, 7, 8, 9, 12]

cur.execute("SELECT distinct(insolvency_id) FROM insolvency_state_tab WHERE state IN (7, 8, 9)") 
insolvency_ids = [insolvency_id[0] for insolvency_id in cur.fetchall()]
transitions = {}
print "Calculating buckets for each state..."
print "====================================="
for i, insolvency_id in enumerate(insolvency_ids):
    if i % 1000 == 0:
        print "Processing insolvency nr. %d" % i
    cur.execute("SELECT state,state_change_timestamp FROM insolvency_state_tab WHERE insolvency_id=%s ORDER BY state_change_timestamp, action_id ASC", (insolvency_id,))
    states = cur.fetchall()       
    last_state = None
    for state in states:
        if last_state:                  
            if state[0] not in valid_transitions:
                continue                
            last_state_id = last_state[0]
            if state[0] in [7, 8, 9] and last_state_id in [7, 8, 9]:
                continue
            if last_state_id in [7, 8, 9]:
                last_state_id = 7
            if not last_state_id in transitions.keys():
                transitions[last_state_id] = []
            days = (state[1] - last_state[1]).days
            transitions[last_state_id].append(days)
        last_state = state
        
num_buckets = 3
for transition in transitions.keys():
    values = transitions[transition]
    values.sort()    
    buckets = []
    for i in range(1, num_buckets):
        buckets.append(values[(len(values) / num_buckets) * i])
    transitions[transition] = buckets
    print "%r, buckets: %r" % (transition, buckets)

print "Exporting final modelC..."
print "========================="

with codecs.open(args.output, 'w', encoding="utf-8") as output:
    print >> output, "@relation 'insolvency proceedings modelC data'"
    print >> output, ""
    for node in nodes:
        print >> output, ("@attribute %s {yes,no}" % node)
    print >> output, ""
    print >> output, "@data"
        
    for i, insolvency_id in enumerate(insolvency_ids):        
        if i % 1000 == 0:
            print "Processing insolvency nr. %d" % i
        cur.execute("SELECT state,state_change_timestamp FROM insolvency_state_tab WHERE insolvency_id=%s ORDER BY state_change_timestamp, action_id ASC", (insolvency_id,))
        states = cur.fetchall()   
        last_state = None
        bn = [no] * len(nodes)
        bn[nodes.index("Start")] = yes
        for state in states:
            if last_state:
                days = (state[1] - last_state[1]).days
                if state[0] not in valid_transitions:
                    continue
                last_state_id = last_state[0]
                if last_state_id in [7, 8, 9]:
                    last_state_id = 7
                buckets = transitions[last_state_id]
                bucket = get_bucket_num(days, buckets)
                if last_state_id == 1 and not contains_state("Unres", bn):
                    bn[nodes.index("Unres%d" % bucket)] = yes
                if last_state_id == 4 and not contains_state("BanOr", bn) and not contains_state("Disch", bn):
                    bn[nodes.index("BanOr%d" % bucket)] = yes
                if last_state_id == 5 and not contains_state("BanOr", bn) and not contains_state("Disch", bn):
                    bn[nodes.index("Disch%d" % bucket)] = yes        
                if last_state_id == 5 and contains_state("BanOr", bn) and not contains_state("DiABO", bn):
                    bn[nodes.index("DiABO%d" % bucket)] = yes        
                if last_state_id == 4 and contains_state("Disch", bn) and not contains_state("BOADi", bn):
                    bn[nodes.index("BOADi%d" % bucket)] = yes        
                if last_state_id in [7, 8, 9] and not contains_state("Finish", bn):
                    bn[nodes.index("Finish%d" % bucket)] = yes        
                if last_state_id == 12 and not contains_state("Revive", bn):
                    bn[nodes.index("Revive%d" % bucket)] = yes        
            last_state = state
        line = ""
        for i, b in enumerate(bn):
            if i != 0:
                line += ","
            line += b
        print >> output, line    
        output.flush()
        
print "Exporting of modelB FINISHED!"
cur.close()
conn.close()