# -*- coding: utf-8 -*-
import codecs
import isirutil as iu
import argparse
import datetime as dt
import psycopg2
import psycopg2.extras
                                                                 
parser = argparse.ArgumentParser(description='Exports data for modelD from the database to SPMF format.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--output', help="Output file name.", default = "modelD.spmf", nargs="?")
parser.add_argument('--id2nameFile', help="File containing the mapping of item ids to item names", default = "modelD_item2name", nargs="?")
parser.add_argument('--ipredictor_db_host', help="PostgreSQL(ipredictor) server's hostname.", default = "localhost", nargs="?")
parser.add_argument('--ipredictor_db_port', help="PostgreSQL(ipredictor) database port.", default = "5432", nargs="?")
parser.add_argument('--ipredictor_db_user', help="PostgreSQL(ipredictor) database username.", default = "postgres", nargs="?")
parser.add_argument('--ipredictor_db_password', help="IPredictor(ipredictor) database password.", default = "postgres", nargs="?")
iu.add_db_parms(parser);
args = parser.parse_args()

legal_form_removal_regexp='(sro|as|ks|spol|spolsro|družstvo|sp|splikv|vos|ops|soukromáspolečnostsručenímomezeným)$'
char_removal_regexp='[,."-&+\s]*'

conn = iu.connect_to_isir_prod_db(args)
cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

ipredictor_conn = psycopg2.connect(database="ipredictor", 
                                            user=args.ipredictor_db_user,
                                            password=args.ipredictor_db_password, 
                                            port=int(args.ipredictor_db_port), 
                                            host=args.ipredictor_db_host)  
ipredictor_cur = ipredictor_conn.cursor(cursor_factory=psycopg2.extras.DictCursor)


ipredictor_cur.execute("SELECT name FROM t_items")
nodes = [n[0] for n in ipredictor_cur.fetchall()]

output_file = args.output

print "Generating output file..."
print "========================="   
with codecs.open(output_file, "w", encoding="utf-8") as output:
    cur.execute("SELECT distinct(id) FROM insolvency_tab WHERE proposal_timestamp > '2011-10-01 00:00:00'")
    insolvency_ids = [insolvency_id[0] for insolvency_id in cur.fetchall()]
    
    for i, insolvency_id in enumerate(insolvency_ids):
        if i % 1000 == 0:
            print "Processing insolvency nr. %d" % i
        
        cur.execute("SELECT *, (trim(debtor_name) ~ '^(Bc\.|BcA\.|Ing\.|MUDr\.|MDDr\.|MVDr\.|MgA\.|Mgr\.|PhDr\.|JUDr\.|RNDr\.|PharmDr\.|ThDr\.|DiS\.|doc\.|prof\.|MBA\.|BPA\.|MPA\.|B\.Th\.|M\.Th\.|B\.A\.|M\.A\.|BSc\.|MSc\.|M\.D\.)') as has_title \
                     FROM insolvency_tab it JOIN regions_tab rt ON it.region_id = rt.id \
                     WHERE it.id = %s", (insolvency_id,));
        insolvency = cur.fetchall()
        if len(insolvency) == 0:
            continue
        
        insolvency = insolvency[0]    
        cur.execute("SELECT state FROM insolvency_state_tab WHERE insolvency_id=%s ORDER BY state_change_timestamp, action_id ASC", (insolvency_id,))           
        states = [int(state[0]) for state in cur.fetchall()]
        transaction = [nodes.index("entrepreneur") if insolvency["ico"] else nodes.index("natural_person"),
                       nodes.index("title") if insolvency["has_title"] else nodes.index("notitle")]
        if insolvency["gender"] == "M":
            transaction.append(nodes.index("male"))
        if insolvency["gender"] == "F":
            transaction.append(nodes.index("female"))
    
        if insolvency["year_of_birth"]:
            age = dt.datetime.now().year - int(insolvency["year_of_birth"])
            if age < 31:
                transaction.append(nodes.index("under30"))
            elif age < 41:
                transaction.append(nodes.index("31To40"))
            elif age < 51:
                transaction.append(nodes.index("41To50"))
            else:
                transaction.append(nodes.index("51AndMore"))
        
        if insolvency["name"]: # region name from the join ...
            transaction.append(nodes.index(insolvency["name"]))                   
        
        insolvency_creditors = cur.execute("SELECT regexp_replace(regexp_replace(lower(creditor), %s, '', 'g'), %s, '', 'g') as normalized_name FROM file_tab WHERE insolvency_id=%s AND creditor is not null", 
                                           (char_removal_regexp, legal_form_removal_regexp, insolvency_id))
        insolvency_creditors = cur.fetchall()
        for creditor in insolvency_creditors:
            if creditor[0] in nodes:
                transaction.append(nodes.index(creditor[0]))
        for state in states:                
            revived = nodes.index("revived") in transaction
            unresolved2 = nodes.index("unresolved2") in transaction
            
            if (state == 12 and revived) or (state == 1 and unresolved2):
                break
            
            # first passing of insolvency
            if not revived and not unresolved2 and state == 1:
                transaction.append(nodes.index("unresolved"))
            if not revived and not unresolved2 and state == 4 and not nodes.index("discharge") in transaction and not nodes.index("bankruptcy_order") in transaction:
                transaction.append(nodes.index("bankruptcy_order"))
            if not revived and not unresolved2 and state == 5 and not nodes.index("bankruptcy_order") in transaction and not nodes.index("discharge") in transaction:
                transaction.append(nodes.index("discharge"))
            if not revived and not unresolved2 and state == 5 and nodes.index("bankruptcy_order") in transaction:
                transaction.append(nodes.index("discharge_after_bankruptcy_order"))
            if not revived and not unresolved2 and state == 4 and nodes.index("discharge") in transaction:
                transaction.append(nodes.index("bankruptcy_order_after_discharge"))
            if not revived and not unresolved2 and not nodes.index("finished") in transaction and state in [7, 8, 9]:
                transaction.append(nodes.index("finished"))
            
            # revival of insolvency
            if state == 12:
                transaction.append(nodes.index("revived"))           
                
            # restarting the insolvency proceeding
            if state == 1 and nodes.index("finished") in transaction:
                transaction.append(nodes.index("unresolved2"))     
        
        if not nodes.index("revived") in transaction and not nodes.index("unresolved2") in transaction and nodes.index("finished") in transaction:
            transaction.append(nodes.index("end"))
        
        transaction.sort()
        line = ""
        for i, b in enumerate(transaction):
            if i != 0:
                line +=" "
            line += str(b)
        print >> output, line    
        output.flush()        
conn.close()
cur.close()
ipredictor_conn.close()
ipredictor_cur.close()        

print "Generating mapping file..."
print "=========================="
with codecs.open(args.id2nameFile, "w", encoding="utf-8") as mapping:
    for node in nodes:
        print >> mapping, unicode(node, "utf-8")
        
print "Extraction of modelC FINISHED!"     

   