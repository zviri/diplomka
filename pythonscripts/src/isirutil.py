import psycopg2

def add_db_parms(parser):
    parser.add_argument('--db_host', help="PostgreSQL(isir_prod_db) server's hostname.", default = "localhost", nargs="?")
    parser.add_argument('--db_port', help="PostgreSQL(isir_prod_db) database port.", default = "5432", nargs="?")
    parser.add_argument('--db_user', help="PostgreSQL(isir_prod_db) database username.", default = "postgres", nargs="?")
    parser.add_argument('--db_password', help="PostgreSQL(isir_prod_db) database password.", default = "postgres", nargs="?")
    
def connect_to_isir_prod_db(args):
    return psycopg2.connect(database="isir_prod_db", 
                            user=args.db_user,
                            password=args.db_password, 
                            port=int(args.db_port), 
                            host=args.db_host)    