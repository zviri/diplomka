# -*- coding: utf-8 -*-
import codecs
import isirutil as iu
import argparse
                                                                 
parser = argparse.ArgumentParser(description='Exports data for modelB from the database to .arff format.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--output', help="Output file name.", default = "modelB.arff", nargs="?")
iu.add_db_parms(parser);
args = parser.parse_args()

output_file = args.output

nodes = ["Unre", "BanOr", "Disch", "DiABO", "BOADi", "Finish", "End", "Revived", \
         "Unre2", "BanOrR", "DischR", "DiABOR", "BOADiR", "FinishR", "Finish2", \
         "BanOr2", "Disch2", "DiABO2", "BOADi2"]

conn = iu.connect_to_isir_prod_db(args)
cur = conn.cursor()

yes = "yes"
no = "no"

with codecs.open(output_file, 'w', encoding="utf-8") as output:
    print >> output, "@relation 'insolvency proceedings modelB data'"
    print >> output, ""
    for node in nodes:
        print >> output, ("@attribute %s {yes,no}" % node)
    print >> output, ""
    print >> output, "@data"
    cur.execute("SELECT distinct(insolvency_id) FROM insolvency_state_tab")
    insolvency_ids = [insolvency_id[0] for insolvency_id in cur.fetchall()]
    for i, insolvency_id in enumerate(insolvency_ids):
        if i % 1000 == 0:
            print "Processing insolvency nr. %d" % i
        cur.execute("SELECT state FROM insolvency_state_tab WHERE insolvency_id=%s ORDER BY state_change_timestamp, action_id ASC", (insolvency_id, ))   
        states = [int(state[0]) for state in cur.fetchall()]
        bn = [no] * len(nodes)
        bn[nodes.index("Unre")] = yes # every insolvency starts as Unresolved
        for state in states:        
            revived = bn[nodes.index("Revived")] == yes
            unresolved2 = bn[nodes.index("Unre2")] == yes
            if (state == 12 and revived) or (state == 1 and unresolved2):
                break
            
            # first passing of insolvency
            if not revived and not unresolved2 and state == 4 and bn[nodes.index("BanOr")] == no and bn[nodes.index("Disch")] == no:
                bn[nodes.index("BanOr")] = yes
            if not revived and not unresolved2 and state == 5 and bn[nodes.index("Disch")] == no and bn[nodes.index("BanOr")] == no:
                bn[nodes.index("Disch")] = yes
            if not revived and not unresolved2 and bn[nodes.index("BanOr")] == yes and state == 5:
                bn[nodes.index("DiABO")] = yes
            if not revived and not unresolved2 and bn[nodes.index("Disch")] == yes and state == 4:
                bn[nodes.index("BOADi")] = yes
            if not revived and not unresolved2 and state in [7, 8, 9] and  bn[nodes.index("Finish")] == no:
                bn[nodes.index("Finish")] = yes
            
            # revival of insolvency
            if state == 12:
                bn[nodes.index("Revived")] = yes                    
            if revived and state == 4 and bn[nodes.index("BanOrR")] == no and bn[nodes.index("DischR")] == no:
                bn[nodes.index("BanOrR")] = yes
            if revived and state == 5 and bn[nodes.index("DischR")] == no and bn[nodes.index("BanOrR")] == no:
                bn[nodes.index("DischR")] = yes
            if revived and bn[nodes.index("BanOrR")] == yes and state == 5:
                bn[nodes.index("DiABOR")] = yes
            if revived and bn[nodes.index("DischR")] == yes and state == 4:
                bn[nodes.index("BOADiR")] = yes
            if revived and state in [7, 8, 9] and bn[nodes.index("FinishR")] == no:
                bn[nodes.index("FinishR")] = yes
                
            # restarting the insolvency proceeding
            if state == 1 and bn[nodes.index("Finish")] == yes:
                bn[nodes.index("Unre2")] = yes       
            if unresolved2 and state == 4 and bn[nodes.index("BanOr2")] == no and bn[nodes.index("Disch2")] == no:
                bn[nodes.index("BanOr2")] = yes
            if unresolved2 and state == 5 and bn[nodes.index("Disch2")] == no and bn[nodes.index("BanOr2")] == no:
                bn[nodes.index("Disch2")] = yes
            if unresolved2 and bn[nodes.index("BanOr2")] == yes and state == 5:
                bn[nodes.index("DiABO2")] = yes
            if unresolved2 and bn[nodes.index("Disch2")] == yes and state == 4:
                bn[nodes.index("BOADi2")] = yes
            if unresolved2 and state in [7, 8, 9] and bn[nodes.index("Finish2")] == no:
                bn[nodes.index("Finish2")] = yes
        
        if bn[nodes.index("Revived")] != yes and bn[nodes.index("Unre2")] != yes and bn[nodes.index("Finish")] == yes:
            bn[6] = yes
    
        line = ""
        for i, b in enumerate(bn):
            if i != 0:
                line +=","
            line += b
        print >> output, line    
        output.flush()
    print "Export of modelB FINISHED!"
cur.close()
conn.close()    