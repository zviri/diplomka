# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table t_items (
  id                        bigint not null,
  name                      varchar(255),
  feature_id                bigint,
  cz_print_name             varchar(255),
  constraint pk_t_items primary key (id))
;

create sequence t_items_seq;




# --- !Downs

drop table if exists t_items cascade;

drop sequence if exists t_items_seq;

