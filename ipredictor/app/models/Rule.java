package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "t_rules")
public class Rule extends Model {

	@Id
	public Long id;

	public Double support;

	public Double confidence;
	
	public Long left_side;
	
	public Long right_side;

	public static Finder<Long, Rule> find = new Finder<>(Long.class, Rule.class);
}
