package forms;

import java.util.List;

public class InsolvencyPredictionForm {
	private String subjectType;
	private String title;
	private String gender;
	private String ageGroup;
	private String region;
	private List<String> creditors;
	private List<String> states;

	public String getSubjectType() {
		return subjectType;
	}

	public void setSubjectType(String subjectType) {
		this.subjectType = subjectType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAgeGroup() {
		return ageGroup;
	}

	public void setAgeGroup(String ageGroup) {
		this.ageGroup = ageGroup;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public List<String> getCreditors() {
		return creditors;
	}

	public void setCreditors(List<String> creditors) {
		this.creditors = creditors;
	}

	public List<String> getStates() {
		return states;
	}

	public void setStates(List<String> states) {
		this.states = states;
	}
}
