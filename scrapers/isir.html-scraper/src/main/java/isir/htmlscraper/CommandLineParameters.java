package isir.htmlscraper;

import com.lexicalscope.jewel.cli.Option;

/**
 * Command line parameters for html-scraper.
 */
public interface CommandLineParameters {

    /**
     * Minimal age of the cached web pages to be still considered valid.
     * @return Max age in days.
     */
    @Option(defaultValue = "-1", description = "What is the max age of cached HTML pages validity (in days). If not set no caching is used!")
    public int getCacheMaxAge();

    /**
     * From which date the scraping should begin.
     * @return Date in format: 23/03/1990
     */
    @Option(defaultToNull = true, description = "From which date the scraping should begin. If not set yesterday's date is used! Format: 23/03/1990", pattern = "^[0-9]{2}/[0-9]{2}/[0-9]{4}$")
    public String getDateFrom();

    /**
     * To which date the scraping should go.
     * @return Date in format: 23/03/1990
     */
    @Option(defaultToNull = true, description = "To which date the scraping should go. If not set today's date is used! Format: 23/03/1990", pattern = "^[0-9]{2}/[0-9]{2}/[0-9]{4}$")
    public String getDateTo();

    /**
     * Help.
     */
    @Option(helpRequest = true, description = "Display help")
    boolean getHelp();
}
