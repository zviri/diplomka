package isir.htmlscraper;

import isir.shared.dataaccess.service.FilesService;
import isir.shared.dataaccess.service.InsolvencyService;
import isir.shared.dataaccess.service.ServiceFactory;
import isir.shared.scraping.CachedHttpDownloader;
import isir.shared.scraping.HttpDownloader;
import isir.shared.scraping.IsirHtmlScraper;
import isir.shared.scraping.IsirHtmlScraperImpl;
import isir.shared.utils.TimeUtils;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lexicalscope.jewel.cli.CliFactory;

/**
 * Class containing the main method and the starting point of the html-scraper.
 */
public class Main {

    private static Logger log = LoggerFactory.getLogger(Main.class);

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public static void main(String[] args) throws Exception {

        CommandLineParameters commandLineParameters = CliFactory.parseArguments(CommandLineParameters.class, args);
        int cacheMaxAgeInDays = commandLineParameters.getCacheMaxAge();
        String dateFromString = commandLineParameters.getDateFrom();
        String dateToString = commandLineParameters.getDateTo();

        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        TimeUtils.resetTime(yesterday);
        Calendar dateTo = Calendar.getInstance();
        Calendar dateFrom = Calendar.getInstance();
        if (dateFromString != null) {
            dateFrom.setTimeInMillis(dateFormat.parse(dateFromString).getTime());
        } else {
            dateFrom = yesterday;
        }
        if (dateToString != null) {
            dateTo.setTimeInMillis(dateFormat.parse(dateToString).getTime());
        } else {
            dateTo = yesterday;
        }

        log.info("********************************** HTML SCRAPER START **************************************");
        log.info("cacheMaxAge: {}", cacheMaxAgeInDays);
        log.info("dateFrom: {}", dateFormat.format(dateFrom.getTime()));
        log.info("dateTo: {}", dateFormat.format(dateTo.getTime()));

        try {
            HttpDownloader httpDownloader = new CachedHttpDownloader("windows-1250", cacheMaxAgeInDays, ServiceFactory.getInstance().getHttpCacheService());
            InsolvencyService insolvencyService = ServiceFactory.getInstance().getInsolvencyService();
            FilesService filesService = ServiceFactory.getInstance().getFilesService();
            IsirHtmlScraper scraper = new IsirHtmlScraperImpl(insolvencyService, filesService, httpDownloader);
            scraper.run(dateFrom, dateTo);
            List<URL> failedUrls = scraper.getFailedUrls();
            if (!failedUrls.isEmpty()) {
                log.warn("Failed URLs:");
            }
            for (URL url : failedUrls) {
                log.warn("Failed URL:{}", url);
            }

        } catch (Exception e) {
            log.error("********************************* HTML Scraper FAILED! ***************************************", e);
            throw e; // For Jenkins to notice failure
        }
        log.info("********************************** HTML SCRAPER FINISHED **************************************");
    }

}
