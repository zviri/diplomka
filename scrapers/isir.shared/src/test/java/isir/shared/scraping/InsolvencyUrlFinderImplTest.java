package isir.shared.scraping;

import com.gargoylesoftware.htmlunit.MockWebConnection;
import com.gargoylesoftware.htmlunit.WebClient;
import isir.shared.dataaccess.dto.Insolvency;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class InsolvencyUrlFinderImplTest {

    public static final String ENCODING = "windows-1250";
    private MockWebConnection mockWebConnection;
    private WebClient webClient;

    @Before
    public void setUp() throws Exception {
        mockWebConnection = new MockWebConnection();
        mockWebConnection.setResponse(new URL("https://isir.justice.cz/isir/common/index.do"), loadFileToString("insolvencySearch.html"), "text/html", ENCODING);
        mockWebConnection.setDefaultResponse("");
        webClient = new WebClient();
        webClient.setJavaScriptEnabled(false);
        webClient.setWebConnection(mockWebConnection);
    }

    @Test
    public void testGetInsolvencyURL1() throws Exception {
        mockWebConnection.setResponse(new URL("https://isir.justice.cz/isir/ueu/vysledek_lustrace.do?nazev_osoby=&vyhledat_pouze_podle_zacatku=on&podpora_vyhledat_pouze_podle_zacatku=true&jmeno_osoby=&ic=&datum_narozeni=&rc=&mesto=&cislo_senatu=MSPH&bc_vec=14713&rocnik=2011&id_osoby_puvodce=&druh_stav_konkursu=&datum_stav_od=&datum_stav_do=&aktualnost=AKTUALNI_I_UKONCENA&druh_kod_udalost=&datum_akce_od=&datum_akce_do=&nazev_osoby_f=&cislo_senatu_vsns=&druh_vec_vsns=&bc_vec_vsns=&rocnik_vsns=&cislo_senatu_icm=&bc_vec_icm=&rocnik_icm=&rowsAtOnce=50&captcha_answer=&spis_znacky_datum=&spis_znacky_obdobi=14DNI"), loadFileToString("insolvencySearchResult.html"), "text/html", ENCODING);
        InsolvencyUrlFinderImpl insolvencyUrlFinder = new InsolvencyUrlFinderImpl(webClient);

        Insolvency insolvency = new Insolvency().setId("msphins14713/2011");
        List<URL> newUrls = insolvencyUrlFinder.getInsolvencyURLs(insolvency.getInsolvencyVec(), insolvency.getInsolvencyRok());
        assertEquals(newUrls.size(), 1);
        assertEquals(newUrls.get(0), new URL("https://isir.justice.cz/isir/ueu/evidence_upadcu_detail.do?id=b3373199-6f69-4a06-a6b5-8c7b281ca5e6"));
    }

    @Test
    public void testGetInsolvencyURL2() throws Exception {
        mockWebConnection.setResponse(new URL("https://isir.justice.cz/isir/ueu/vysledek_lustrace.do?nazev_osoby=&vyhledat_pouze_podle_zacatku=on&podpora_vyhledat_pouze_podle_zacatku=true&jmeno_osoby=&ic=&datum_narozeni=&rc=&mesto=&cislo_senatu=MSPH&bc_vec=1&rocnik=1&id_osoby_puvodce=&druh_stav_konkursu=&datum_stav_od=&datum_stav_do=&aktualnost=AKTUALNI_I_UKONCENA&druh_kod_udalost=&datum_akce_od=&datum_akce_do=&nazev_osoby_f=&cislo_senatu_vsns=&druh_vec_vsns=&bc_vec_vsns=&rocnik_vsns=&cislo_senatu_icm=&bc_vec_icm=&rocnik_icm=&rowsAtOnce=50&captcha_answer=&spis_znacky_datum=&spis_znacky_obdobi=14DNI"), loadFileToString("insolvencySearchEmptyResult.html"), "text/html", ENCODING);
        InsolvencyUrlFinderImpl insolvencyUrlFinder = new InsolvencyUrlFinderImpl(webClient);

        Insolvency insolvency = new Insolvency().setId("msphins1/1");
        List<URL> newUrls = insolvencyUrlFinder.getInsolvencyURLs(insolvency.getInsolvencyVec(), insolvency.getInsolvencyRok());
        assertEquals(newUrls.size(), 0);
    }

    private String loadFileToString(String name) throws IOException {
        return IOUtils.toString(getClass().getResourceAsStream(name), ENCODING);
    }
}
