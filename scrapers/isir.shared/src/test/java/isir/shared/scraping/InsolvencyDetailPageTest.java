package isir.shared.scraping;

import static org.junit.Assert.assertEquals;
import isir.shared.dataaccess.dto.Administrator;
import isir.shared.dataaccess.dto.AdministratorType;
import isir.shared.dataaccess.dto.AttachedFile;
import isir.shared.dataaccess.dto.BirthNumber;
import isir.shared.dataaccess.dto.FileType;
import isir.shared.dataaccess.dto.Insolvency;
import isir.shared.exceptions.InvalidInsolvencyDetailsPageException;
import isir.shared.utils.Pair;
import isir.shared.utils.TimeUtils;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

public class InsolvencyDetailPageTest {

    public static final String ENCODING = "windows-1250";

    private InsolvencyDetailPage detailPage;

    @Before
    public void setUp() throws Exception {
        String page = IOUtils.toString(getClass().getResourceAsStream("insolvencyDetailPageTest.html"), ENCODING);
        detailPage = new InsolvencyDetailPage(page);
    }


    @Test(expected = InvalidInsolvencyDetailsPageException.class)
    public void testCheckInsolvencyPage() throws Exception {
        String page = IOUtils.toString(getClass().getResourceAsStream("nonExistingDetailPage.html"));
        new InsolvencyDetailPage(page);
    }

    @Test
    public void testGetInsolvency() throws Exception {
        Insolvency insolvency = detailPage.getInsolvency();
        Calendar proposalTimestamp = Calendar.getInstance();
        TimeUtils.resetTime(proposalTimestamp);
        proposalTimestamp.set(2011, Calendar.AUGUST, 18, 13, 47, 0);
        BirthNumber birthNumber = new BirthNumber("745903/0326");

        assertEquals(insolvency.getId(), "msphins14713/2011");
        assertEquals(insolvency.getDebtorName(), "Jana Jeřábková");
        assertEquals(insolvency.getProposalTimestamp(), TimeUtils.calendarToTimestamp(proposalTimestamp));
        assertEquals(insolvency.getBirthNumberHashCode(), (Integer) birthNumber.hashCode());
        assertEquals(insolvency.getDebtorAddress(), "Praha 5, Dreyerova 639/11, PSČ 152 00");
        assertEquals(insolvency.getGender(), BirthNumber.Gender.F);
        assertEquals(insolvency.getIco(), "68889291");
        assertEquals(insolvency.getReferenceNumber(), "MSPH 99 INS 14713 / 2011");
        assertEquals(insolvency.getYearOfBirth(), (Integer) birthNumber.getYearOfBirth());

    }

    @Test
    public void testGetAdministrators() throws Exception {
        List<Pair<AdministratorType, Administrator>> administrators = detailPage.getAdministrators();

        assertEquals(administrators.size(), 1);
        Pair<AdministratorType, Administrator> administratorTypeAdministratorPair = administrators.get(0);
        AdministratorType administratorType = administratorTypeAdministratorPair.getKey();
        assertEquals(administratorType.getDescription(), "Insolvenční správce");
        Administrator administrator = administratorTypeAdministratorPair.getValue();
        assertEquals(administrator.getAdministrator(), "JUDr. Václav Kulhavý");
    }

    @Test
    public void testGetAttachedFiles() throws Exception {
        List<Pair<AttachedFile, FileType>> files = detailPage.getAttachedFiles();
        assertEquals(files.size(), 3);

        Calendar proposalTimestamp = Calendar.getInstance();
        TimeUtils.resetTime(proposalTimestamp);
        proposalTimestamp.set(2011, Calendar.AUGUST, 18, 13, 47, 0);
        AttachedFile file1 = files.get(0).getKey();
        assertEquals(file1.getPublishDate(), TimeUtils.calendarToTimestamp(proposalTimestamp));
        assertEquals(file1.getDocumentSection(), InsolvencyDetailPage.DocumentSection.A);
        assertEquals(file1.getInsolvencyId(), "msphins14713/2011");
        assertEquals(file1.getUrl(), new URL("https://isir.justice.cz/isir/doc/dokument.PDF?id=2799953"));
        FileType fileType1 = files.get(0).getValue();
        assertEquals(fileType1.getDescription(), "Insolvenční návrh spojený s návrhem na povolení oddlužení");

        proposalTimestamp.set(2011, Calendar.SEPTEMBER, 30, 13, 13, 0);
        AttachedFile file2 = files.get(1).getKey();
        assertEquals(file2.getPublishDate(), TimeUtils.calendarToTimestamp(proposalTimestamp));
        assertEquals(file2.getDocumentSection(), InsolvencyDetailPage.DocumentSection.B);
        assertEquals(file2.getInsolvencyId(), "msphins14713/2011");
        assertEquals(file2.getUrl(), new URL("https://isir.justice.cz/isir/doc/dokument.PDF?id=2964926"));
        FileType fileType2 = files.get(1).getValue();
        assertEquals(fileType2.getDescription(), "Usnesení o uložení povinnosti");

        proposalTimestamp.set(2011, Calendar.SEPTEMBER, 13, 11, 8, 0);
        AttachedFile file3 = files.get(2).getKey();
        assertEquals(file3.getPublishDate(), TimeUtils.calendarToTimestamp(proposalTimestamp));
        assertEquals(file3.getDocumentSection(), InsolvencyDetailPage.DocumentSection.P);
        assertEquals(file3.getInsolvencyId(), "msphins14713/2011");
        assertEquals(file3.getUrl(), new URL("https://isir.justice.cz/isir/doc/dokument.PDF?id=2897429"));
        assertEquals(file3.getCreditor(), "Euroclaim Praha s.r.o.");
        FileType fileType3 = files.get(2).getValue();
        assertEquals(fileType3.getDescription(), "Přihláška pohledávky");
    }
}
