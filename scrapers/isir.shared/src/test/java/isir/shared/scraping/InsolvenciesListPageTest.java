package isir.shared.scraping;

import isir.shared.dataaccess.dto.BirthNumber;
import isir.shared.dataaccess.dto.Insolvency;
import isir.shared.utils.TimeUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class InsolvenciesListPageTest {

    private InsolvenciesListPage insolvenciesListPage;

    @Before
    public void setUp() throws Exception {
        String page = IOUtils.toString(getClass().getResourceAsStream("insolvencyListPageTest.html"));
        insolvenciesListPage = new InsolvenciesListPage(page);
    }

    @Test
    public void testGetInsolvencies() throws Exception {
        Calendar calendar = Calendar.getInstance();
        TimeUtils.resetTime(calendar);
        calendar.set(2008, Calendar.JANUARY, 2, 12, 30, 0);
        List<Insolvency> insolvencyDetailsUrls = insolvenciesListPage.getInsolvencies(calendar);

        assertEquals(insolvencyDetailsUrls.size(), 1);
        Insolvency insolvency = insolvencyDetailsUrls.get(0);
        assertEquals(insolvency.getId(), "ksosins1/2008");
        assertEquals(insolvency.getUrl(), new URL("https://isir.justice.cz/isir/ueu/evidence_upadcu_detail.do?id=3281c3c7-918f-4296-af8a-cd52fe7a4568"));
        assertEquals(insolvency.getIco(), "25865366");
        BirthNumber birthNumber = new BirthNumber("715802/5523");
        assertEquals(insolvency.getYearOfBirth(), (Integer) birthNumber.getYearOfBirth());
        assertEquals(insolvency.getBirthNumberHashCode(), (Integer) birthNumber.hashCode());
        assertEquals(insolvency.getDebtorName(), "KOTYSGLOBAL s.r.o.");
        assertEquals(insolvency.getProposalTimestamp(), TimeUtils.calendarToTimestamp(calendar));
    }
}
