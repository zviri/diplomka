package isir.shared.scraping;

import com.gargoylesoftware.htmlunit.*;
import isir.shared.dataaccess.dto.HttpWebsite;
import isir.shared.dataaccess.service.HttpCacheService;
import org.apache.commons.lang3.time.StopWatch;
import org.easymock.Capture;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;
import java.util.Calendar;

import static isir.shared.utils.TimeUtils.calendarToTimestamp;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.easymock.EasyMock.*;

public class CachedHttpDownloaderTest {

    private MockWebConnection mockWebConnection;
    private WebClient webClient;

    @Before
    public void setUp() throws Exception {
        mockWebConnection = new MockWebConnection();
        mockWebConnection.setDefaultResponse("");
        webClient = new WebClient();
        webClient.setJavaScriptEnabled(false);
        webClient.setWebConnection(mockWebConnection);
    }

    @Test
    public void testGetHtmlWebsiteNotInCache() throws Exception {
        HttpCacheService cacheService = createMock(HttpCacheService.class);
        CachedHttpDownloader cachedHttpDownloader = new CachedHttpDownloader("utf-8", 10, cacheService, webClient);
        URL url = new URL("http://www.google.sk/");
        String pageContent = "html content";
        mockWebConnection.setResponse(url, pageContent);

        expect(cacheService.getWebsite(url)).andReturn(null);
        Capture<HttpWebsite> httpWebsiteCapture = new Capture<>();
        cacheService.insertWebsite(capture(httpWebsiteCapture));
        replay(cacheService);

        String htmlWebsite = cachedHttpDownloader.getHtmlWebsite(url);

        assertEquals(htmlWebsite, pageContent);
        HttpWebsite insertedHttpWebsite = httpWebsiteCapture.getValue();
        assertEquals(insertedHttpWebsite.getContent(), pageContent);
        assertEquals(insertedHttpWebsite.getUrl(), url);
        Calendar insertedHttpWebsiteDownloadDate = Calendar.getInstance();
        insertedHttpWebsiteDownloadDate.setTimeInMillis(insertedHttpWebsite.getDownloadDate().getTime());
        Calendar today = Calendar.getInstance();
        assertEquals(today.get(Calendar.DAY_OF_YEAR), insertedHttpWebsiteDownloadDate.get(Calendar.DAY_OF_YEAR));
        verify(cacheService);
    }

    @Test
    public void testGetHtmlWebsiteInCacheOld() throws Exception {
        HttpCacheService cacheService = createMock(HttpCacheService.class);
        CachedHttpDownloader cachedHttpDownloader = new CachedHttpDownloader("utf-8", 10, cacheService, webClient);
        URL url = new URL("http://www.google.sk/");
        String pageContent = "html content";
        mockWebConnection.setResponse(url, pageContent);

        Calendar cacheDate = Calendar.getInstance();
        cacheDate.set(1990, Calendar.MARCH, 23);
        HttpWebsite cachedHttpWebsite = new HttpWebsite().setUrl(url)
                                                         .setContent("old content")
                                                         .setDownloadDate(calendarToTimestamp(cacheDate));
        expect(cacheService.getWebsite(url)).andReturn(cachedHttpWebsite);
        Capture<HttpWebsite> httpWebsiteCapture = new Capture<>();
        cacheService.updateWebsite(capture(httpWebsiteCapture));
        replay(cacheService);

        String htmlWebsite = cachedHttpDownloader.getHtmlWebsite(url);

        assertEquals(htmlWebsite, pageContent);
        HttpWebsite insertedHttpWebsite = httpWebsiteCapture.getValue();
        assertEquals(insertedHttpWebsite.getContent(), pageContent);
        assertEquals(insertedHttpWebsite.getUrl(), url);
        Calendar insertedHttpWebsiteDownloadDate = Calendar.getInstance();
        insertedHttpWebsiteDownloadDate.setTimeInMillis(insertedHttpWebsite.getDownloadDate().getTime());
        Calendar today = Calendar.getInstance();
        assertEquals(today.get(Calendar.DAY_OF_YEAR), insertedHttpWebsiteDownloadDate.get(Calendar.DAY_OF_YEAR));
        verify(cacheService);
    }

    @Test
    public void testGetHtmlWebsiteInCache() throws Exception {
        HttpCacheService cacheService = createMock(HttpCacheService.class);
        CachedHttpDownloader cachedHttpDownloader = new CachedHttpDownloader("utf-8", 10, cacheService, webClient);
        URL url = new URL("http://www.google.sk/");
        String pageContent = "html content";
        mockWebConnection.setResponse(url, pageContent);

        Calendar cacheDate = Calendar.getInstance();
        HttpWebsite cachedHttpWebsite = new HttpWebsite().setUrl(url)
                                                         .setContent(pageContent)
                                                         .setDownloadDate(calendarToTimestamp(cacheDate));
        expect(cacheService.getWebsite(url)).andReturn(cachedHttpWebsite);
        replay(cacheService);

        String htmlWebsite = cachedHttpDownloader.getHtmlWebsite(url);

        assertEquals(htmlWebsite, pageContent);
        verify(cacheService);
    }

    @Test
    public void testDelayBetweenDownloads() throws Exception {
        HttpCacheService cacheService = createMock(HttpCacheService.class);
        CachedHttpDownloader cachedHttpDownloader = new CachedHttpDownloader("utf-8", -1, cacheService, webClient);
        URL url = new URL("http://www.google.sk/");
        String pageContent = "html content";
        mockWebConnection.setResponse(url, pageContent);

        StopWatch watch = new StopWatch();
        watch.start();
        cachedHttpDownloader.getHtmlWebsite(url);
        cachedHttpDownloader.getHtmlWebsite(url);
        watch.stop();
        assertTrue(watch.getTime() >= 1000);
    }

    @Test
    public void testWebClientException() throws Exception {
        HttpCacheService cacheService = createMock(HttpCacheService.class);
        WebClient mockClient = createMock(WebClient.class);
        CachedHttpDownloader cachedHttpDownloader = new CachedHttpDownloader("utf-8", -1, cacheService, mockClient);
        cachedHttpDownloader.setDelayAfterFailure(1);
        URL url = new URL("http://www.google.sk/");
        String pageContentExpected = "html content";
        FailingHttpStatusCodeException codeException = createMock(FailingHttpStatusCodeException.class);

        expect(mockClient.getPage(url)).andThrow(codeException);
        expect(codeException.getStatusCode()).andReturn(502);
        Page page = createMock(Page.class);
        expect(mockClient.getPage(url)).andReturn(page);
        WebResponse webResponse = createMock(WebResponse.class);
        expect(page.getWebResponse()).andReturn(webResponse);
        expect(webResponse.getContentAsString("utf-8")).andReturn(pageContentExpected);

        replay(cacheService, mockClient, codeException, page, webResponse);

        String pageContent = cachedHttpDownloader.getHtmlWebsite(url);
        assertEquals(pageContent, pageContentExpected);
    }
}
