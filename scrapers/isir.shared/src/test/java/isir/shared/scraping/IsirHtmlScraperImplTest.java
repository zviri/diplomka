package isir.shared.scraping;

import isir.shared.dataaccess.dto.*;
import isir.shared.dataaccess.service.FilesService;
import isir.shared.dataaccess.service.InsolvencyService;
import isir.shared.utils.TimeUtils;
import org.apache.commons.io.IOUtils;
import org.easymock.Capture;
import org.easymock.IAnswer;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.easymock.EasyMock.*;

public class IsirHtmlScraperImplTest {

    public static final String ENCODING = "utf-8";

    private InsolvencyService insolvencyService;
    private FilesService fileService;
    private HttpDownloader httpDownloader;
    private IsirHtmlScraper isirHtmlScraper;

    @Before
    public void setUp() throws Exception {
        insolvencyService = createMock(InsolvencyService.class);
        fileService = createMock(FilesService.class);
        httpDownloader = createMock(HttpDownloader.class);
        isirHtmlScraper = new IsirHtmlScraperImpl(insolvencyService, fileService, httpDownloader);

    }

    @Test
    public void testRun1() throws Exception {
        final URL url = new URL("http://insolvence.cz");
        expect(httpDownloader.getHtmlWebsite(url)).andReturn(loadFileToString("insolvencyDetailPageOneFileTest.html"));
        Capture<Insolvency> insolvencyCapture = new Capture<>();
        insolvencyService.save(capture(insolvencyCapture));
        Calendar filePublishingDate = Calendar.getInstance();
        TimeUtils.resetTime(filePublishingDate);
        filePublishingDate.set(2011, Calendar.AUGUST, 18, 13, 47, 0);
        Capture<AttachedFile> attachedFileCapture = new Capture<>();
        Capture<FileType> fileTypeCapture = new Capture<>();
        fileService.save(capture(fileTypeCapture));
        expectLastCall().andAnswer(new IAnswer<Object>() {
            @Override
            public Object answer() throws Throwable {
                ((FileType)getCurrentArguments()[0]).setId(1L);
                return null;
            }
        });
        fileService.save(capture(attachedFileCapture));

        Capture<AdministratorType> administratorTypeCapture = new Capture<>();
        Capture<Administrator> administratorCapture = new Capture<>();

        insolvencyService.save(anyObject(Insolvency.class), capture(administratorTypeCapture), capture(administratorCapture));
        replay(fileService, insolvencyService, httpDownloader);

        isirHtmlScraper.run(new ArrayList<URL>() {{ add(url); }});

        verify(fileService, insolvencyService, httpDownloader);
        Insolvency insolvency = insolvencyCapture.getValue();
        Calendar proposalTimestamp = Calendar.getInstance();
        TimeUtils.resetTime(proposalTimestamp);
        proposalTimestamp.set(2011, Calendar.AUGUST, 18, 13, 47, 0);
        BirthNumber birthNumber = new BirthNumber("745903/0326");
        assertEquals(insolvency.getId(), "msphins14713/2011");
        assertEquals(insolvency.getDebtorName(), "Jana Jeřábková");
        assertEquals(insolvency.getProposalTimestamp(), TimeUtils.calendarToTimestamp(proposalTimestamp));
        assertEquals(insolvency.getBirthNumberHashCode(), (Integer) birthNumber.hashCode());
        assertEquals(insolvency.getDebtorAddress(), "Praha 5, Dreyerova 639/11, PSČ 152 00");
        assertEquals(insolvency.getGender(), BirthNumber.Gender.F);
        assertEquals(insolvency.getIco(), "68889291");
        assertEquals(insolvency.getReferenceNumber(), "MSPH 99 INS 14713 / 2011");
        assertEquals(insolvency.getYearOfBirth(), (Integer) birthNumber.getYearOfBirth());
        assertEquals(insolvency.getUrl(), url);

        FileType fileType = fileTypeCapture.getValue();
        AttachedFile attachedFile = attachedFileCapture.getValue();
        assertEquals(attachedFile.getPublishDate(), TimeUtils.calendarToTimestamp(filePublishingDate));
        assertEquals(attachedFile.getDocumentSection(), InsolvencyDetailPage.DocumentSection.A);
        assertEquals(attachedFile.getInsolvencyId(), "msphins14713/2011");
        assertEquals(attachedFile.getUrl(), new URL("https://isir.justice.cz/isir/doc/dokument.PDF?id=2799953"));
        assertEquals(attachedFile.getFileTypeId(), new Long(1));
        assertEquals(fileType.getDescription(), "Insolvenční návrh spojený s návrhem na povolení oddlužení");

        Administrator administrator = administratorCapture.getValue();
        AdministratorType administratorType = administratorTypeCapture.getValue();
        assertEquals(administratorType.getDescription(), "Insolvenční správce");
        assertEquals(administrator.getAdministrator(), "JUDr. Václav Kulhavý");
    }

    @Test
    public void testRun2() throws Exception {
        final URL url = new URL("http://insolvence.cz");
        expect(httpDownloader.getHtmlWebsite(url)).andReturn(loadFileToString("insolvencyDetailPageOneFileTest.html"));
        Capture<Insolvency> insolvencyCapture = new Capture<>();
        insolvencyService.save(capture(insolvencyCapture));
        AttachedFile existingAttachedFile = new AttachedFile();
        Calendar filePublishingDate = Calendar.getInstance();
        TimeUtils.resetTime(filePublishingDate);
        filePublishingDate.set(2011, Calendar.AUGUST, 18, 13, 47, 0);
        Capture<AttachedFile> attachedFileCapture = new Capture<>();
        Capture<FileType> fileTypeCapture = new Capture<>();
        fileService.save(capture(fileTypeCapture));
        expectLastCall().andAnswer(new IAnswer<Object>() {
            @Override
            public Object answer() throws Throwable {
                ((FileType)getCurrentArguments()[0]).setId(1L);
                return null;
            }
        });
        fileService.save(capture(attachedFileCapture));

        Capture<AdministratorType> administratorTypeCapture = new Capture<>();
        Capture<Administrator> administratorCapture = new Capture<>();

        insolvencyService.save(anyObject(Insolvency.class), capture(administratorTypeCapture), capture(administratorCapture));
        replay(fileService, insolvencyService, httpDownloader);

        isirHtmlScraper.run(new ArrayList<URL>() {{ add(url); }});

        verify(fileService, insolvencyService, httpDownloader);
        Insolvency insolvency = insolvencyCapture.getValue();
        Calendar proposalTimestamp = Calendar.getInstance();
        TimeUtils.resetTime(proposalTimestamp);
        proposalTimestamp.set(2011, Calendar.AUGUST, 18, 13, 47, 0);
        BirthNumber birthNumber = new BirthNumber("745903/0326");
        assertEquals(insolvency.getId(), "msphins14713/2011");
        assertEquals(insolvency.getDebtorName(), "Jana Jeřábková");
        assertEquals(insolvency.getProposalTimestamp(), TimeUtils.calendarToTimestamp(proposalTimestamp));
        assertEquals(insolvency.getBirthNumberHashCode(), (Integer) birthNumber.hashCode());
        assertEquals(insolvency.getDebtorAddress(), "Praha 5, Dreyerova 639/11, PSČ 152 00");
        assertEquals(insolvency.getGender(), BirthNumber.Gender.F);
        assertEquals(insolvency.getIco(), "68889291");
        assertEquals(insolvency.getReferenceNumber(), "MSPH 99 INS 14713 / 2011");
        assertEquals(insolvency.getYearOfBirth(), (Integer) birthNumber.getYearOfBirth());
        assertEquals(insolvency.getUrl(), url);

        FileType fileType = fileTypeCapture.getValue();
        AttachedFile attachedFile = attachedFileCapture.getValue();
        TimeUtils.resetTime(proposalTimestamp);
        proposalTimestamp.set(2011, Calendar.AUGUST, 18, 13, 47, 0);
        assertEquals(attachedFile.getPublishDate(), TimeUtils.calendarToTimestamp(filePublishingDate));
        assertEquals(attachedFile.getDocumentSection(), InsolvencyDetailPage.DocumentSection.A);
        assertEquals(attachedFile.getInsolvencyId(), "msphins14713/2011");
        assertEquals(attachedFile.getUrl(), new URL("https://isir.justice.cz/isir/doc/dokument.PDF?id=2799953"));
        assertEquals(attachedFile.getFileTypeId(), new Long(1));
        assertEquals(fileType.getDescription(), "Insolvenční návrh spojený s návrhem na povolení oddlužení");

        Administrator administrator = administratorCapture.getValue();
        AdministratorType administratorType = administratorTypeCapture.getValue();
        assertEquals(administratorType.getDescription(), "Insolvenční správce");
        assertEquals(administrator.getAdministrator(), "JUDr. Václav Kulhavý");
    }

    @Test
    public void testRunDates1() throws Exception {
        URL insolvenciesListURL = new URL("https://isir.justice.cz/isir/ueu/vysledek_lustrace.do?spis_znacky_datum=15.01.2008&spis_znacky_obdobi=14DNI");
        expect(httpDownloader.getHtmlWebsite(insolvenciesListURL)).andReturn(loadFileToString("insolvencyListPageTest.html"));
        final URL url = new URL("https://isir.justice.cz/isir/ueu/evidence_upadcu_detail.do?id=3281c3c7-918f-4296-af8a-cd52fe7a4568");
        expect(httpDownloader.getHtmlWebsite(url)).andReturn(loadFileToString("insolvencyDetailPageOneFileTest.html"));
        Capture<Insolvency> insolvencyCapture = new Capture<>();
        insolvencyService.save(capture(insolvencyCapture));
        Calendar filePublishingDate = Calendar.getInstance();
        TimeUtils.resetTime(filePublishingDate);
        filePublishingDate.set(2011, Calendar.AUGUST, 18, 13, 47, 0);
        Capture<AttachedFile> attachedFileCapture = new Capture<>();
        Capture<FileType> fileTypeCapture = new Capture<>();
        fileService.save(capture(fileTypeCapture));
        expectLastCall().andAnswer(new IAnswer<Object>() {
            @Override
            public Object answer() throws Throwable {
                ((FileType)getCurrentArguments()[0]).setId(1L);
                return null;
            }
        });
        fileService.save(capture(attachedFileCapture));

        Capture<AdministratorType> administratorTypeCapture = new Capture<>();
        Capture<Administrator> administratorCapture = new Capture<>();

        insolvencyService.save(anyObject(Insolvency.class), capture(administratorTypeCapture), capture(administratorCapture));
        replay(fileService, insolvencyService, httpDownloader);

        Calendar fromTo = Calendar.getInstance();
        fromTo.set(2008, Calendar.JANUARY, 2);
        isirHtmlScraper.run(fromTo, fromTo);

        verify(fileService, insolvencyService, httpDownloader);
        Insolvency insolvency = insolvencyCapture.getValue();
        Calendar proposalTimestamp = Calendar.getInstance();
        TimeUtils.resetTime(proposalTimestamp);
        proposalTimestamp.set(2011, Calendar.AUGUST, 18, 13, 47, 0);
        BirthNumber birthNumber = new BirthNumber("745903/0326");
        assertEquals(insolvency.getId(), "msphins14713/2011");
        assertEquals(insolvency.getDebtorName(), "Jana Jeřábková");
        assertEquals(insolvency.getProposalTimestamp(), TimeUtils.calendarToTimestamp(proposalTimestamp));
        assertEquals(insolvency.getBirthNumberHashCode(), (Integer) birthNumber.hashCode());
        assertEquals(insolvency.getDebtorAddress(), "Praha 5, Dreyerova 639/11, PSČ 152 00");
        assertEquals(insolvency.getGender(), BirthNumber.Gender.F);
        assertEquals(insolvency.getIco(), "68889291");
        assertEquals(insolvency.getReferenceNumber(), "MSPH 99 INS 14713 / 2011");
        assertEquals(insolvency.getYearOfBirth(), (Integer) birthNumber.getYearOfBirth());
        assertEquals(insolvency.getUrl(), url);

        FileType fileType = fileTypeCapture.getValue();
        AttachedFile attachedFile = attachedFileCapture.getValue();
        TimeUtils.resetTime(proposalTimestamp);
        proposalTimestamp.set(2011, Calendar.AUGUST, 18, 13, 47, 0);
        assertEquals(attachedFile.getPublishDate(), TimeUtils.calendarToTimestamp(filePublishingDate));
        assertEquals(attachedFile.getDocumentSection(), InsolvencyDetailPage.DocumentSection.A);
        assertEquals(attachedFile.getInsolvencyId(), "msphins14713/2011");
        assertEquals(attachedFile.getUrl(), new URL("https://isir.justice.cz/isir/doc/dokument.PDF?id=2799953"));
        assertEquals(attachedFile.getFileTypeId(), new Long(1));
        assertEquals(fileType.getDescription(), "Insolvenční návrh spojený s návrhem na povolení oddlužení");

        Administrator administrator = administratorCapture.getValue();
        AdministratorType administratorType = administratorTypeCapture.getValue();
        assertEquals(administratorType.getDescription(), "Insolvenční správce");
        assertEquals(administrator.getAdministrator(), "JUDr. Václav Kulhavý");
    }

    @Test
    public void testRunDates2() throws Exception {
        URL insolvenciesListURL = new URL("https://isir.justice.cz/isir/ueu/vysledek_lustrace.do?spis_znacky_datum=15.01.2008&spis_znacky_obdobi=14DNI");
        expect(httpDownloader.getHtmlWebsite(insolvenciesListURL)).andReturn(loadFileToString("insolvencyListPageTest.html"));
        final URL url = new URL("https://isir.justice.cz/isir/ueu/evidence_upadcu_detail.do?id=3281c3c7-918f-4296-af8a-cd52fe7a4568");
        expect(httpDownloader.getHtmlWebsite(url)).andReturn(loadFileToString("nonExistingDetailPage.html"));
        expect(insolvencyService.getInsolvency("ksosins1/2008")).andReturn(null);
        Capture<Insolvency> insolvencyCapture = new Capture<>();
        insolvencyService.save(capture(insolvencyCapture));
        replay(fileService, insolvencyService, httpDownloader);

        Calendar fromTo = Calendar.getInstance();
        TimeUtils.resetTime(fromTo);
        fromTo.set(2008, Calendar.JANUARY, 2, 12, 30, 0);
        isirHtmlScraper.run(fromTo, fromTo);

        verify(fileService, insolvencyService, httpDownloader);
        Insolvency insolvency = insolvencyCapture.getValue();
        BirthNumber birthNumber = new BirthNumber("715802/5523");
        assertEquals(insolvency.getId(), "ksosins1/2008");
        assertEquals(insolvency.getDebtorName(), "KOTYSGLOBAL s.r.o.");
        assertEquals(insolvency.getProposalTimestamp(), TimeUtils.calendarToTimestamp(fromTo));
        assertEquals(insolvency.getBirthNumberHashCode(), (Integer) birthNumber.hashCode());
        assertEquals(insolvency.getGender(), BirthNumber.Gender.F);
        assertEquals(insolvency.getIco(), "25865366");
        assertEquals(insolvency.getReferenceNumber(), "KSOS 8 INS 1 / 2008");
        assertEquals(insolvency.getYearOfBirth(), (Integer) birthNumber.getYearOfBirth());
        assertNull(insolvency.getUrl());

        List<URL> failedUrls = isirHtmlScraper.getFailedUrls();
        assertEquals(failedUrls.size(), 1);
        URL failedUrl = failedUrls.get(0);
        assertEquals(failedUrl, url);
    }

    private String loadFileToString(String name) throws IOException {
        return IOUtils.toString(getClass().getResourceAsStream(name), ENCODING);
    }
}
