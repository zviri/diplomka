package isir.shared.dataaccess.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AdministratorTypeTest {

    @Test
    public void testGetSetId() throws Exception {
        AdministratorType administratorType = new AdministratorType().setId(1L);
        assertEquals(administratorType.getId(), new Long(1));
    }

    @Test
    public void testGetSetDescription() throws Exception {
        AdministratorType administratorType = new AdministratorType().setDescription("  description  description ");
        assertEquals(administratorType.getDescription(), "description description");
    }
}
