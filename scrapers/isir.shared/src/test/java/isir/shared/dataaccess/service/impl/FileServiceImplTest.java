package isir.shared.dataaccess.service.impl;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import isir.shared.dataaccess.dao.IFileDAO;
import isir.shared.dataaccess.dao.IFileTypeDAO;
import isir.shared.dataaccess.dto.AttachedFile;
import isir.shared.dataaccess.dto.FileType;
import isir.shared.utils.TimeUtils;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

public class FileServiceImplTest {

    private IFileDAO fileDAO;
    private IFileTypeDAO fileTypeDAO;
    private FileServiceImpl fileService;

    @Before
    public void setUp() throws Exception {
        fileDAO = createMock(IFileDAO.class);
        fileTypeDAO = createMock(IFileTypeDAO.class);
        fileService = new FileServiceImpl(fileDAO, fileTypeDAO);
    }

    @Test
    public void testSaveAttachedFile1() throws Exception {
        AttachedFile file = new AttachedFile().setId(1L)
                                              .setInsolvencyId("insolvencyId")
                                              .setFileTypeId(2L)
                                              .setPublishDate(TimeUtils.calendarToTimestamp(Calendar.getInstance()));
        fileDAO.update(file);
        replay(fileDAO, fileTypeDAO);
        fileService.save(file);
        verify(fileDAO, fileTypeDAO);
    }

    @Test
    public void testSaveAttachedFile2() throws Exception {
        AttachedFile file = new AttachedFile().setInsolvencyId("insolvencyId")
                                              .setFileTypeId(2L)
                                              .setPublishDate(TimeUtils.calendarToTimestamp(Calendar.getInstance()));
        expect(fileDAO.getFile(file.getFileTypeId(), file.getInsolvencyId(), file.getUrl())).andReturn(new AttachedFile().setId(1L));
        fileDAO.update(file);
        replay(fileDAO, fileTypeDAO);

        fileService.save(file);

        assertEquals(file.getId(), new Long(1));
        verify(fileDAO, fileTypeDAO);
    }

    @Test
    public void testSaveAttachedFile3() throws Exception {
        AttachedFile file = new AttachedFile().setInsolvencyId("insolvencyId")
                                              .setFileTypeId(2L)
                                              .setPublishDate(TimeUtils.calendarToTimestamp(Calendar.getInstance()));
        expect(fileDAO.getFile(file.getFileTypeId(), file.getInsolvencyId(), file.getUrl())).andReturn(null);
        fileDAO.insert(file);
        expect(fileDAO.getFile(file.getFileTypeId(), file.getInsolvencyId(), file.getUrl())).andReturn(new AttachedFile().setId(1L));
        replay(fileDAO, fileTypeDAO);

        fileService.save(file);

        assertEquals(file.getId(), new Long(1));
        verify(fileDAO, fileTypeDAO);
    }

    @Test
    public void testSaveFileType1() throws Exception {
        FileType fileType = new FileType().setId(1L)
                                          .setDescription("description");
        fileTypeDAO.update(fileType);
        replay(fileDAO, fileTypeDAO);

        fileService.save(fileType);

        verify(fileDAO, fileTypeDAO);
    }

    @Test
    public void testSaveFileType2() throws Exception {
        FileType fileType = new FileType().setDescription("description");
        expect(fileTypeDAO.getFileType(fileType.getDescription())).andReturn(new FileType().setId(1L));
        fileTypeDAO.update(fileType);
        replay(fileDAO, fileTypeDAO);

        fileService.save(fileType);

        assertEquals(fileType.getId(), new Long(1));
        verify(fileDAO, fileTypeDAO);
    }

    @Test
    public void testSaveFileType3() throws Exception {
        FileType fileType = new FileType().setDescription("description");
        expect(fileTypeDAO.getFileType(fileType.getDescription())).andReturn(null);
        fileTypeDAO.insert(fileType);
        expect(fileTypeDAO.getFileType(fileType.getDescription())).andReturn(new FileType().setId(1L));
        replay(fileDAO, fileTypeDAO);

        fileService.save(fileType);

        assertEquals(fileType.getId(), new Long(1));
        verify(fileDAO, fileTypeDAO);
    }

    @Test
    public void testSaveFileFileType() throws Exception {
        AttachedFile file = new AttachedFile().setId(1L)
                                              .setInsolvencyId("insolvencyId")
                                              .setPublishDate(TimeUtils.calendarToTimestamp(Calendar.getInstance()));
        FileType fileType = new FileType().setId(1L)
                                          .setDescription("description");
        fileTypeDAO.update(fileType);
        fileDAO.update(file);
        replay(fileDAO, fileTypeDAO);

        fileService.save(file, fileType);

        assertEquals(file.getFileTypeId(), new Long(1));
        verify(fileDAO, fileTypeDAO);
    }
}
