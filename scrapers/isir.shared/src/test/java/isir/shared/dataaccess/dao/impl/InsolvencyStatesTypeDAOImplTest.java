package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dto.InsolvencyStateType;
import isir.shared.dataaccess.test.IsirDbTestCase;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class InsolvencyStatesTypeDAOImplTest extends IsirDbTestCase {

    InsolvencyStatesTypeDAOImpl insolvencyStatesTypeDAO = null;

    @Before
    public void setupDao() {
        insolvencyStatesTypeDAO = new InsolvencyStatesTypeDAOImpl();
        insolvencyStatesTypeDAO.setDataSource(getDataSource());
    }

    @Test
    public void testGetAllInsolvencyStatesTypes() throws Exception {
        List<InsolvencyStateType> allInsolvencyStatesTypes = insolvencyStatesTypeDAO.getAllInsolvencyStatesTypes();

        assertEquals(allInsolvencyStatesTypes.size(), 1);
        InsolvencyStateType insolvencyStateType = allInsolvencyStatesTypes.get(0);
        assertEquals(insolvencyStateType.getId(), new Long(1000000L));
        assertEquals(insolvencyStateType.getDescription(), "description1");
        assertEquals(insolvencyStateType.getTextIdentifier(), "ODDLUZENI");
    }

    @Test
    public void testGetInsolvencyStateType() throws Exception {
        String oddluzeni = "ODDLUZENI";
        InsolvencyStateType insolvencyStateType = insolvencyStatesTypeDAO.getInsolvencyStateType(oddluzeni);

        assertNotNull(insolvencyStateType);
        assertEquals(insolvencyStateType.getId(), new Long(1000000L));
        assertEquals(insolvencyStateType.getTextIdentifier(), oddluzeni);
        assertEquals(insolvencyStateType.getDescription(), "description1");
    }

    @Test
    public void testInsert() throws Exception {
        InsolvencyStateType insolvencyStateType = new InsolvencyStateType().setTextIdentifier("REORGANIZACE")
                                                                           .setDescription("description2");
        insolvencyStatesTypeDAO.insert(insolvencyStateType);
        InsolvencyStateType insolvencyStateTypeFromDb = insolvencyStatesTypeDAO.getInsolvencyStateType(insolvencyStateType.getTextIdentifier());

        assertNotNull(insolvencyStateTypeFromDb);
        assertNotNull(insolvencyStateTypeFromDb.getId());
        assertEquals(insolvencyStateTypeFromDb.getTextIdentifier(), insolvencyStateType.getTextIdentifier());
        assertEquals(insolvencyStateTypeFromDb.getDescription(), insolvencyStateType.getDescription());
    }

    @Test
    public void testUpdate() throws Exception {
        InsolvencyStateType insolvencyStateType = new InsolvencyStateType().setId(1000000L)
                                                                           .setTextIdentifier("REORGANIZACE")
                                                                           .setDescription("description2");
        insolvencyStatesTypeDAO.update(insolvencyStateType);
        InsolvencyStateType insolvencyStateTypeFromDb = insolvencyStatesTypeDAO.getInsolvencyStateType(insolvencyStateType.getTextIdentifier());

        assertNotNull(insolvencyStateTypeFromDb);
        assertEquals(insolvencyStateTypeFromDb.getId(), insolvencyStateType.getId());
        assertEquals(insolvencyStateTypeFromDb.getTextIdentifier(), insolvencyStateType.getTextIdentifier());
        assertEquals(insolvencyStateTypeFromDb.getDescription(), insolvencyStateType.getDescription());
    }

    @Override
    public String getDataSetFileName() {
        return "InsolvencyStatesTypeDAOImplTest.xml";
    }
}
