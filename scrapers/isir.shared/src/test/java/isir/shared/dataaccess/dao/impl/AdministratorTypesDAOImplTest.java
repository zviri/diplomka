package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dto.AdministratorType;
import isir.shared.dataaccess.test.IsirDbTestCase;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AdministratorTypesDAOImplTest extends IsirDbTestCase {

    AdministratorTypesDAOImpl administratorTypesDAO = null;

    @Before
    public void setupDao() {
        administratorTypesDAO = new AdministratorTypesDAOImpl();
        administratorTypesDAO.setDataSource(getDataSource());
    }

    @Test
    public void testGetAdministratorType() throws Exception {
        String description = "administratorType1";
        AdministratorType administratorType = administratorTypesDAO.getAdministratorType(description);

        assertNotNull(administratorType);
        assertEquals(administratorType.getDescription(), description);
        assertEquals(administratorType.getId(), (Long) 1000000L);
    }

    @Test
    public void testInsert() throws Exception {
        AdministratorType administratorType = new AdministratorType().setDescription("administratorType2");
        administratorTypesDAO.insert(administratorType);
        AdministratorType administratorTypeFromDb = administratorTypesDAO.getAdministratorType(administratorType.getDescription());

        assertNotNull(administratorTypeFromDb);
        assertNotNull(administratorTypeFromDb.getId());
        assertEquals(administratorTypeFromDb.getDescription(), administratorType.getDescription());
    }

    @Test
    public void testUpdate() throws Exception {
        AdministratorType administratorType = new AdministratorType().setDescription("administratorType2")
                                                                     .setId(1000000L);
        administratorTypesDAO.update(administratorType);
        AdministratorType administratorTypeFromDb = administratorTypesDAO.getAdministratorType(administratorType.getDescription());

        assertNotNull(administratorTypeFromDb);
        assertEquals(administratorTypeFromDb.getId(), administratorType.getId());
        assertEquals(administratorTypeFromDb.getDescription(), administratorType.getDescription());
    }

    @Override
    public String getDataSetFileName() {
        return "AdministratorTypesDAOImplTest.xml";
    }
}
