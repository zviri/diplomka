package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dto.FileType;
import isir.shared.dataaccess.test.IsirDbTestCase;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FileTypeDAOImplTest extends IsirDbTestCase{

    FileTypeDAOImpl fileTypeDAO = null;

    @Before
    public void setupDao() {
        fileTypeDAO = new FileTypeDAOImpl();
        fileTypeDAO.setDataSource(getDataSource());
    }

    @Test
    public void testGetFileType() throws Exception {
        FileType fileType = fileTypeDAO.getFileType("fileType1");
        assertNotNull(fileType);
        assertEquals(fileType.getId(), (Long) 1000000L);
        assertEquals(fileType.getDescription(), "fileType1");
    }

    @Test
    public void testGetFileType2() throws Exception {
        FileType fileType = fileTypeDAO.getFileType(1000000L);
        assertNotNull(fileType);
        assertEquals(fileType.getId(), (Long) 1000000L);
        assertEquals(fileType.getDescription(), "fileType1");
    }

    @Test
    public void testInsert() throws Exception {
        String description = "fileType2";
        FileType fileType = new FileType().setDescription(description);

        fileTypeDAO.insert(fileType);
        FileType fileTypeFromDb = fileTypeDAO.getFileType(description);

        assertNotNull(fileTypeFromDb);
        assertNotNull(fileTypeFromDb.getId());
        assertEquals(fileTypeFromDb.getDescription(), description);
    }

    @Test
    public void testUpdate() throws Exception {
        String description = "fileType2";
        FileType fileType = new FileType().setId(1000000L)
                                          .setDescription(description);

        fileTypeDAO.update(fileType);
        FileType fileTypeFromDb = fileTypeDAO.getFileType(description);

        assertNotNull(fileTypeFromDb);
        assertEquals(fileTypeFromDb.getId(), new Long(1000000L));
        assertEquals(fileTypeFromDb.getDescription(), description);
    }

    @Test
    public void testDelete() throws Exception {
        fileTypeDAO.delete(1000000L);
        FileType fileType = fileTypeDAO.getFileType("fileType1");
        assertNull(fileType);
    }

    @Override
    public String getDataSetFileName() {
        return "FileTypeDAOImplTest.xml";
    }
}
