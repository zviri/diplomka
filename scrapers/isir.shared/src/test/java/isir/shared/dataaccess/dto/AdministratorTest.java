package isir.shared.dataaccess.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AdministratorTest {

    @Test
    public void testGetSetId() throws Exception {
        Administrator administrator = new Administrator().setId(1L);
        assertEquals(administrator.getId(), new Long(1));
    }

    @Test
    public void testGetSetAdministrator() throws Exception {
        Administrator administrator = new Administrator().setAdministrator(" Fetoslav  Fetak  ");
        assertEquals(administrator.getAdministrator(), "Fetoslav Fetak");
    }
}
