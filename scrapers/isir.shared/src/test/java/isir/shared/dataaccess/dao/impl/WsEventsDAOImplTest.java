package isir.shared.dataaccess.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import isir.shared.dataaccess.dto.WsEvent;
import isir.shared.dataaccess.test.IsirDbTestCase;
import isir.shared.isirpub001.IsirPub001Stub;
import isir.shared.utils.TimeUtils;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

public class WsEventsDAOImplTest extends IsirDbTestCase {

    WsEventsDAOImpl wsEventsDAO = null;

    @Before
    public void setupDao() {
        wsEventsDAO = new WsEventsDAOImpl();
        wsEventsDAO.setDataSource(getDataSource());
    }

    @Test
    public void testGetEvent() throws Exception {
        WsEvent wsEvent = wsEventsDAO.getEvent(1L);

        assertNotNull(wsEvent);
        assertEquals(wsEvent.getId(), new Long(1));
        assertEquals(wsEvent.getInsolvencyId(), "insolvencyId");
        Calendar timestamp = Calendar.getInstance();
        TimeUtils.resetTime(timestamp);
        timestamp.set(2012, 3, 24, 14, 37, 0);
        assertEquals(wsEvent.getTimestamp(), TimeUtils.calendarToTimestamp(timestamp));

        IsirPub001Stub.IsirPub001Data isirPub001Data = wsEvent.getEvent();
        assertNotNull(isirPub001Data);
        assertEquals(isirPub001Data.getId(), 1246L);
        assertEquals(isirPub001Data.getSpisZnacka(), "INS 1/2008");
        assertEquals(isirPub001Data.getTypText(), "Insolvenční návrh");
        assertEquals(isirPub001Data.getTyp(), "5");
        assertEquals(isirPub001Data.getPoradiVOddilu(), 1);
        assertEquals(isirPub001Data.getOddil(), "A");
        Calendar cas = Calendar.getInstance(TimeZone.getTimeZone("Europe/Prague"));
        cas.setTimeInMillis(1199273400000L);
        assertEquals(isirPub001Data.getCas(), cas);
        assertEquals(isirPub001Data.getIdDokument(), "https://isir.justice.cz:8443/isir_ws/doc/Document?idDokument=1246");
        assertEquals(StringUtils.deleteWhitespace(isirPub001Data.getPoznamka()), StringUtils.deleteWhitespace("<?xml version=\"1.0\" encoding=\"UTF-8\"?><tns:udalost xmlns:tns=\"http://www.cca.cz/isir/poznamka\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" verzeXsd=\"1\" xsi:schemaLocation=\"http://www.cca.cz/isir/poznamka https://isir.justice.cz:8443/isir_ws/xsd/poznamka.xsd\"><idOsobyPuvodce>KSSEMOS</idOsobyPuvodce><vec><druhStavRizeni>NEVYRIZENA</druhStavRizeni></vec></tns:udalost>"));
    }

    @Test
    public void testGetEvents() throws Exception {
        Calendar from = Calendar.getInstance();
        from.set(2012, 4, 24);
        Calendar to = Calendar.getInstance();
        to.set(2012, 4, 25);
        List<WsEvent> events = wsEventsDAO.getEvents(from, to);
        assertEquals(events.size(), 2);
    }

    @Test
    public void testGetMaxId() throws Exception {
        long maxId = wsEventsDAO.getMaxId();
        assertEquals(maxId, 4);
    }

    @Test
    public void testGetWsEventsFromId() throws Exception {
        List<WsEvent> wsEventsFromId = wsEventsDAO.getWsEventsFromId(new Long(4));
        assertEquals(wsEventsFromId.size(), 1);
        WsEvent wsEvent = wsEventsFromId.get(0);
        assertEquals(wsEvent.getId(), new Long(4));
    }

    @Test
    public void testInsert() throws Exception {
        WsEvent wsEvent = new WsEvent();
        wsEvent.setId(10L);
        wsEvent.setTimestamp(TimeUtils.calendarToTimestamp(Calendar.getInstance()));
        wsEvent.setInsolvencyId("insolvencyId4");
        IsirPub001Stub.IsirPub001Data data = new IsirPub001Stub.IsirPub001Data();
        data.setId(15L);
        data.setPoznamka("Poznamka");
        wsEvent.setEvent(data);

        wsEventsDAO.insert(wsEvent);
        WsEvent wsEventFromDb = wsEventsDAO.getEvent(wsEvent.getId());

        assertNotNull(wsEventFromDb);
        assertEquals(wsEventFromDb.getId(), wsEvent.getId());
        assertEquals(wsEventFromDb.getInsolvencyId(), wsEvent.getInsolvencyId());
        assertEquals(wsEventFromDb.getTimestamp(), wsEvent.getTimestamp());
        assertEquals(wsEventFromDb.getEvent().getId(), wsEvent.getEvent().getId());
        assertEquals(wsEventFromDb.getEvent().getPoznamka(), wsEvent.getEvent().getPoznamka());


    }

    @Test
    public void testUpdate() throws Exception {
        WsEvent wsEvent = new WsEvent();
        wsEvent.setId(4L);
        wsEvent.setTimestamp(TimeUtils.calendarToTimestamp(Calendar.getInstance()));
        wsEvent.setInsolvencyId("insolvencyId4");
        IsirPub001Stub.IsirPub001Data data = new IsirPub001Stub.IsirPub001Data();
        data.setId(15L);
        data.setPoznamka("Poznamka");
        wsEvent.setEvent(data);

        wsEventsDAO.update(wsEvent);
        WsEvent wsEventFromDb = wsEventsDAO.getEvent(wsEvent.getId());

        assertNotNull(wsEventFromDb);
        assertEquals(wsEventFromDb.getId(), wsEvent.getId());
        assertEquals(wsEventFromDb.getInsolvencyId(), wsEvent.getInsolvencyId());
        assertEquals(wsEventFromDb.getTimestamp(), wsEvent.getTimestamp());
        assertEquals(wsEventFromDb.getEvent().getId(), wsEvent.getEvent().getId());
        assertEquals(wsEventFromDb.getEvent().getPoznamka(), wsEvent.getEvent().getPoznamka());
    }

    @Override
    public String getDataSetFileName() {
        return "WsEventsDAOImplTest.xml";
    }
}
