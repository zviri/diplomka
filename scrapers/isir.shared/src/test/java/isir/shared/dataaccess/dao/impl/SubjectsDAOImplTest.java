package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dto.Subject;
import isir.shared.dataaccess.test.IsirDbTestCase;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SubjectsDAOImplTest extends IsirDbTestCase {

    SubjectsDAOImpl subjectsDAO = null;

    @Before
    public void setupDao() {
        subjectsDAO = new SubjectsDAOImpl();
        subjectsDAO.setDataSource(getDataSource());
    }

    @Test
    public void testGetSubject() throws Exception {
        Subject subject = subjectsDAO.getSubject("subject1");
        assertEquals(subject.getName(), "subject1");
        assertEquals(subject.getIco(), "123456");
        assertEquals(subject.getForm(), "form1");
        assertEquals(subject.getLegalForm(), "legal_form1");
        assertEquals(subject.getAddressCity(), "Praha");
        assertEquals(subject.getAddressCountry(), "Ceska Republika");
        assertEquals(subject.getAddressDescriptionNumber(), "1");
        assertEquals(subject.getAddressForm(), "TRVALA");
        assertEquals(subject.getAddresStreet(), "Parizska");
        assertEquals(subject.getAddressZipCode(), "15000");
        assertEquals(subject.getDegreeBefore(), "Bc.");
        assertEquals(subject.getDegreeAfter(), "PhD.");

    }

    @Test
    public void testInsert() throws Exception {
        Subject subject = new Subject().setName("subject2")
                .setIco("456789")
                .setForm("form2")
                .setLegalForm("legal_form2")
                .setAddressCity("Plzen")
                .setAddressCountry("Ceska Republika")
                .setAddressDescriptionNumber("2")
                .setAddressForm("PRECHODNA")
                .setAddresStreet("Hlavni")
                .setAddressZipCode("78910")
                .setDegreeBefore("Bc.")
                .setDegreeAfter("PhD.");
        subjectsDAO.insert(subject);
        Subject existingSubject = subjectsDAO.getSubject(subject.getName());
        assertTrue(subject.equals(existingSubject));
    }

    @Test
    public void testUpdate() throws Exception {
        Subject subject = new Subject().setName("subject1")
                .setIco("456789")
                .setForm("form2")
                .setLegalForm("legal_form2")
                .setAddressCity("Plzen")
                .setAddressCountry("Ceska Republika")
                .setAddressDescriptionNumber("2")
                .setAddressForm("PRECHODNA")
                .setAddresStreet("Hlavni")
                .setAddressZipCode("78910")
                .setDegreeBefore("Bc.")
                .setDegreeAfter("PhD.");
        subjectsDAO.update(subject);
        Subject existingSubject = subjectsDAO.getSubject(subject.getName());
        assertTrue(subject.equals(existingSubject));
    }

    @Override
    public String getDataSetFileName() {
        return "SubjectsDAOImplTest.xml";
    }
}
