package isir.shared.dataaccess.dto;

import isir.shared.scraping.InsolvencyDetailPage;
import isir.shared.utils.TimeUtils;
import org.junit.Test;

import java.net.URL;
import java.sql.Timestamp;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AttachedFileTest {

    @Test
    public void testGetSetId() throws Exception {
        AttachedFile attachedFile = new AttachedFile().setId(1L);
        assertEquals(attachedFile.getId(), new Long(1));

    }

    @Test
    public void testGetSetUrl() throws Exception {
        URL url = new URL("http://www.google.sk");
        AttachedFile attachedFile = new AttachedFile().setUrl(url);
        assertEquals(attachedFile.getUrl(), url);
    }

    @Test
    public void testGetSetInsolvencyId() throws Exception {
        AttachedFile attachedFile = new AttachedFile().setInsolvencyId("insolvencyId");
        assertEquals(attachedFile.getInsolvencyId(), "insolvencyId");
    }

    @Test
    public void testGetSetFileTypeId() throws Exception {
        AttachedFile attachedFile = new AttachedFile().setFileTypeId(1L);
        assertEquals(attachedFile.getFileTypeId(), new Long(1));
    }

    @Test
    public void testGetSetDocumentSection() throws Exception {
        AttachedFile attachedFile = new AttachedFile().setDocumentSection(InsolvencyDetailPage.DocumentSection.P);
        assertEquals(attachedFile.getDocumentSection(), InsolvencyDetailPage.DocumentSection.P);
    }

    @Test
    public void testGetSetPublishDate() throws Exception {
        Timestamp publishDate = TimeUtils.calendarToTimestamp(Calendar.getInstance());
        AttachedFile attachedFile = new AttachedFile().setPublishDate(publishDate);
        assertEquals(attachedFile.getPublishDate(), publishDate);
    }

    @Test
    public void testGetSetCreditor() throws Exception {
        AttachedFile attachedFile = new AttachedFile().setCreditor("  Creditor  creditor ");
        assertEquals(attachedFile.getCreditor(), "Creditor creditor");

        attachedFile.setCreditor(" ");
        assertNull(attachedFile.getCreditor());
    }
}
