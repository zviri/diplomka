package isir.shared.dataaccess.service.impl;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import isir.shared.dataaccess.dao.IWsEventsDAO;
import isir.shared.dataaccess.dto.WsEvent;

import java.util.ArrayList;

import org.junit.Test;

public class WsEventsServiceImplTest {

    @Test
    public void testSave1() throws Exception {
        IWsEventsDAO wsEventsDAO = createMock(IWsEventsDAO.class);
        WsEventsServiceImpl wsEventsService = new WsEventsServiceImpl(wsEventsDAO);
        final WsEvent wsEvent = new WsEvent();
        wsEventsDAO.insert(wsEvent);
        replay(wsEventsDAO);
        wsEventsService.save(new ArrayList<WsEvent>() {{ add(wsEvent); }});
    }

    @Test
    public void testSave2() throws Exception {
        IWsEventsDAO wsEventsDAO = createMock(IWsEventsDAO.class);
        WsEventsServiceImpl wsEventsService = new WsEventsServiceImpl(wsEventsDAO);
        final WsEvent wsEvent = new WsEvent();
        Long eventId = 1L;
        wsEvent.setId(eventId);
        expect(wsEventsDAO.getEvent(eventId)).andReturn(null);
        wsEventsDAO.insert(wsEvent);
        replay(wsEventsDAO);
        wsEventsService.save(new ArrayList<WsEvent>() {{ add(wsEvent); }});
    }

    @Test
    public void testSave3() throws Exception {
        IWsEventsDAO wsEventsDAO = createMock(IWsEventsDAO.class);
        WsEventsServiceImpl wsEventsService = new WsEventsServiceImpl(wsEventsDAO);
        final WsEvent wsEvent = new WsEvent();
        Long eventId = 1L;
        wsEvent.setId(eventId);
        expect(wsEventsDAO.getEvent(eventId)).andReturn(wsEvent);
        wsEventsDAO.update(wsEvent);
        replay(wsEventsDAO);
        wsEventsService.save(new ArrayList<WsEvent>() {{ add(wsEvent); }});
    }
}
