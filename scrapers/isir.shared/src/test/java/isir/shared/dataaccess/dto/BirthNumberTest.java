package isir.shared.dataaccess.dto;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BirthNumberTest {

    @Test
    public void testGetGender() throws Exception {
        assertEquals(new BirthNumber("747204/0020").getGender(), BirthNumber.Gender.F);
        assertEquals(new BirthNumber("745104/0020").getGender(), BirthNumber.Gender.F);
        assertEquals(new BirthNumber("740204/0020").getGender(), BirthNumber.Gender.M);
        assertEquals(new BirthNumber("742104/0020").getGender(), BirthNumber.Gender.M);
    }

    @Test
    public void testGetYearOfBirth() throws Exception {
        assertEquals(new BirthNumber("747204/020").getYearOfBirth(), 1974);
        assertEquals(new BirthNumber("747204/0020").getYearOfBirth(), 1974);
        assertEquals(new BirthNumber("107204/0020").getYearOfBirth(), 2010);
    }
}
