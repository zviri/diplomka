package isir.shared.dataaccess.dto;

import isir.shared.utils.TimeUtils;
import org.junit.Test;

import java.net.URL;
import java.sql.Timestamp;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;

public class InsolvencyTest {

    @Test
    public void testGetSetId() throws Exception {
        String insolvencyId = "insolvencyId";
        Insolvency insolvency = new Insolvency().setId(insolvencyId);
        assertEquals(insolvency.getId(), insolvencyId);
    }

    @Test
    public void testGetSetDebtorName() throws Exception {
        Insolvency insolvency = new Insolvency().setDebtorName(" debtor  debtor  ");
        assertEquals(insolvency.getDebtorName(), "debtor debtor");
    }

    @Test
    public void testGetSetIco() throws Exception {
        Insolvency insolvency = new Insolvency().setIco("123456");
        assertEquals(insolvency.getIco(), "123456");
    }

    @Test
    public void testGetSetReferenceNumber() throws Exception {
        Insolvency insolvency = new Insolvency().setReferenceNumber(" reference  number ");
        assertEquals(insolvency.getReferenceNumber(), "reference number");
    }

    @Test
    public void testGetSetProposalTimestamp() throws Exception {
        Timestamp proposalTimestamp = TimeUtils.calendarToTimestamp(Calendar.getInstance());
        Insolvency insolvency = new Insolvency().setProposalTimestamp(proposalTimestamp);
        assertEquals(insolvency.getProposalTimestamp(), proposalTimestamp);
    }

    @Test
    public void testGetSetUrl() throws Exception {
        URL url = new URL("http://www.google.sk");
        Insolvency insolvency = new Insolvency().setUrl(url);
        assertEquals(insolvency.getUrl(), url);

    }

    @Test
    public void testGetSetInsolvencyVec() throws Exception {
        Insolvency insolvency = new Insolvency().setId("ksbrins10001/2012");
        assertEquals(insolvency.getInsolvencyVec(), 10001);
    }

    @Test
    public void testGetSetInsolvencyRok() throws Exception {
        Insolvency insolvency = new Insolvency().setId("ksbrins10001/2012");
        assertEquals(insolvency.getInsolvencyRok(), 2012);
    }

    @Test
    public void testGetSenatNumber() throws Exception {
        Insolvency insolvency = new Insolvency().setReferenceNumber("KSUL 69 INS 2985 / 2011");
        assertEquals(insolvency.getSenatNumber(), 69);
    }

    @Test
    public void testGetSetDebtorAddress() throws Exception {
        Insolvency insolvency = new Insolvency().setDebtorAddress(" debtor  address  ");
        assertEquals(insolvency.getDebtorAddress(), "debtor address");
    }

    @Test
    public void testGetSetGender() throws Exception {
        Insolvency insolvency = new Insolvency().setGender(BirthNumber.Gender.M);
        assertEquals(insolvency.getGender(), BirthNumber.Gender.M);
    }

    @Test
    public void testGetSetBirthNumberHashCode() throws Exception {
        Insolvency insolvency = new Insolvency().setBirthNumberHashCode(1);
        assertEquals(insolvency.getBirthNumberHashCode(), new Integer(1));
    }

    @Test
    public void testGetSetYearOfBirth() throws Exception {
        Insolvency insolvency = new Insolvency().setYearOfBirth(1990);
        assertEquals(insolvency.getYearOfBirth(), new Integer(1990));
    }

    @Test
    public void testGetSetRegionId() throws Exception {
        Insolvency insolvency = new Insolvency().setRegionId(1L);
        assertEquals(insolvency.getRegionId(), new Long(1));
    }
}
