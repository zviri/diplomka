package isir.shared.dataaccess.dto;

import isir.shared.utils.TimeUtils;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;

public class InsolvencyStateTest {

    @Test
    public void testGetSetState() throws Exception {
        InsolvencyState insolvencyState = new InsolvencyState().setState(1L);
        assertEquals(insolvencyState.getState(), new Long(1));
    }

    @Test
    public void testGetSetActionID() throws Exception {
        InsolvencyState insolvencyState = new InsolvencyState().setActionID(1L);
        assertEquals(insolvencyState.getActionID(), new Long(1));
    }

    @Test
    public void testGetSetInsolvencyID() throws Exception {
        InsolvencyState insolvencyState = new InsolvencyState().setInsolvencyID("insolvencyId");
        assertEquals(insolvencyState.getInsolvencyID(), "insolvencyId");
    }

    @Test
    public void testGetSetStateChangeTimestamp() throws Exception {
        Timestamp stateChangeTimestamp = TimeUtils.calendarToTimestamp(Calendar.getInstance());
        InsolvencyState insolvencyState = new InsolvencyState().setStateChangeTimestamp(stateChangeTimestamp);
        assertEquals(insolvencyState.getStateChangeTimestamp(), stateChangeTimestamp);
    }
}
