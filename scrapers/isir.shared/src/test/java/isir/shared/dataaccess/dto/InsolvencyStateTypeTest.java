package isir.shared.dataaccess.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class InsolvencyStateTypeTest {

    @Test
    public void testGetSetId() throws Exception {
        InsolvencyStateType insolvencyStateType = new InsolvencyStateType().setId(1L);
        assertEquals(insolvencyStateType.getId(), new Long(1));
    }

    @Test
    public void testGetSetTextIdentifier() throws Exception {
        InsolvencyStateType insolvencyStateType = new InsolvencyStateType().setTextIdentifier(" text  identifier ");
        assertEquals(insolvencyStateType.getTextIdentifier(), "text identifier");
    }

    @Test
    public void testGetSetDescription() throws Exception {
        InsolvencyStateType insolvencyStateType = new InsolvencyStateType().setDescription("  description  description  ");
        assertEquals(insolvencyStateType.getDescription(), "description description");
    }
}
