package isir.shared.dataaccess.procedures;


import isir.shared.dataaccess.test.IsirDbTestCase;
import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestCleanInsolvencyStates2 extends IsirDbTestCase {

    @Test
    public void testCleanInsolvencyStates() throws Exception {
        Connection connection = getDataSource().getConnection();
        connection.prepareStatement("SELECT clean_insolvency_states()").execute();

        ResultSet resultSet = connection.prepareStatement("SELECT * FROM insolvency_state_tab ORDER BY action_id ASC").executeQuery();
        assertTrue(resultSet.next());
        assertEquals(resultSet.getLong("action_id"), 1);
        assertTrue(resultSet.next());
        assertEquals(resultSet.getLong("action_id"), 4);
        assertTrue(resultSet.next());
        assertEquals(resultSet.getLong("action_id"), 5);
    }

    @Override
    public String getDataSetFileName() {
        return "TestCleanInsolvencyStates2.xml";
    }
}
