package isir.shared.dataaccess.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import isir.shared.dataaccess.dto.Administrator;
import isir.shared.dataaccess.test.IsirDbTestCase;

import org.junit.Before;
import org.junit.Test;

public class AdministratorsDAOImplTest extends IsirDbTestCase {

    AdministratorsDAOImpl administratorsDAO = null;

    @Before
    public void setupDao() {
        administratorsDAO = new AdministratorsDAOImpl();
        administratorsDAO.setDataSource(getDataSource());
    }

    @Test
    public void testGetAdministrator() throws Exception {
        String administratorName = "administrator1";
        Administrator administrator = administratorsDAO.getAdministrator(administratorName);

        assertEquals(administrator.getAdministrator(), administratorName);
        assertEquals(administrator.getId(), (Long) 1000000L);
    }

    @Test
    public void testInsert() throws Exception {
        String administratorName = "administrator2";
        Administrator administrator = new Administrator().setAdministrator(administratorName);

        administratorsDAO.insert(administrator);
        Administrator administratorFromDb = administratorsDAO.getAdministrator(administratorName);

        assertNotNull(administratorFromDb);
        assertNotNull(administratorFromDb.getId());
        assertEquals(administratorFromDb.getAdministrator(), administratorName);
    }

    @Test
    public void testUpdate() throws Exception {
        String administratorName = "administrator2";
        Administrator administrator = new Administrator().setId(1000000L)
                                                         .setAdministrator(administratorName);
        administratorsDAO.update(administrator);
        Administrator administratorFromDb = administratorsDAO.getAdministrator(administratorName);

        assertNotNull(administratorFromDb);
        assertEquals(administratorFromDb.getId(), administrator.getId());
        assertEquals(administratorFromDb.getAdministrator(), administratorName);
    }

    @Override
    public String getDataSetFileName() {
        return "AdministratorDAOImplTest.xml";
    }
}
