package isir.shared.dataaccess.dto;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SubjectTest {

    @Test
    public void testGetSetName() throws Exception {
        Subject subject = new Subject().setName(" Janko  mrkvicka");
        assertEquals(subject.getName(), "Janko mrkvicka");
    }

    @Test
    public void testGetSetIco() throws Exception {
        Subject subject = new Subject().setIco(" 123456 ");
        assertEquals(subject.getIco(), "123456");
    }

    @Test
    public void testGetSetForm() throws Exception {
        Subject subject = new Subject().setForm(" la  la ");
        assertEquals(subject.getForm(), "la la");
    }

    @Test
    public void testGetSetLegalForm() throws Exception {
        Subject subject = new Subject().setLegalForm(" a.s.");
        assertEquals(subject.getLegalForm(), "a.s.");
    }

    @Test
    public void testGetSetAddressForm() throws Exception {
        Subject subject = new Subject().setAddressForm(" TRVALA");
        assertEquals(subject.getAddressForm(), "TRVALA");
    }

    @Test
    public void testGetSetAddressCity() throws Exception {
        Subject subject = new Subject().setAddressCity(" Praha");
        assertEquals(subject.getAddressCity(), "Praha");
    }

    @Test
    public void testGetSetAddresStreet() throws Exception {
        Subject subject = new Subject().setAddresStreet(" Parizska");
        assertEquals(subject.getAddresStreet(), "Parizska");
    }

    @Test
    public void testGetSetAddressDescriptionNumber() throws Exception {
        Subject subject = new Subject().setAddressDescriptionNumber(" 1");
        assertEquals(subject.getAddressDescriptionNumber(), "1");
    }

    @Test
    public void testGetSetAddressCountry() throws Exception {
        Subject subject = new Subject().setAddressCountry(" Cesko");
        assertEquals(subject.getAddressCountry(), "Cesko");
    }

    @Test
    public void testGetSetAddressZipCode() throws Exception {
        Subject subject = new Subject().setAddressZipCode("150 00");
        assertEquals(subject.getAddressZipCode(), "15000");
    }

    @Test
    public void testGetSetDegreeBefore() throws Exception {
        Subject subject = new Subject().setDegreeBefore("Bc. ");
        assertEquals(subject.getDegreeBefore(), "Bc.");
    }

    @Test
    public void testGetSetDegreeAfter() throws Exception {
        Subject subject = new Subject().setDegreeAfter("Bc. ");
        assertEquals(subject.getDegreeAfter(), "Bc.");
    }
}
