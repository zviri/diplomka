package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dto.BirthNumber;
import isir.shared.dataaccess.dto.Insolvency;
import isir.shared.dataaccess.test.IsirDbTestCase;
import isir.shared.utils.TimeUtils;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class InsolvencyDAOImplTest extends IsirDbTestCase {

    InsolvencyDAOImpl insolvencyDAO = null;

    @Before
    public void setupDao() {
        insolvencyDAO = new InsolvencyDAOImpl();
        insolvencyDAO.setDataSource(getDataSource());
    }

    @Test
    public void testGetInsolvency() throws Exception {
        Insolvency insolvency = insolvencyDAO.getInsolvency("ksbrins10001/2012");

        assertNotNull(insolvency);
        assertEquals(insolvency.getId(), "ksbrins10001/2012");
        assertEquals(insolvency.getDebtorName(), "Milan Jansta");
        assertEquals(insolvency.getIco(), "26883392");
        assertEquals(insolvency.getReferenceNumber(), "KSBR 29 INS 10001 / 2012");
        Calendar proposalTimeStamp = Calendar.getInstance();
        TimeUtils.resetTime(proposalTimeStamp);
        proposalTimeStamp.set(2012, 3, 24, 14, 37, 0);
        assertEquals(insolvency.getProposalTimestamp(), TimeUtils.calendarToTimestamp(proposalTimeStamp));
        assertEquals(insolvency.getUrl(), new URL("https://isir.justice.cz/isir/ueu/evidence_upadcu_detail.do?id=9de4d34d-f7a3-4873-a5e9-ba7a2e889c19"));
        assertEquals(insolvency.getGender(), BirthNumber.Gender.M);
        assertEquals(insolvency.getDebtorAddress(), "Kroměříž, Obvodová 3790/26, PSČ 767 01");
        assertEquals(insolvency.getYearOfBirth(), (Integer) 1990);
        assertEquals(insolvency.getBirthNumberHashCode(), (Integer) 5);
        assertEquals(insolvency.getRegionId(), (Long) 1L);

    }

    @Test
    public void testInsert() throws Exception {
        Insolvency insolvency = new Insolvency().setId("insolvencyId1")
                                                .setDebtorName("debtor1")
                                                .setIco("123456")
                                                .setReferenceNumber("referenceNumber1")
                                                .setProposalTimestamp(TimeUtils.calendarToTimestamp(Calendar.getInstance()))
                                                .setUrl(new URL("http://www.google.sk"))
                                                .setGender(BirthNumber.Gender.M)
                                                .setYearOfBirth(1991)
                                                .setBirthNumberHashCode(7)
                                                .setRegionId(1L)
                                                .setDebtorAddress("adress");
        insolvencyDAO.insert(insolvency);
        Insolvency insolvencyFromDb = insolvencyDAO.getInsolvency("insolvencyId1");

        assertNotNull(insolvencyFromDb);
        assertEquals(insolvencyFromDb, insolvency);
    }

    @Test
    public void testUpdate() throws Exception {
        Insolvency insolvency = new Insolvency().setId("ksbrins10001/2012")
                                                .setDebtorName("debtor1")
                                                .setIco("123456")
                                                .setReferenceNumber("referenceNumber1")
                                                .setProposalTimestamp(TimeUtils.calendarToTimestamp(Calendar.getInstance()))
                                                .setUrl(new URL("http://www.google.sk"))
                                                .setGender(BirthNumber.Gender.M)
                                                .setYearOfBirth(1991)
                                                .setBirthNumberHashCode(7)
                                                .setRegionId(1L)
                                                .setDebtorAddress("adress");
        insolvencyDAO.update(insolvency);
        Insolvency insolvencyFromDb = insolvencyDAO.getInsolvency(insolvency.getId());

        assertNotNull(insolvencyFromDb);
        assertEquals(insolvencyFromDb, insolvency);
    }

    @Override
    public String getDataSetFileName() {
        return "InsolvencyDAOImplTest.xml";
    }
}
