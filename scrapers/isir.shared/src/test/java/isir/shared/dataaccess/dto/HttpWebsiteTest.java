package isir.shared.dataaccess.dto;

import isir.shared.utils.TimeUtils;
import org.junit.Test;

import java.net.URL;
import java.sql.Timestamp;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;

public class HttpWebsiteTest {

    @Test
    public void testGetSetUrl() throws Exception {
        URL url = new URL("http://www.google.sk");
        HttpWebsite httpWebsite = new HttpWebsite().setUrl(url);
        assertEquals(httpWebsite.getUrl(), url);
    }

    @Test
    public void testGetSetContent() throws Exception {
        String actual = "content content";
        HttpWebsite httpWebsite = new HttpWebsite().setContent(actual);
        assertEquals(httpWebsite.getContent(), actual);
    }

    @Test
    public void testGetSetDownloadDate() throws Exception {
        Timestamp downloadDate = TimeUtils.calendarToTimestamp(Calendar.getInstance());
        HttpWebsite httpWebsite = new HttpWebsite().setDownloadDate(downloadDate);
        assertEquals(httpWebsite.getDownloadDate(), downloadDate);
    }
}
