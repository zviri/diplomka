package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dto.InsolvencyAdministrator;
import isir.shared.dataaccess.test.IsirDbTestCase;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class InsolvenciesAdministratorsDAOImplTest extends IsirDbTestCase {

    InsolvenciesAdministratorsDAOImpl insolvenciesAdministratorsDAO = null;

    @Before
    public void setupDao() {
        insolvenciesAdministratorsDAO = new InsolvenciesAdministratorsDAOImpl();
        insolvenciesAdministratorsDAO.setDataSource(getDataSource());
    }

    @Test
    public void testGetFileAdministrator() throws Exception {
        InsolvencyAdministrator fileAdministrator = insolvenciesAdministratorsDAO.getInsolvencyAdministrator("kshkins15518/2013", 1L, 2L);

        assertNotNull(fileAdministrator);
        assertEquals(fileAdministrator.getAdministratorId(), (Long) 1L);
        assertEquals(fileAdministrator.getInsolvencyId(), "kshkins15518/2013");
        assertEquals(fileAdministrator.getAdministratorType(), (Long) 2L);
    }

    @Test
    public void testInsert() throws Exception {
        String insolvencyId = "kshkins11111/2013";
        Long administratorTypeId = 2L;
        Long administratorId = 1L;
        InsolvencyAdministrator insolvencyAdministrator = new InsolvencyAdministrator().setAdministratorId(administratorId)
                                                                                       .setAdministratorType(administratorTypeId)
                                                                                       .setInsolvencyId(insolvencyId);
        insolvenciesAdministratorsDAO.insert(insolvencyAdministrator);
        InsolvencyAdministrator insolvencyAdministratorFromDb = insolvenciesAdministratorsDAO.getInsolvencyAdministrator(insolvencyId, administratorId, administratorTypeId);
        assertNotNull(insolvencyAdministratorFromDb);
        assertEquals(insolvencyAdministratorFromDb.getAdministratorType(), administratorTypeId);
        assertEquals(insolvencyAdministratorFromDb.getAdministratorId(), administratorId);
        assertEquals(insolvencyAdministratorFromDb.getInsolvencyId(), insolvencyId);
    }

    @Test
    public void testDelete() throws Exception {
        insolvenciesAdministratorsDAO.delete("kshkins15518/2013");
        InsolvencyAdministrator fileAdministrator = insolvenciesAdministratorsDAO.getInsolvencyAdministrator("kshkins15518/2013", 1L, 2L);
        assertNull(fileAdministrator);
    }

    @Override
    public String getDataSetFileName() {
        return "InsolvenciesAdministratorsDAOImplTest.xml";
    }
}
