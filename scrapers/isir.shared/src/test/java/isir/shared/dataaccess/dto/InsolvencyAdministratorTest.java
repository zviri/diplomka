package isir.shared.dataaccess.dto;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InsolvencyAdministratorTest {

    @Test
    public void testGetSetInsolvencyId() throws Exception {
        InsolvencyAdministrator insolvency = new InsolvencyAdministrator().setInsolvencyId("insolvencyId");
        assertEquals(insolvency.getInsolvencyId(), "insolvencyId");
    }

    @Test
    public void testGetSetAdministratorId() throws Exception {
        InsolvencyAdministrator insolvency = new InsolvencyAdministrator().setAdministratorId(1L);
        assertEquals(insolvency.getAdministratorId(), new Long(1));
    }

    @Test
    public void testGetSetAdministratorType() throws Exception {
        InsolvencyAdministrator insolvency = new InsolvencyAdministrator().setAdministratorType(1L);
        assertEquals(insolvency.getAdministratorType(), new Long(1L));
    }
}
