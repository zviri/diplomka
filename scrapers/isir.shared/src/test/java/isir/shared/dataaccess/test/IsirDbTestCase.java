package isir.shared.dataaccess.test;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;

public abstract class IsirDbTestCase {

    private IDatabaseTester databaseTester = null;
    BasicDataSource dataSource = null;

    @Before
    public void setUp() throws Exception {
        dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost/isir_test_db");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres");
        databaseTester = new DataSourceDatabaseTester(dataSource);
        databaseTester.setDataSet(getDataSet());
        databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
        databaseTester.setTearDownOperation(DatabaseOperation.DELETE_ALL);
        databaseTester.onSetup();
    }

    @After
    public void tearDown() throws Exception {
        databaseTester.onTearDown();
        dataSource.close();
    }

    protected DataSource getDataSource() {
        return dataSource;
    }

    protected IDataSet getDataSet() throws Exception
    {
        FlatXmlDataSet dataSet = new FlatXmlDataSetBuilder().build(getClass().getResourceAsStream(getDataSetFileName()));
        ReplacementDataSet replacementDataSet = new ReplacementDataSet(dataSet);
        for (Map.Entry<String, String> keyValue : getReplacementKeys().entrySet()) {
            replacementDataSet.addReplacementSubstring(keyValue.getKey(), keyValue.getValue());
        }
        return replacementDataSet;
    }

    public abstract String getDataSetFileName();

    public Map<String, String> getReplacementKeys() {
        return new HashMap<>();
    }
}
