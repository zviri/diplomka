package isir.shared.dataaccess.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import isir.shared.dataaccess.dto.InsolvencyState;
import isir.shared.dataaccess.test.IsirDbTestCase;
import isir.shared.utils.TimeUtils;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

public class InsolvencyStatesDAOImplTest extends IsirDbTestCase {

    InsolvencyStatesDAOImpl insolvencyStatesDAO = null;

    @Before
    public void setupDao() {
        insolvencyStatesDAO = new InsolvencyStatesDAOImpl();
        insolvencyStatesDAO.setDataSource(getDataSource());
    }

    @Test
    public void testGetStateByActionId() throws Exception {
        InsolvencyState state = insolvencyStatesDAO.getInsolvencyStateByActionId(1L);

        assertNotNull(state);
        assertEquals(state.getActionID(), (Long) 1L);
        assertEquals(state.getInsolvencyID(), "ksbrins10001/2012");
        assertEquals(state.getState(), (Long) 2L);
        Calendar changeTimestamp = Calendar.getInstance();
        TimeUtils.resetTime(changeTimestamp);
        changeTimestamp.set(2012, 3, 24, 14, 37, 0);
        assertEquals(state.getStateChangeTimestamp(), TimeUtils.calendarToTimestamp(changeTimestamp));
    }

    @Test
    public void testInsert() throws Exception {
        InsolvencyState state = new InsolvencyState().setActionID(6L)
                                                     .setInsolvencyID("ksbrins10001/2012")
                                                     .setState(3L)
                                                     .setStateChangeTimestamp(TimeUtils.calendarToTimestamp(Calendar.getInstance()));
        insolvencyStatesDAO.insert(state);
        InsolvencyState stateFromDb = insolvencyStatesDAO.getInsolvencyStateByActionId(state.getActionID());

        assertNotNull(stateFromDb);
        assertEquals(stateFromDb, state);
    }

    @Test
    public void testUpdate() throws Exception {
        InsolvencyState state = new InsolvencyState().setActionID(1L)
                                                     .setInsolvencyID("ksbrins10000/2012")
                                                     .setState(3L)
                                                     .setStateChangeTimestamp(TimeUtils.calendarToTimestamp(Calendar.getInstance()));
        insolvencyStatesDAO.update(state);
        InsolvencyState stateFromDb = insolvencyStatesDAO.getInsolvencyStateByActionId(state.getActionID());

        assertNotNull(stateFromDb);
        assertEquals(stateFromDb, state);
    }

    @Test
    public void testGetLastActionID() throws Exception {
        Long lastActionID = insolvencyStatesDAO.getLastActionID();
        assertEquals(lastActionID, (Long) 5L);
    }

    @Override
    public String getDataSetFileName() {
        return "InsolvencyStatesDAOImplTest.xml";
    }
}
