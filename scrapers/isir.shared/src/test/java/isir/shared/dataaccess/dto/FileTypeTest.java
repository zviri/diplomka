package isir.shared.dataaccess.dto;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FileTypeTest {
    @Test
    public void testGetSetId() throws Exception {
        FileType fileType = new FileType().setId(1L);
        assertEquals(fileType.getId(), new Long(1));
    }

    @Test
    public void testGetSetDescription() throws Exception {
        FileType fileType = new FileType().setDescription(" description  description  ");
        assertEquals(fileType.getDescription(), "description description");
    }
}
