package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dto.HttpWebsite;
import isir.shared.dataaccess.test.IsirDbTestCase;
import isir.shared.utils.TimeUtils;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;
import java.sql.Timestamp;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class HttpCacheDAOImplTest extends IsirDbTestCase {

    HttpCacheDAOImpl httpCacheDAO = null;

    @Before
    public void setupDao() {
        httpCacheDAO = new HttpCacheDAOImpl();
        httpCacheDAO.setDataSource(getDataSource());
    }

    @Test
    public void testGetWebsite() throws Exception {
        URL url = new URL("http://www.google.sk");
        HttpWebsite website = httpCacheDAO.getWebsite(url);

        assertNotNull(website);
        assertEquals(website.getUrl(), url);
        assertEquals(website.getContent(), "content1");
        Calendar downloadDate = Calendar.getInstance();
        TimeUtils.resetTime(downloadDate);
        downloadDate.set(2013, 5, 11, 5, 31, 0);
        assertEquals(website.getDownloadDate(), TimeUtils.calendarToTimestamp(downloadDate));
    }

    @Test
    public void testInsertWebsite() throws Exception {
        URL url = new URL("http://www.isir.cz");
        String content = "content2";
        Timestamp downloadDate = new Timestamp(Calendar.getInstance().getTimeInMillis());
        HttpWebsite httpWebsite = new HttpWebsite().setUrl(url)
                                                   .setContent(content)
                                                   .setDownloadDate(downloadDate);
        httpCacheDAO.insertWebsite(httpWebsite);
        HttpWebsite website = httpCacheDAO.getWebsite(url);

        assertNotNull(website);
        assertEquals(website.getUrl(), url);
        assertEquals(website.getContent(), content);
        assertEquals(website.getDownloadDate(), downloadDate);
    }

    @Test
    public void testUpdateWebsite() throws Exception {
        URL url = new URL("http://www.google.sk");
        String content = "content2";
        Timestamp downloadDate = new Timestamp(Calendar.getInstance().getTimeInMillis());
        HttpWebsite httpWebsite = new HttpWebsite().setUrl(url)
                                                   .setContent(content)
                                                   .setDownloadDate(downloadDate);
        httpCacheDAO.updateWebsite(httpWebsite);
        HttpWebsite website = httpCacheDAO.getWebsite(url);

        assertNotNull(website);
        assertEquals(website.getUrl(), url);
        assertEquals(website.getContent(), content);
        assertEquals(website.getDownloadDate(), downloadDate);
    }

    @Override
    public String getDataSetFileName() {
        return "HttpCacheDAOImplTest.xml";
    }
}
