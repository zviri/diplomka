package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dto.AttachedFile;
import isir.shared.dataaccess.test.IsirDbTestCase;
import isir.shared.scraping.InsolvencyDetailPage;
import isir.shared.utils.TimeUtils;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static isir.shared.utils.IsirStringUtils.stringToUrl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FileDAOImplTest extends IsirDbTestCase {

    FileDAOImpl fileDAO = null;

    @Before
    public void setupDao() {
        fileDAO = new FileDAOImpl();
        fileDAO.setDataSource(getDataSource());
    }


    @Override
    public Map<String, String> getReplacementKeys() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map<String, String> keyValues = new HashMap<>();

        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DAY_OF_MONTH, -1);
        yesterday.set(Calendar.HOUR_OF_DAY, 0);
        String yesterdayString = simpleDateFormat.format(yesterday.getTime());
        keyValues.put("${yesterday}", yesterdayString);

        String todayString = simpleDateFormat.format(Calendar.getInstance().getTime());
        keyValues.put("${today}", todayString);

        return keyValues;
    }

    @Test
    public void testGetAttachedFile() throws Exception {
        URL url = new URL("https://isir.justice.cz/isir/doc/dokument.PDF?id=7658934");
        List<AttachedFile> attachedFiles = fileDAO.getFile(url);
        assertEquals(attachedFiles.size(), 1);
        AttachedFile file = attachedFiles.get(0);

        assertNotNull(file);
        assertEquals(file.getDocumentSection(), InsolvencyDetailPage.DocumentSection.A);
        assertEquals(file.getId(), (Long) 1000000L);
        assertEquals(file.getCreditor(), "creditor1");
        assertEquals(file.getFileTypeId(), (Long) 2L);
        assertEquals(file.getInsolvencyId(), "kshkins15518/2013");
        assertEquals(file.getUrl(), url);
        Calendar publishingDate = Calendar.getInstance();
        TimeUtils.resetTime(publishingDate);
        publishingDate.set(2013, Calendar.JUNE, 11, 5, 31, 0);
        assertEquals(new Timestamp(publishingDate.getTimeInMillis()), file.getPublishDate());
    }

    @Test
    public void testGetAttachedFile2() throws Exception {
        Calendar publishingDate = Calendar.getInstance();
        TimeUtils.resetTime(publishingDate);
        publishingDate.set(2013, Calendar.JUNE, 11, 5, 31, 0);
        AttachedFile file = fileDAO.getFile(2L, "kshkins15518/2013", stringToUrl("https://isir.justice.cz/isir/doc/dokument.PDF?id=7658934"));

        assertNotNull(file);
        assertEquals(file.getDocumentSection(), InsolvencyDetailPage.DocumentSection.A);
        assertEquals(file.getId(), (Long) 1000000L);
        assertEquals(file.getCreditor(), "creditor1");
        assertEquals(file.getFileTypeId(), (Long) 2L);
        assertEquals(file.getInsolvencyId(), "kshkins15518/2013");
        URL url = new URL("https://isir.justice.cz/isir/doc/dokument.PDF?id=7658934");
        assertEquals(file.getUrl(), url);
        assertEquals(new Timestamp(publishingDate.getTimeInMillis()), file.getPublishDate());
    }

    @Test
    public void testGetAttachedFile3() throws Exception {
        Calendar publishingDate = Calendar.getInstance();
        TimeUtils.resetTime(publishingDate);
        publishingDate.set(2013, Calendar.JUNE, 11, 5, 31, 0);
        AttachedFile file = fileDAO.getFile(1000000L);

        assertNotNull(file);
        assertEquals(file.getDocumentSection(), InsolvencyDetailPage.DocumentSection.A);
        assertEquals(file.getId(), (Long) 1000000L);
        assertEquals(file.getCreditor(), "creditor1");
        assertEquals(file.getFileTypeId(), (Long) 2L);
        assertEquals(file.getInsolvencyId(), "kshkins15518/2013");
        URL url = new URL("https://isir.justice.cz/isir/doc/dokument.PDF?id=7658934");
        assertEquals(file.getUrl(), url);
        assertEquals(new Timestamp(publishingDate.getTimeInMillis()), file.getPublishDate());
    }

    @Test
    public void testInsertAttachedFile() throws Exception {
        AttachedFile file = new AttachedFile();
        file.setCreditor("creditor2");
        file.setDocumentSection(InsolvencyDetailPage.DocumentSection.P);
        file.setFileTypeId(2L);
        file.setInsolvencyId("insolvency_id2");
        file.setPublishDate(TimeUtils.calendarToTimestamp(Calendar.getInstance()));
        file.setUrl(new URL("http://www.google.sk"));

        fileDAO.insert(file);
        List<AttachedFile> attachedFilesFromDb = fileDAO.getFile(file.getUrl());
        assertEquals(attachedFilesFromDb.size(), 1);
        AttachedFile fileFromDb = attachedFilesFromDb.get(0);

        file.setId(fileFromDb.getId());
        assertNotNull(fileFromDb);
        assertEquals(fileFromDb, file);
    }

    @Test
    public void testUpdateAttachedFile() throws Exception {
        AttachedFile file = new AttachedFile();
        file.setCreditor("creditor2");
        file.setDocumentSection(InsolvencyDetailPage.DocumentSection.P);
        file.setFileTypeId(2L);
        file.setInsolvencyId("insolvency_id2");
        file.setPublishDate(TimeUtils.calendarToTimestamp(Calendar.getInstance()));
        file.setUrl(new URL("http://www.google.sk"));
        file.setId(1000000L);

        fileDAO.update(file);
        List<AttachedFile> attachedFilesFromDb = fileDAO.getFile(file.getUrl());
        assertEquals(attachedFilesFromDb.size(), 1);
        AttachedFile fileFromDb = attachedFilesFromDb.get(0);
        assertNotNull(fileFromDb);
        assertEquals(fileFromDb, file);
    }

    @Test
    public void testGetFilesByInsolvencyId() throws Exception {
        List<AttachedFile> filesByInsolvencyId = fileDAO.getFilesByInsolvencyId("kshkins15518/2013");

        assertEquals(filesByInsolvencyId.size(), 2);
        AttachedFile file = filesByInsolvencyId.get(filesByInsolvencyId.get(0).getId() == 1000000L ? 0 : 1);
        assertEquals(file.getDocumentSection(), InsolvencyDetailPage.DocumentSection.A);
        assertEquals(file.getId(), (Long) 1000000L);
        assertEquals(file.getCreditor(), "creditor1");
        assertEquals(file.getFileTypeId(), (Long) 2L);
        assertEquals(file.getInsolvencyId(), "kshkins15518/2013");
        Calendar publishingDate = Calendar.getInstance();
        TimeUtils.resetTime(publishingDate);
        publishingDate.set(2013, Calendar.JUNE, 11, 5, 31, 0);
        assertEquals(new Timestamp(publishingDate.getTimeInMillis()), file.getPublishDate());
    }

    @Override
    public String getDataSetFileName() {
        return "FileDAOImplTest.xml";
    }
}
