package isir.shared.dataaccess.service.impl;

import isir.shared.dataaccess.dao.*;
import isir.shared.dataaccess.dto.*;
import org.easymock.Capture;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;

import static org.junit.Assert.assertEquals;
import static org.easymock.EasyMock.*;

public class InsolvencyServiceImplTest {

    private IInsolvencyDAO insolvencyDAO;
    private IAdministratorsDAO administratorsDAO;
    private IInsolvenciesAdministratorsDAO insolvenciesAdministratorsDAO;
    private IInsolvencyStatesDAO insolvencyStatesDAO;
    private IInsolvencyStatesTypeDAO insolvencyStatesTypeDAO;
    private IAdministratorTypesDAO administratorTypesDAO;
    private InsolvencyServiceImpl insolvencyService;
    private ISubjectsDAO subjectsDAO;

    @Before
    public void setUp() throws Exception {
        insolvencyDAO = createMock(IInsolvencyDAO.class);
        administratorsDAO = createMock(IAdministratorsDAO.class);
        insolvenciesAdministratorsDAO = createMock(IInsolvenciesAdministratorsDAO.class);
        insolvencyStatesDAO = createMock(IInsolvencyStatesDAO.class);
        insolvencyStatesTypeDAO = createMock(IInsolvencyStatesTypeDAO.class);
        administratorTypesDAO = createMock(IAdministratorTypesDAO.class);
        subjectsDAO = createMock(ISubjectsDAO.class);
        insolvencyService = new InsolvencyServiceImpl(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

    }

    @Test
    public void testSaveAdministrator1() throws Exception {
        Administrator administrator = new Administrator().setId(1L)
                                                         .setAdministrator("administrator1");
        administratorsDAO.update(administrator);
        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(administrator);

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test
    public void testSaveAdministrator2() throws Exception {
        Administrator administrator = new Administrator().setAdministrator("administrator1");
        expect(administratorsDAO.getAdministrator(administrator.getAdministrator())).andReturn(new Administrator().setId(1L));
        administratorsDAO.update(administrator);
        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(administrator);

        assertEquals(administrator.getId(), new Long(1));
        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test
    public void testSaveAdministrator3() throws Exception {
        Administrator administrator = new Administrator().setAdministrator("administrator1");
        expect(administratorsDAO.getAdministrator(administrator.getAdministrator())).andReturn(null);
        administratorsDAO.insert(administrator);
        expect(administratorsDAO.getAdministrator(administrator.getAdministrator())).andReturn(new Administrator().setId(1L));
        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(administrator);

        assertEquals(administrator.getId(), new Long(1));
        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test
    public void testSaveInsolvency1() throws Exception {
        Insolvency insolvency = new Insolvency().setId("insolvencyId1");
        expect(insolvencyDAO.getInsolvency(insolvency.getId())).andReturn(null);
        insolvencyDAO.insert(insolvency);
        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(insolvency);

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test
    public void testSaveInsolvency2() throws Exception {
        Insolvency insolvency = new Insolvency().setId("insolvencyId1");
        expect(insolvencyDAO.getInsolvency(insolvency.getId())).andReturn(new Insolvency());
        insolvencyDAO.update(insolvency);
        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(insolvency);

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test
    public void testSaveInsolvencyState1() throws Exception {
        InsolvencyState insolvencyState = new InsolvencyState().setActionID(1L)
                                                               .setStateChangeTimestamp(new Timestamp(1))
                                                               .setState(2L)
                                                               .setInsolvencyID("insolvencyId");
        expect(insolvencyStatesDAO.getInsolvencyStateByActionId(insolvencyState.getActionID())).andReturn(null);
        insolvencyStatesDAO.insert(insolvencyState);
        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(insolvencyState);

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test
    public void testSaveInsolvencyState2() throws Exception {
        InsolvencyState insolvencyState = new InsolvencyState().setActionID(1L)
                                                               .setStateChangeTimestamp(new Timestamp(1))
                                                               .setState(2L)
                                                               .setInsolvencyID("insolvencyId");
        expect(insolvencyStatesDAO.getInsolvencyStateByActionId(insolvencyState.getActionID())).andReturn(new InsolvencyState());
        insolvencyStatesDAO.update(insolvencyState);
        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(insolvencyState);

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test
    public void testSaveInsolvencyStateType1() throws Exception {
        InsolvencyStateType insolvencyStateType = new InsolvencyStateType().setId(1L)
                                                                           .setTextIdentifier("textIdentifier1");
        insolvencyStatesTypeDAO.update(insolvencyStateType);
        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(insolvencyStateType);
        assertEquals(insolvencyStateType.getId(), new Long(1));

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test
    public void testSaveInsolvencyStateType2() throws Exception {
        InsolvencyStateType insolvencyStateType = new InsolvencyStateType().setTextIdentifier("textIdentifier1");
        expect(insolvencyStatesTypeDAO.getInsolvencyStateType(insolvencyStateType.getTextIdentifier())).andReturn(new InsolvencyStateType().setId(1L));
        insolvencyStatesTypeDAO.update(insolvencyStateType);
        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(insolvencyStateType);
        assertEquals(insolvencyStateType.getId(), new Long(1));

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test
    public void testSaveInsolvencyStateType3() throws Exception {
        InsolvencyStateType insolvencyStateType = new InsolvencyStateType().setTextIdentifier("textIdentifier1");
        expect(insolvencyStatesTypeDAO.getInsolvencyStateType(insolvencyStateType.getTextIdentifier())).andReturn(null);
        insolvencyStatesTypeDAO.insert(insolvencyStateType);
        expect(insolvencyStatesTypeDAO.getInsolvencyStateType(insolvencyStateType.getTextIdentifier())).andReturn(new InsolvencyStateType().setId(1L));
        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(insolvencyStateType);
        assertEquals(insolvencyStateType.getId(), new Long(1));

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test
    public void testSaveAdministratorType1() throws Exception {
        AdministratorType administratorType = new AdministratorType().setId(1L)
                                                                     .setDescription("administrator1");
        administratorTypesDAO.update(administratorType);
        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(administratorType);
        assertEquals(administratorType.getId(), new Long(1));

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test
    public void testSaveAdministratorType2() throws Exception {
        AdministratorType administratorType = new AdministratorType().setDescription("administrator1");
        expect(administratorTypesDAO.getAdministratorType(administratorType.getDescription())).andReturn(new AdministratorType().setId(1L));
        administratorTypesDAO.update(administratorType);
        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(administratorType);
        assertEquals(administratorType.getId(), new Long(1));

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test
    public void testSaveAdministratorType3() throws Exception {
        AdministratorType administratorType = new AdministratorType().setDescription("administrator1");
        expect(administratorTypesDAO.getAdministratorType(administratorType.getDescription())).andReturn(null);
        administratorTypesDAO.insert(administratorType);
        expect(administratorTypesDAO.getAdministratorType(administratorType.getDescription())).andReturn(new AdministratorType().setId(1L));
        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(administratorType);
        assertEquals(administratorType.getId(), new Long(1));

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test
    public void getTestInsolvencyAdministratorTypeAdministrator() throws Exception {
        Insolvency insolvency = new Insolvency().setId("insolvencyId1");
        expect(insolvencyDAO.getInsolvency(insolvency.getId())).andReturn(null);
        insolvencyDAO.insert(insolvency);

        AdministratorType administratorType = new AdministratorType().setId(2L)
                                                                     .setDescription("administrator1");
        administratorTypesDAO.update(administratorType);

        Administrator administrator = new Administrator().setId(3L)
                                                         .setAdministrator("administrator1");
        administratorsDAO.update(administrator);
        expect(insolvenciesAdministratorsDAO.getInsolvencyAdministrator(insolvency.getId(), administrator.getId(), administratorType.getId())).andReturn(new InsolvencyAdministrator());


        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
        insolvencyService.save(insolvency, administratorType, administrator);
        assertEquals(administratorType.getId(), new Long(2));
        assertEquals(administrator.getId(), new Long(3));

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }


    @Test
    public void testSaveInsolvencyInsolvenStateInsolvencyStateType() throws Exception {
        Insolvency insolvency = new Insolvency().setId("insolvencyId1");
        expect(insolvencyDAO.getInsolvency(insolvency.getId())).andReturn(null);
        insolvencyDAO.insert(insolvency);

        InsolvencyState insolvencyState = new InsolvencyState().setActionID(1L)
                                                               .setStateChangeTimestamp(new Timestamp(1))
                                                               .setState(2L)
                                                               .setInsolvencyID("insolvencyId");
        expect(insolvencyStatesDAO.getInsolvencyStateByActionId(insolvencyState.getActionID())).andReturn(null);
        insolvencyStatesDAO.insert(insolvencyState);

        InsolvencyStateType insolvencyStateType = new InsolvencyStateType().setId(2L)
                                                                           .setTextIdentifier("textIdentifier1");
        insolvencyStatesTypeDAO.update(insolvencyStateType);

        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(insolvency, insolvencyState, insolvencyStateType);
        assertEquals(insolvencyStateType.getId(), new Long(2));

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test(expected = IllegalStateException.class)
    public void testSaveSubject1() throws Exception {
        insolvencyService.save(new Subject());
    }

    @Test
    public void testSaveSubject2() throws Exception {
        Subject subject = new Subject().setName("subject2")
                                       .setIco("456789")
                                       .setForm("form2")
                                       .setLegalForm("legal_form2");
        expect(subjectsDAO.getSubject(subject.getName())).andReturn(null);
        Capture<Subject> subjectCapture = new Capture<>();
        subjectsDAO.insert(capture(subjectCapture));

        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(subject);

        assertEquals(subjectCapture.getValue(), subject);

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }

    @Test
    public void testSaveSubject3() throws Exception {
        Subject subject = new Subject().setName("subject2")
                                       .setIco("456789")
                                       .setForm("form2")
                                       .setLegalForm("legal_form2");
        expect(subjectsDAO.getSubject(subject.getName())).andReturn(new Subject().setName("subject2"));
        Capture<Subject> subjectCapture = new Capture<>();
        subjectsDAO.update(capture(subjectCapture));

        replay(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);

        insolvencyService.save(subject);

        assertEquals(subjectCapture.getValue(), subject);

        verify(insolvencyDAO, administratorsDAO, insolvenciesAdministratorsDAO, insolvencyStatesDAO, insolvencyStatesTypeDAO, administratorTypesDAO, subjectsDAO);
    }
}
