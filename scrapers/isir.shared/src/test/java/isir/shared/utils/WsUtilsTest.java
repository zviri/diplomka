package isir.shared.utils;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import isir.shared.isirpub001.IsirPub001Stub;

import org.junit.Test;

public class WsUtilsTest {

    @Test
    public void testGetInsolvencyId() throws Exception {
        IsirPub001Stub.IsirPub001Data pub001Data = createMock(IsirPub001Stub.IsirPub001Data.class);
        expect(pub001Data.getSpisZnacka()).andReturn("INS 1 / 2008");
        expect(pub001Data.getPoznamka()).andReturn("<idOsobyPuvodce>KSSEMOS</idOsobyPuvodce>");
        replay(pub001Data);

        String insolvencyId = WsUtils.getInsolvencyId(pub001Data);

        verify(pub001Data);
        assertEquals(insolvencyId, "ksosins1/2008");
    }

    @Test
    public void testGetInsolvencyState() throws Exception {
        IsirPub001Stub.IsirPub001Data pub001Data = createMock(IsirPub001Stub.IsirPub001Data.class);
        expect(pub001Data.getPoznamka()).andReturn("<druhStavRizeni>ODDLUZENI</druhStavRizeni>");
        replay(pub001Data);

        String insolvencyState = WsUtils.getInsolvencyState(pub001Data);

        verify(pub001Data);
        assertEquals(insolvencyState, "ODDLUZENI");
    }
}
