package isir.shared.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import org.junit.Test;

public class CalendarDayIteratorTest {

    @Test
    public void testIterator() throws Exception {
        Calendar fromTo = Calendar.getInstance();
        CalendarDayIterator calendarDayIterator = new CalendarDayIterator(fromTo, fromTo);
        assertEquals(calendarDayIterator, calendarDayIterator.iterator());
    }

    @Test
    public void testHasNext() throws Exception {
        Calendar fromTo = Calendar.getInstance();
        CalendarDayIterator calendarDayIterator = new CalendarDayIterator(fromTo, fromTo);
        assertTrue(calendarDayIterator.hasNext());
    }

    @Test
    public void testNext() throws Exception {
        Calendar fromTo = Calendar.getInstance();
        CalendarDayIterator calendarDayIterator = new CalendarDayIterator(fromTo, fromTo);
        Calendar next = calendarDayIterator.next();
        assertEquals(next, fromTo);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testRemove() throws Exception {
        Calendar fromTo = Calendar.getInstance();
        CalendarDayIterator calendarDayIterator = new CalendarDayIterator(fromTo, fromTo);
        calendarDayIterator.remove();
    }

    @Test
    public void testGetStart() throws Exception {
        Calendar fromTo = Calendar.getInstance();
        CalendarDayIterator calendarDayIterator = new CalendarDayIterator(fromTo, fromTo);
        assertEquals(calendarDayIterator.getStart(), fromTo);
    }

    @Test
    public void testGetEnd() throws Exception {
        Calendar fromTo = Calendar.getInstance();
        CalendarDayIterator calendarDayIterator = new CalendarDayIterator(fromTo, fromTo);
        assertEquals(calendarDayIterator.getEnd(), fromTo);
    }
}
