package isir.shared.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.junit.Test;

public class IsirStringUtilsTest {

    @Test
    public void testNormalizeId() throws Exception {
        assertEquals(IsirStringUtils.normalizeId("KSOS INS  1 / 2008"), "ksosins1/2008");
    }

    @Test
    public void testNormalizeStringForCompare() throws Exception {
        assertEquals(IsirStringUtils.normalizeStringForCompare("aB      áÁ"), "abaa");
    }

    @Test
    public void testNormalizedCompare() throws Exception {
        assertTrue(IsirStringUtils.normalizedCompare("A b C č Š", "abccs"));
    }

    @Test
    public void testReferenceNumberToInsolvencyId() throws Exception {
        assertEquals(IsirStringUtils.referenceNumberToInsolvencyId("KSOS 2 INS  1 / 2008"), "ksosins1/2008");
    }

    @Test
    public void testDbStringValue() throws Exception {
        assertNull(IsirStringUtils.dbStringValue(" "));
        assertEquals(IsirStringUtils.dbStringValue("a"), "a");
    }

    @Test
    public void testStringToUrl() throws Exception {
        assertEquals(IsirStringUtils.stringToUrl("http://www.google.sk"), new URL("http://www.google.sk"));
        assertNull(IsirStringUtils.stringToUrl("www.google.sk"));
    }

    @Test
    public void testUrlToString() throws Exception {
        assertEquals(IsirStringUtils.urlToString(new URL("http://www.google.sk")), "http://www.google.sk");
    }
}
