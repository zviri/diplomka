package isir.shared.utils;

import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TimeUtilsTest {

    @Test
    public void testIsDayTime() throws Exception {
        Calendar dayTime = Calendar.getInstance();
        dayTime.set(1990, Calendar.MARCH, 23, 14, 0, 0);
        assertTrue(TimeUtils.isDayTime(dayTime));

        Calendar nightTime = Calendar.getInstance();
        nightTime.set(1990, Calendar.MARCH, 23, 23, 0, 0);
        assertFalse(TimeUtils.isDayTime(nightTime));
    }
}
