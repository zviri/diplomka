package isir.shared.utils;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import org.junit.Test;

import com.google.common.util.concurrent.FutureCallback;

public class ListenablePooledExecutorTest {

    @Test
    public void testExecute() throws Exception {
        class TestObject {
            public Integer number;
        }
        final TestObject testObject = new TestObject();
        new ListenablePooledExecutor<>(new ArrayList<Callable<Integer>>() {{
            add(new Callable<Integer>() {
                @Override
                public Integer call() throws Exception {
                    return 2;
                }
            });
        }}, new FutureCallback<Integer>() {
            @Override
            public void onSuccess(Integer result) {
                testObject.number = result;
            }

            @Override
            public void onFailure(Throwable t) {
                //
            }
        }, 1
        ).execute();
        assertEquals(testObject.number, new Integer(2));
    }
}
