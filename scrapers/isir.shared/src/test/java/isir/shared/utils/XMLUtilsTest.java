package isir.shared.utils;

import org.junit.Assert;
import org.junit.Test;

public class XMLUtilsTest {

    @Test
    public void testGetElementValue1() throws Exception {
        String elementValue = XMLUtils.getElementValue("//osoba", "<osoby><osoba>Peter</osoba></osoby>");
        Assert.assertEquals(elementValue, "Peter");
    }

    @Test
    public void testGetElementValue2() throws Exception {
        String elementValue = XMLUtils.getElementValue("//osoba", "<osoby></osoby>");
        Assert.assertEquals(elementValue, null);
    }
}
