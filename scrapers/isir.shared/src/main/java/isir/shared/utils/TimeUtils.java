package isir.shared.utils;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Utility class for working with time.
 */
public class TimeUtils {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Is day time.
     *
     * @return the boolean
     */
    public static boolean isDayTime() {
        return isDayTime(Calendar.getInstance());
    }

    /**
     * Is day time.
     *
     * @param time the time
     * @return the boolean
     */
    public static boolean isDayTime(Calendar time) {
        int hourOfDay = time.get(Calendar.HOUR_OF_DAY);
        return (hourOfDay >= 6 && hourOfDay <= 22);
    }

    /**
     * Reset time.
     *
     * @param calendar the calendar
     */
    public static void resetTime(Calendar calendar) {
        if (calendar == null) {
            return;
        }
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    /**
     * String to calendar.
     *
     * @param stringDate the string date
     * @param dateFormat the date format
     * @return the calendar
     * @throws ParseException the parse exception
     */
    public static Calendar stringToCalendar(String stringDate, SimpleDateFormat dateFormat) throws ParseException {
        if (stringDate == null || dateFormat == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFormat.parse(stringDate));
        return calendar;
    }

    /**
     * String to calendar.
     *
     * @param stringDate the string date
     * @return the calendar
     * @throws ParseException the parse exception
     */
    public static Calendar stringToCalendar(String stringDate) throws ParseException {
        return stringToCalendar(stringDate, dateFormat);
    }

    /**
     * Calendar to string.
     *
     * @param calendar the calendar
     * @return the string
     */
    public static String calendarToString(Calendar calendar) {
        if (calendar == null) {
            return null;
        }
        return dateFormat.format(calendar.getTime());
    }

    /**
     * Calendar to sql date.
     *
     * @param calendar the calendar
     * @return the date
     */
    public static Date calendarToSqlDate(Calendar calendar) {
        if (calendar == null) {
            return null;
        }
        return new Date(calendar.getTimeInMillis());
    }

    /**
     * Calendar to timestamp.
     *
     * @param calendar the calendar
     * @return the timestamp
     */
    public static Timestamp calendarToTimestamp(Calendar calendar) {
        if (calendar == null) {
            return null;
        }
        return new Timestamp(calendar.getTimeInMillis());
    }
}
