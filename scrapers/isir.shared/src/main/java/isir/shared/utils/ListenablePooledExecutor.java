package isir.shared.utils;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

/**
 * Pooled executor for parallel processing.
 * @param <T>  the return type parameter
 */
public class ListenablePooledExecutor<T> {
	
	private static Logger log = LoggerFactory.getLogger(ListenablePooledExecutor.class);

	private List<Callable<T>> tasks;
	private ListeningExecutorService executorService;
	private FutureCallback<T> callback;

    /**
     * Instantiates a new Listenable pooled executor.
     *
     * @param tasks the tasks
     * @param callback the callback
     * @param threadsNumber the threads number
     */
    public ListenablePooledExecutor(List<Callable<T>> tasks, FutureCallback<T> callback, int threadsNumber) {
		this.tasks = tasks;
		this.callback = callback;
		ExecutorService fixedThreadPool = Executors.newFixedThreadPool(threadsNumber);
		executorService = MoreExecutors.listeningDecorator(fixedThreadPool);
	}

    /**
     * Execute tasks in pool.
     */
    public void execute() {
		for (Callable<T> task : tasks) {
			ListenableFuture<T> future = executorService.submit(task);
			Futures.addCallback(future, callback);			
		}
		executorService.shutdown();
		try {
			executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			log.error("", e);
		}
	}

}
