package isir.shared.utils;

import java.util.Calendar;
import java.util.Iterator;

/**
 * Class used for iterating between dates.
 */
public class CalendarDayIterator implements Iterator<Calendar>, Iterable<Calendar> {

	private Calendar start;
	private Calendar end;
	private Calendar iterator;

    /**
     * Instantiates a new Calendar day iterator.
     *
     * @param start the start
     * @param end the end
     */
    public CalendarDayIterator(Calendar start, Calendar end) {
		this.start = (Calendar) start.clone();
		this.end = (Calendar) end.clone();
				
		this.iterator = (Calendar) this.start.clone();		
		// so that we cover the closed interval between [start, end]
		this.iterator.add(Calendar.DATE, -1);
	}

	@Override
	public Iterator<Calendar> iterator() {
		return this;
	}

	@Override
	public boolean hasNext() {
		return iterator.before(end);
	}

	@Override
	public Calendar next() {
		iterator.add(Calendar.DATE, 1);
		return iterator;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("Cannot remove!");
	}

    /**
     * Gets start.
     *
     * @return the start
     */
    public Calendar getStart() {
		return start;
	}

    /**
     * Gets end.
     *
     * @return the end
     */
    public Calendar getEnd() {
		return end;
	}
}
