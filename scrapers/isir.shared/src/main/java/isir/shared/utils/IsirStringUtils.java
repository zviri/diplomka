package isir.shared.utils;

import org.apache.commons.lang3.StringUtils;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Utility class for common string processing.
 */
public class IsirStringUtils {

    /**
     * Normalize id.
     *
     * @param id the id
     * @return the string
     */
    public static String normalizeId(String id) {
        return id.toLowerCase().replaceAll("\\s", "");
    }

    /**
     * Normalize string for compare.
     *
     * @param string the string
     * @return the string
     */
    public static String normalizeStringForCompare(String string) {
        if (string == null) {
            return null;
        }
        return StringUtils.stripAccents(string.toLowerCase().replaceAll("\\s", ""));
    }

    /**
     * Normalized compare.
     *
     * @param string1 the string 1
     * @param string2 the string 2
     * @return the boolean
     */
    public static boolean normalizedCompare(String string1, String string2) {
        if (string1 == null && string2 == null) {
            return true;
        }
        return string1 != null ? normalizeStringForCompare(string1).equals(normalizeStringForCompare(string2)) : normalizeStringForCompare(string2).equals(normalizeStringForCompare(string1));
    }

    /**
     * Converts reference number to insolvency id.
     *
     * @param refNumber the ref number
     * @return the string
     */
    public static String referenceNumberToInsolvencyId(String refNumber) {
        return refNumber.replaceFirst("\\d+", "").toLowerCase().replaceAll("\\s", "");
    }

    /**
     * Db string value.
     *
     * @param string the string
     * @return the string
     */
    public static String dbStringValue(String string) {
        return (string == null || string.replaceAll("\\s", "").isEmpty()) ? null : string;
    }

    /**
     * String to url.
     *
     * @param url the url
     * @return the uRL
     */
    public static URL stringToUrl(String url) {
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            return null;
        }
    }

    /**
     * Url to string.
     *
     * @param url the url
     * @return the string
     */
    public static String urlToString(URL url) {
        return url != null ? url.toString() : null;
    }
}
