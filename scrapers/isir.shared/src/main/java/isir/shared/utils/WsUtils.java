package isir.shared.utils;

import isir.shared.isirpub001.IsirPub001Stub.IsirPub001Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static isir.shared.utils.IsirStringUtils.normalizeId;
import static isir.shared.utils.IsirStringUtils.normalizeStringForCompare;

/**
 * Utility class for working with the isir web service.
 */
public class WsUtils {

    private static Logger log = LoggerFactory.getLogger(WsUtils.class);

    /**
     * Gets insolvency id prefix.
     *
     * @param idOsobyPuvodce the id osoby puvodce
     * @return the insolvency id prefix
     * @throws XPathExpressionException the x path expression exception
     */
    public static String getInsolvencyIdPrefix(String idOsobyPuvodce) throws XPathExpressionException {

        switch (idOsobyPuvodce) {
            case "KSJIMBM":
                return "KSBR";
            case "KSJICCB":
                return "KSCB";
            case "KSVYCHK":
            case "KSVYCHKHK":
                return "KSHK";
            case "KSVYCHKP1":
                return "KSPA";
            case "KSSEMOS":
                return "KSOS";
            case "KSSEMOSP1":
                return "KSOL";
            case "KSZPCPM":
                return "KSPL";
            case "KSSTCAB":
                return "KSPH";
            case "KSSCEUL":
                return "KSUL";
            case "KSSECULP1":
                return "KSLB";
            case "MSPHAAB":
                return "MSPH";
            case "CCA":
                return "CCA"; // System maintanance idOsobyPuvodce, occuring together with "ins-1/-1" spis znacka
            default: {
                log.error("Failed to map idOsobyPuvodce to InsolvencyID prefix, idOsobyPuvodce: " + idOsobyPuvodce);
                return idOsobyPuvodce;
            }
        }
    }

    /**
     * Gets insolvency id.
     *
     * @param data the data
     * @return the insolvency id
     * @throws ParserConfigurationException the parser configuration exception
     * @throws XPathExpressionException the x path expression exception
     * @throws SAXException the sAX exception
     * @throws IOException the iO exception
     */
    public static String getInsolvencyId(IsirPub001Data data) throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr = xpath.compile("//idOsobyPuvodce");
        String idOsobyPuvodce = expr.evaluate(new InputSource(new StringReader(data.getPoznamka())));
        String prefix = getInsolvencyIdPrefix(idOsobyPuvodce);
        String id = prefix + data.getSpisZnacka();
        return normalizeId(id);
    }

    /**
     * As normalized map.
     *
     * @param fileTypes the file types
     * @return the map
     */
    public static Map<String, Object> asNormalizedMap(List<String> fileTypes) {
        Map<String, Object> hashMap = new HashMap<>();
        for (String fileType : fileTypes) {
            hashMap.put(normalizeStringForCompare(fileType), null);
        }
        return hashMap;
    }

    /**
     * Gets insolvency state.
     *
     * @param data the data
     * @return the insolvency state
     * @throws XPathExpressionException the x path expression exception
     */
    public static String getInsolvencyState(IsirPub001Data data) throws XPathExpressionException {
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr = xpath.compile("//druhStavRizeni");
        return expr.evaluate(new InputSource(new StringReader(data.getPoznamka())));
    }
}
