package isir.shared.utils;

/**
 * Class for storing pair values.
 * @param <T>  the type parameter
 * @param <V>  the type parameter
 */
public class Pair<T, V> {

	private T key;
	private V value;

    /**
     * Instantiates a new Pair.
     *
     * @param key the key
     * @param value the value
     */
    public Pair(T key, V value) {
		this.key = key;
		this.value = value;
	}

    /**
     * Gets key.
     *
     * @return the key
     */
    public T getKey() {
		return key;
	}

    /**
     * Gets value.
     *
     * @return the value
     */
    public V getValue() {
		return value;
	}
}
