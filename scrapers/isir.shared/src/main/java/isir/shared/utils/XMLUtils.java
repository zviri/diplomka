package isir.shared.utils;

import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;

/**
 * Utility class for working with XMLs.
 */
public class XMLUtils {

    /**
     * Gets element value.
     *
     * @param xpathExpression the xpath expression
     * @param xml the xml
     * @return the element value
     * @throws XPathExpressionException the x path expression exception
     */
    public static String getElementValue(String xpathExpression, String xml) throws XPathExpressionException {
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr = xpath.compile(xpathExpression);
        String value = expr.evaluate(new InputSource(new StringReader(xml)));
        return "".equals(value) ? null : value;
    }

}
