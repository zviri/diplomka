package isir.shared.utils;

/**
 * Class for storing triples of objects.
 * @param <T>  the type parameter
 * @param <U>  the type parameter
 * @param <V>  the type parameter
 */
public class Trio<T, U, V> {

    T first;
    U second;
    V third;

    /**
     * Instantiates a new Trio.
     *
     * @param first the first
     * @param second the second
     * @param third the third
     */
    public Trio(T first, U second, V third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    /**
     * Gets first.
     *
     * @return the first
     */
    public T getFirst() {
        return first;
    }

    /**
     * Gets second.
     *
     * @return the second
     */
    public U getSecond() {
        return second;
    }

    /**
     * Gets third.
     *
     * @return the third
     */
    public V getThird() {
        return third;
    }
}
