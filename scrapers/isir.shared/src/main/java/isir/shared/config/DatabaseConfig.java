package isir.shared.config;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Database configuration holder as a Singleton.
 */
public final class DatabaseConfig {

	
	private static Logger log = LoggerFactory.getLogger(DatabaseConfig.class);

	private static XMLConfiguration configuration = null;

	private DatabaseConfig() {
		//
	}

	private static void init() {
		if (configuration == null) {
			try {
				configuration = new XMLConfiguration("config/database-config.xml");
			} catch (ConfigurationException e) {
				log.error("Failed to load Database configuration file.", e);
			}
		}
	}
	
	private static String getString(String key) {
		init();
		return configuration.getString(key);
	}

    /**
     * @return JDBC url of the database.
     */
	public static String getDatabaseURL() {
		return getString("database.url");
	}

    /**
     * @return Database user name.
     */
	public static String getUserName() {
		return getString("database.authentification.user-name");
	}

    /**
     * @return Database password.
     */
	public static String getPassword() {
		return getString("database.authentification.password");
	}
}
