package isir.shared.isirpub001;

import java.util.Calendar;

/**
 * Wrapper for calling the isir web service.
 */
public interface IIsirPub001Service {

    /**
     * Get isir pub 001.
     *
     * @param calendar the calendar
     * @return the isir pub 001 stub . isir pub 001 data [ ]
     * @throws Exception the exception
     */
    IsirPub001Stub.IsirPub001Data[] getIsirPub001(Calendar calendar) throws Exception;

    /**
     * Get isir pub 0012.
     *
     * @param actionID the action iD
     * @return the isir pub 001 stub . isir pub 001 data [ ]
     * @throws Exception the exception
     */
    IsirPub001Stub.IsirPub001Data[] getIsirPub0012(long actionID) throws Exception;
}
