package isir.shared.isirpub001;

import isir.shared.isirpub001.IsirPub001Stub.GetIsirPub001;
import isir.shared.isirpub001.IsirPub001Stub.GetIsirPub0012;
import isir.shared.isirpub001.IsirPub001Stub.GetIsirPub0012E;
import isir.shared.isirpub001.IsirPub001Stub.GetIsirPub001E;
import isir.shared.isirpub001.IsirPub001Stub.IsirPub001Data;

import java.rmi.RemoteException;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IsirPub001ServiceImpl implements IIsirPub001Service {

    private static final int WS_RETRIES = 10;
    public static final int CLIENT_TIMEOUT = 300_000;

    private static Logger log = LoggerFactory.getLogger(IsirPub001ServiceImpl.class);

    private IsirPub001Stub stub;

    public IsirPub001ServiceImpl() throws Exception {
        this(CLIENT_TIMEOUT);
    }

    public IsirPub001ServiceImpl(int clientTimeout) throws Exception {
        init(clientTimeout);
    }

    private void init(int clientTimeout) throws org.apache.axis2.AxisFault {
        stub = new IsirPub001Stub();
        log.info("Setting connection timeout to " + clientTimeout + " ms");
        stub._getServiceClient().getOptions().setTimeOutInMilliSeconds(clientTimeout);
    }

    @Override
    public IsirPub001Data[] getIsirPub001(Calendar calendar) throws Exception {
        final GetIsirPub001E getIsirPub001E = new GetIsirPub001E();
        GetIsirPub001 getIsirPub001 = new GetIsirPub001();
        getIsirPub001.setCalendar_1(calendar);
        getIsirPub001E.setGetIsirPub001(getIsirPub001);
        return callWebService(new IsirWsCall() {
            @Override
            public IsirPub001Data[] call() throws RemoteException {
                return stub.getIsirPub001(getIsirPub001E).getGetIsirPub001Response().getResult();
            }
        });
    }

    @Override
    public IsirPub001Data[] getIsirPub0012(long actionID) throws Exception {
        final GetIsirPub0012E getIsirPub0012E = new GetIsirPub0012E();
        GetIsirPub0012 getIsirPub0012 = new GetIsirPub0012();
        getIsirPub0012.setLong_1(actionID);
        getIsirPub0012E.setGetIsirPub0012(getIsirPub0012);
        return callWebService(new IsirWsCall() {
            @Override
            public IsirPub001Data[] call() throws RemoteException {
                return stub.getIsirPub0012(getIsirPub0012E).getGetIsirPub0012Response().getResult();
            }
        });
    }

    private IsirPub001Data[] callWebService(IsirWsCall isirWsCall) throws Exception {
        IsirPub001Data[] isirPub0012Response = null;
        for (int i = 0; i < WS_RETRIES; ++i) {
            try {
                isirPub0012Response = isirWsCall.call();
                break;
            } catch (RemoteException e) {
                log.info("Calling web service failed, trying again in 1 minute!");
                Thread.sleep(60_000);
                init(CLIENT_TIMEOUT);
                if (i == 9) {
                    throw e;
                }
            }
        }
        return isirPub0012Response;
    }

    private static interface IsirWsCall {
        public IsirPub001Data[] call() throws RemoteException;
    }
}
