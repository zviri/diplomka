package isir.shared.scraping;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import isir.shared.dataaccess.dto.HttpWebsite;
import isir.shared.dataaccess.service.HttpCacheService;
import isir.shared.utils.TimeUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLPeerUnverifiedException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Implementation of a cached http downloader.
 */
public class CachedHttpDownloader implements HttpDownloader {

	private static final int DOWNLOAD_PER_MILLIS = 1000;

	private static Logger log = LoggerFactory.getLogger(CachedHttpDownloader.class);

	private String sourceCharset = "windows-1250";
	private int cacheMaxDays = -1;
    private HttpCacheService cacheService;
    private StopWatch watch = new StopWatch();
	private WebClient webClient;
    private long delayAfterFailure = 60_000;

    /**
     * Instantiates a new Cached http downloader.
     *
     * @param sourceCharset the source charset
     * @param cacheMaxDays the cache max days
     * @param cacheService the cache service
     */
    public CachedHttpDownloader(String sourceCharset, int cacheMaxDays, HttpCacheService cacheService) {
        this(sourceCharset, cacheMaxDays, cacheService, null);
        webClient = new WebClient(BrowserVersion.CHROME_16);
        webClient.setJavaScriptEnabled(false);
        webClient.setTimeout(60_000);
	}

    /**
     * Instantiates a new Cached http downloader.
     *
     * @param sourceCharset the source charset
     * @param cacheMaxDays the cache max days
     * @param cacheService the cache service
     * @param webClient the web client
     */
    public CachedHttpDownloader(String sourceCharset, int cacheMaxDays, HttpCacheService cacheService, WebClient webClient) {
		this.sourceCharset = sourceCharset;
		this.cacheMaxDays = cacheMaxDays;
        this.cacheService = cacheService;
        log.info("Cache enabled: {}", isCacheEnabled());
		if (isCacheEnabled()) {
			log.info("Cache max days: {}", cacheMaxDays);
		}
        this.webClient = webClient;
	}

	@Override
    public String getHtmlWebsite(URL url) throws IOException, URISyntaxException {
		if (isCacheEnabled()) {
			HttpWebsite website = cacheService.getWebsite(url);
			if (website != null) {
				Calendar cacheValidFrom = new GregorianCalendar();
				Calendar downloadDate = new GregorianCalendar();
				downloadDate.setTime(website.getDownloadDate());

				TimeUtils.resetTime(cacheValidFrom);
                TimeUtils.resetTime(downloadDate);

				// subtract maximum possible age from today's date
				cacheValidFrom.add(Calendar.DATE, -cacheMaxDays);
				if (!downloadDate.before(cacheValidFrom)) {
					return website.getContent();
				}
				HttpWebsite newWebsite = getHttpWebsite(url);
                cacheService.updateWebsite(newWebsite);
				return newWebsite.getContent();
			}
		}

		HttpWebsite newWebsite = getHttpWebsite(url);
		if (isCacheEnabled()) {
            cacheService.insertWebsite(newWebsite);
		}
		return newWebsite.getContent();
	}

    /**
     * Gets delay after failure.
     *
     * @return the delay after failure
     */
    public long getDelayAfterFailure() {
        return delayAfterFailure;
    }

    /**
     * Sets delay after failure.
     *
     * @param delayAfterFailure the delay after failure
     */
    public void setDelayAfterFailure(long delayAfterFailure) {
        this.delayAfterFailure = delayAfterFailure;
    }

    private boolean isCacheEnabled() {
		return cacheMaxDays != -1;
	}

	private HttpWebsite getHttpWebsite(URL url) throws IOException, URISyntaxException {
		long elapsedTimeBeforeLastDownload = watch.getTime();
		if (elapsedTimeBeforeLastDownload < DOWNLOAD_PER_MILLIS) {
			try {
				Thread.sleep(DOWNLOAD_PER_MILLIS - elapsedTimeBeforeLastDownload);
			} catch (InterruptedException e) {
				log.error("", e);
			}
		}
		watch.reset();
		watch.start();
		Page page;
		try {
			page = webClient.getPage(url);
		} catch (FailingHttpStatusCodeException | SocketTimeoutException | SSLPeerUnverifiedException e) {
			if (e instanceof FailingHttpStatusCodeException && ((FailingHttpStatusCodeException) e).getStatusCode() != 502) {
				throw e;
			} else {
				log.error("Downloading page {} failed! Trying again in {} millis...", url.toString(), delayAfterFailure);
				try {
					Thread.sleep(delayAfterFailure);
				} catch (InterruptedException e1) {
					log.error("", e1);
				}
				page = webClient.getPage(url);
			}
		}
		String content = page.getWebResponse().getContentAsString(sourceCharset);
		return new HttpWebsite().setUrl(url)
                                .setContent(content)
                                .setDownloadDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
	}
}
