package isir.shared.scraping;

import static isir.shared.utils.IsirStringUtils.referenceNumberToInsolvencyId;
import static isir.shared.utils.IsirStringUtils.stringToUrl;
import isir.shared.dataaccess.dto.BirthNumber;
import isir.shared.dataaccess.dto.Insolvency;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for extracting data from the insolvency list page.
 */
public class InsolvenciesListPage {

	private static Logger log = LoggerFactory.getLogger(InsolvenciesListPage.class);

	private static SimpleDateFormat insolvencyListDateFormat = new SimpleDateFormat("dd.MM.yyyy - HH:mm");

	private static final String INSOLVENCY_DETAIL_BASE_URI = "https://isir.justice.cz/isir/ueu/";
	private Document page;

    /**
     * Instantiates a new Insolvencies list page.
     *
     * @param pageHtml the insolvency list page html
     */
    public InsolvenciesListPage(String pageHtml) {
		page = Jsoup.parse(pageHtml);
		page.setBaseUri(INSOLVENCY_DETAIL_BASE_URI);
	}

    /**
     * Extract insolvencies from the page from the given date.
     *
     * @param date the date
     * @return the insolvencies
     */
    public List<Insolvency> getInsolvencies(Calendar date) {
		List<Insolvency> insolvencies = new ArrayList<>();
		for (Element listRow : page.select("table.vysledekLustrace:eq(0) tr:gt(0)")) {
			try {
				Calendar rowDate = Calendar.getInstance();
				rowDate.setTime(insolvencyListDateFormat.parse(listRow.select("td:eq(6)").first().text().trim()));
				if (equalsOnDate(date, rowDate)) {
					String referenceNumber = listRow.select("td:eq(0)").first().text().trim();
					referenceNumber += " "+listRow.select("td:eq(1)").first().text().trim();
					referenceNumber += " "+listRow.select("td:eq(2)").first().text().trim();
					referenceNumber += " "+listRow.select("td:eq(3)").first().text().trim();
					referenceNumber += " "+listRow.select("td:eq(4)").first().text().trim();
					Insolvency insolvency = new Insolvency().setUrl(stringToUrl(listRow.select("td:eq(7) > a").first().attr("abs:href")))
									                        .setId(referenceNumberToInsolvencyId(referenceNumber))
									                        .setReferenceNumber(referenceNumber)
									                        .setDebtorName(listRow.select("td:eq(7)").first().text().trim())
									                        .setProposalTimestamp(new Timestamp(rowDate.getTimeInMillis()))
									                        .setIco(listRow.select("td:eq(8)").first().text().trim());
					
					String birthNumberString = listRow.select("td:eq(9)").first().text().trim();
					if (!birthNumberString.isEmpty()) {
						BirthNumber birthNumber = new BirthNumber(birthNumberString);
						insolvency.setBirthNumberHashCode(birthNumber.hashCode())
								  .setGender(birthNumber.getGender())
								  .setYearOfBirth(birthNumber.getYearOfBirth());
					}
					insolvencies.add(insolvency);
				}
			} catch (Exception e) {
				log.error("Failed to parse insolvency detail", e);
			}
		}
		return insolvencies;
	}

	private boolean equalsOnDate(Calendar date, Calendar rowDate) {
		return rowDate.get(Calendar.DAY_OF_YEAR) == date.get(Calendar.DAY_OF_YEAR);
	}
}

