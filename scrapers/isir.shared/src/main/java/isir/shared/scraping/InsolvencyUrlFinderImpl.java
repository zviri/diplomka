package isir.shared.scraping;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * The implementation of the insolvency url finder using the search form of the Insolvency Register.
 */
public class InsolvencyUrlFinderImpl implements InsolvencyUrlFinder {

    private WebClient webClient;

    /**
     * Instantiates a new Insolvency url finder impl.
     */
    public InsolvencyUrlFinderImpl() {
        webClient = new WebClient(BrowserVersion.CHROME_16);
        webClient.setJavaScriptEnabled(false);
        webClient.setTimeout(60_000);
    }

    /**
     * Instantiates a new Insolvency url finder impl.
     *
     * @param webClient the web client
     */
    public InsolvencyUrlFinderImpl(WebClient webClient) {
        this.webClient = webClient;
    }

    @Override
    public List<URL> getInsolvencyURLs(Integer matterNumber, Integer matterYear) throws IOException {
        HtmlPage searchPage = webClient.getPage("https://isir.justice.cz/isir/common/index.do");
        HtmlForm searchForm = searchPage.getFormByName("podminky_lustrace_form");
        HtmlInput vecInput = searchForm.getInputByName("bc_vec");
        vecInput.setValueAttribute("" + matterNumber);
        HtmlInput rocnikInput = searchForm.getInputByName("rocnik");
        rocnikInput.setValueAttribute("" + matterYear);
        HtmlPage searchResultPage = searchForm.getInputByValue("Potvrzení výběru").click();
        ArrayList<URL> newUrls = new ArrayList<>();
        for (HtmlAnchor htmlAnchor : searchResultPage.getAnchors()) {
            if ("Detail".equals(htmlAnchor.asText())) {
                newUrls.add(searchResultPage.getFullyQualifiedUrl(htmlAnchor.getHrefAttribute()));
            }
        }
        return newUrls;
    }
}
