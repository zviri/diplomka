package isir.shared.scraping;

import java.io.IOException;
import java.net.URL;
import java.util.List;


/**
 * The interface for a class used for finding the insolvency's new url after it was changed.
 */
public interface InsolvencyUrlFinder {

    /**
     * Gets insolvency new urls.
     *
     * @param matterNumber the matter number
     * @param matterYear the matter year
     * @return the insolvency uR ls
     * @throws IOException the iO exception
     */
    List<URL> getInsolvencyURLs(Integer matterNumber, Integer matterYear) throws IOException;
}
