package isir.shared.scraping;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

/**
 * The interface for the isir html scraper/
 */
public interface IsirHtmlScraper {

    /**
     * Runs the scraper on the insolvency proceedings started between the two dates.
     *
     * @param dateFrom the date from
     * @param dateTo the date to
     * @throws Exception the exception
     */
    void run(Calendar dateFrom, Calendar dateTo) throws Exception;

    /**
     * Runs the scraper on the giver insolvency detail urls.
     *
     * @param insolvencyDetailsUrls the insolvency details urls
     * @throws Exception the exception
     */
    void run(List<URL> insolvencyDetailsUrls) throws Exception;

    /**
     * Gets urls that the scraper failed to process.
     *
     * @return the failed urls
     */
    List<URL> getFailedUrls();
}
