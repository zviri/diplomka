package isir.shared.scraping;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Simple interface for a HttpDownloader.
 */
public interface HttpDownloader {
    /**
     * Gets html website.
     *
     * @param url the url
     * @return the html website
     * @throws IOException the iO exception
     * @throws URISyntaxException the uRI syntax exception
     */
    String getHtmlWebsite(URL url) throws IOException, URISyntaxException;
}
