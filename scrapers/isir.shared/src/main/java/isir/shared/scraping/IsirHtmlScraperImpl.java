package isir.shared.scraping;

import isir.shared.dataaccess.dto.*;
import isir.shared.dataaccess.service.FilesService;
import isir.shared.dataaccess.service.InsolvencyService;
import isir.shared.exceptions.InvalidInsolvencyDetailsPageException;
import isir.shared.utils.CalendarDayIterator;
import isir.shared.utils.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * The implementation of the isir html scraper.
 */
public class IsirHtmlScraperImpl implements IsirHtmlScraper {

	private static Logger log = LoggerFactory.getLogger(IsirHtmlScraperImpl.class);
	
	private static SimpleDateFormat queryStringDateFormat = new SimpleDateFormat("dd.MM.yyyy");

	private HttpDownloader downloader;
    private InsolvencyService insolvencyService;
	private List<URL> failedUrls;
    private FilesService filesService;

    /**
     * Instantiates a new Isir html scraper impl.
     *
     * @param insolvencyService the insolvency service
     * @param filesService the files service
     * @param downloader the downloader
     */
    public IsirHtmlScraperImpl(InsolvencyService insolvencyService, FilesService filesService, HttpDownloader downloader) {
		super();
        this.insolvencyService = insolvencyService;
        this.filesService = filesService;
		this.downloader = downloader;
	}
	
	@Override
    public void run(Calendar dateFrom, Calendar dateTo) throws Exception {
		log.info("Start Date: " + dateFrom.getTime().toString());
		log.info("End Date:" + dateTo.getTime().toString());
        CalendarDayIterator calendarIterator = new CalendarDayIterator(dateFrom, dateTo);
		failedUrls = new ArrayList<>();
		while (calendarIterator.hasNext()) {
			Calendar date = calendarIterator.next();
			log.info("Processign date: {}", date.getTime());
			Calendar date13daysAhead = Calendar.getInstance();
			date13daysAhead.setTimeInMillis(date.getTimeInMillis());
			date13daysAhead.add(Calendar.DATE, 13);
			String insolvenciesListUrlString = String.format("https://isir.justice.cz/isir/ueu/vysledek_lustrace.do?spis_znacky_datum=%s&spis_znacky_obdobi=14DNI", queryStringDateFormat.format(date13daysAhead.getTime()));
			URL insolvenciesListUrl = new URL(insolvenciesListUrlString);
			InsolvenciesListPage insolvenciessListPage = new InsolvenciesListPage(downloader.getHtmlWebsite(insolvenciesListUrl));
			List<Insolvency> insolvencies = insolvenciessListPage.getInsolvencies(date);
			runOnInsolvencies(insolvencies, failedUrls);
		}
	}
	
	@Override
    public void run(List<URL> insolvencyDetailsUrls) throws Exception {
		failedUrls = new ArrayList<>();
		run(insolvencyDetailsUrls, failedUrls, null);
	}
	
	private void runOnInsolvencies(List<Insolvency> insolvencies, List<URL> failedUrls) throws Exception {
		for (Insolvency insolvency : insolvencies) {
			processInsolvency(insolvency.getUrl(), failedUrls, insolvency);
		}
	}

	private void run(List<URL> insolvencyDetailsUrls, List<URL> failedUrls, Insolvency alreadyParsedInsolvency) throws Exception {
		for (URL url : insolvencyDetailsUrls) {
			processInsolvency(url, failedUrls, alreadyParsedInsolvency);
		}
	}

	private void processInsolvency(URL url, List<URL> failedUrls, Insolvency alreadyParsedInsolvency) throws IOException, URISyntaxException {
		log.info("Processing insolvency {}", url);
		InsolvencyDetailPage insolvencyDetailPage;
		Insolvency insolvency;
		try {
			insolvencyDetailPage = new InsolvencyDetailPage(downloader.getHtmlWebsite(url));
			insolvency = insolvencyDetailPage.getInsolvency();
			insolvency.setUrl(url);			
			insolvencyService.save(insolvency);
			for (Pair<AttachedFile,FileType> fileFileTypePair : insolvencyDetailPage.getAttachedFiles()) {
                AttachedFile file = fileFileTypePair.getKey();
                if (file.getUrl() == null) {
                    continue;
                }
                file.setInsolvencyId(insolvency.getId());
				saveFile(fileFileTypePair);
			}
			for (Pair<AdministratorType, Administrator> administratorTypeAdministratorPair : insolvencyDetailPage.getAdministrators()) {
                AdministratorType administratorType = administratorTypeAdministratorPair.getKey();
                Administrator administrator = administratorTypeAdministratorPair.getValue();
                insolvencyService.save(insolvency, administratorType, administrator);
			}
		} catch (InvalidInsolvencyDetailsPageException e) {
			log.warn("Insolvency page is invalid: {}", url);
			failedUrls.add(url);
			if (alreadyParsedInsolvency == null) {
				return;
			}
			Insolvency existingInsolvency = insolvencyService.getInsolvency(alreadyParsedInsolvency.getId());
			insolvency = alreadyParsedInsolvency;
			if (existingInsolvency != null) {
				insolvency.setDebtorAddress(existingInsolvency.getDebtorAddress());
			}
			insolvency.setUrl(null);
			insolvencyService.save(insolvency);
		}
	}

    private void saveFile(Pair<AttachedFile, FileType> fileFileTypePair) throws URISyntaxException {
        AttachedFile file = fileFileTypePair.getKey();
        FileType fileType = fileFileTypePair.getValue();
        filesService.save(fileType);
        file.setFileTypeId(fileType.getId());
        filesService.save(file);
	}
	
	@Override
    public List<URL> getFailedUrls() {
		return failedUrls;
	}
}
