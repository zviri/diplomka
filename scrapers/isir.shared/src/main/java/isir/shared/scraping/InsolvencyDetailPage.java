package isir.shared.scraping;

import isir.shared.dataaccess.dto.*;
import isir.shared.exceptions.InvalidBirdNumberException;
import isir.shared.exceptions.InvalidInsolvencyDetailsPageException;
import isir.shared.utils.IsirStringUtils;
import isir.shared.utils.Pair;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static isir.shared.utils.IsirStringUtils.*;

/**
 * Class for extracting data from insolvency detail page.
 */
public class InsolvencyDetailPage {

	private static Logger log = LoggerFactory.getLogger(InsolvencyDetailPage.class);

    private static SimpleDateFormat insolvencyDocumentsDateFormat = new SimpleDateFormat("dd.MM.yyyy-HH:mm");

	private Document page;

    /**
     * Instantiates a new Insolvency detail page.
     *
     * @param htmlPage the html page
     * @throws InvalidInsolvencyDetailsPageException the invalid insolvency details page exception
     */
    public InsolvencyDetailPage(String htmlPage) throws InvalidInsolvencyDetailsPageException {
        String BASE_URL = "https://isir.justice.cz/";
        page = Jsoup.parse(htmlPage);
        page.setBaseUri(BASE_URL);
		checkInsolvencyPage();
	}

    /**
     * Extracts insolvency details from the page.
     *
     * @return the insolvency
     * @throws InvalidInsolvencyDetailsPageException the invalid insolvency details page exception
     */
    public Insolvency getInsolvency() throws InvalidInsolvencyDetailsPageException {
		Element detailsTable = page.select("table.evidenceUpadcuDetail").first();
		Insolvency insolvency = new Insolvency();

        insolvency.setReferenceNumber(detailsTable.select("td:contains(Spisová značka) + td > strong").first().text().trim());
		insolvency.setDebtorName(detailsTable.select("td:contains(Jméno/název) + td").first().text().trim());

		String birthNumberAndBirdDate = StringUtils.deleteWhitespace(detailsTable.select("td:contains(Rodné číslo / Datum nar.) + td").first().text());
		if (!birthNumberAndBirdDate.equals("/")) {
			String birthNumberString = birthNumberAndBirdDate.substring(0, birthNumberAndBirdDate.lastIndexOf("/"));
            String birthDateString = birthNumberAndBirdDate.substring(birthNumberAndBirdDate.lastIndexOf("/") + 1);
            if (!birthNumberString.isEmpty()) {
                try {
                    BirthNumber birdNumber = new BirthNumber(birthNumberString);
                    insolvency.setGender(birdNumber.getGender());
                    insolvency.setBirthNumberHashCode(birdNumber.hashCode());
                    insolvency.setYearOfBirth(birdNumber.getYearOfBirth());
                } catch (InvalidBirdNumberException e) {
                    log.error("Failed to process BirthNumber {}", birthNumberString);
                }
            } else if (!birthDateString.isEmpty()) {
                String[] birthDateSplit = birthDateString.split("\\.");
                assert birthDateSplit.length == 3;
                insolvency.setYearOfBirth(Integer.parseInt(birthDateSplit[2]));
            }
		}
		Element debtorAddress = detailsTable.select("td:contains(Bydliště) + td").first();
        if (debtorAddress == null) {
            debtorAddress = detailsTable.select("td:contains(Sídlo společnosti) + td").first();
        }
		if (debtorAddress != null) {
			insolvency.setDebtorAddress(debtorAddress.text().trim());
		}
		insolvency.setIco(detailsTable.select("td:contains(IČ) + td").first().ownText().replaceAll("[^\\d+]", ""));
		insolvency.setId(referenceNumberToInsolvencyId(insolvency.getReferenceNumber()));

		Elements documentsTableA = page.select("div#zalozkaA table.evidenceUpadcuDetailTable");
		String date = documentsTableA.select("td:contains(1.) + td").first().text().trim();
		String time = documentsTableA.select("td:contains(1.) + td + td").first().text().trim();
		insolvency.setProposalTimestamp(parseDocumentPublishingDate(date + "-" + time));
		return insolvency;
	}

    /**
     * Extracts list of administrators from the insolvency details page.
     *
     * @return the administrators
     */
    public List<Pair<AdministratorType, Administrator>> getAdministrators() {
        List<Pair<AdministratorType, Administrator>> administrators = new ArrayList<>();
		Elements administratorsElements = page.select("table.evidenceUpadcuDetail td:contains(správce) + td");
		for (Element administratorElement : administratorsElements) {
			String administratorTypeDescription = administratorElement.previousElementSibling().text().trim();
			AdministratorType administratorType = new AdministratorType().setDescription(administratorTypeDescription);
			Administrator administrator = new Administrator().setAdministrator(administratorElement.text().trim());
			administrators.add(new Pair<>(administratorType, administrator));
		}
		return administrators;
	}

    /**
     * Extracts files from the file listings of the insolvency detail page.
     *
     * @return the attached files
     * @throws InvalidInsolvencyDetailsPageException the invalid insolvency details page exception
     */
    public List<Pair<AttachedFile, FileType>> getAttachedFiles() throws InvalidInsolvencyDetailsPageException {
		Insolvency insolvency = getInsolvency();
		List<Pair<AttachedFile, FileType>> attachedFiles = new ArrayList<>();
		processTable(insolvency, attachedFiles, DocumentSection.A, page.select("div#zalozkaA table.evidenceUpadcuDetailTable").first());
		processTable(insolvency, attachedFiles, DocumentSection.B, page.select("div#zalozkaB table.evidenceUpadcuDetailTable").first());
		processTable(insolvency, attachedFiles, DocumentSection.C, page.select("div#zalozkaC table.evidenceUpadcuDetailTable").first());
		processTable(insolvency, attachedFiles, DocumentSection.D, page.select("div#zalozkaD table.evidenceUpadcuDetailTable").first());
		processCreditorsTable(insolvency, attachedFiles, DocumentSection.P, page.select("div#zalozkaP table.evidenceUpadcuDetailTable").first());
		return attachedFiles;
	}
	
	private void checkInsolvencyPage() throws InvalidInsolvencyDetailsPageException {
		Elements errorSpans = page.select("table.mainTable span.errorPage");
		if (errorSpans.size() > 0) {
			for (Element errorSpan : errorSpans) {
				if (IsirStringUtils.normalizedCompare(errorSpan.text(), "Zvolili jste neplatný odkaz. Opakujte dotaz ZDE")) {
					throw new InvalidInsolvencyDetailsPageException("Page does not exists anymore.");
				}
			}
		}
	}

	private Timestamp parseDocumentPublishingDate(String dateString) {
		try {
			Date publishingDate = insolvencyDocumentsDateFormat.parse(dateString);
			return new Timestamp(publishingDate.getTime());
		} catch (ParseException e) {
			log.error("Failed to parse document publishing date: " + dateString, e);
			return null;
		}
	}

	private void processTable(Insolvency insolvency, List<Pair<AttachedFile, FileType>> attachedFiles, DocumentSection documentSection, Element table) {
		for (Element row : table.select("tr:gt(0)")) {
			List<Pair<AttachedFile, FileType>> fileFileTypes = processCommonCells(insolvency, row);
			for (Pair<AttachedFile, FileType> fileFileType : fileFileTypes) {
				fileFileType.getKey().setDocumentSection(documentSection);
				attachedFiles.add(fileFileType);
			}
		}
	}

	private void processCreditorsTable(Insolvency insolvency, List<Pair<AttachedFile, FileType>> attachedFiles, DocumentSection documentSection, Element tableA) {
		for (Element row : tableA.select("tr:gt(0)")) {
			List<Pair<AttachedFile, FileType>> fileFileTypes = processCommonCells(insolvency, row);
			for (Pair<AttachedFile, FileType> fileFileType : fileFileTypes) {
                AttachedFile file = fileFileType.getKey();
                file.setDocumentSection(documentSection);
				Elements cells = row.select("td");
				Elements creditorSpan = cells.get(8).select("span");
				if (creditorSpan.size() > 0) {
                    file.setCreditor(creditorSpan.first().text().trim());
				}
				attachedFiles.add(fileFileType);
			}
		}
	}

	private List<Pair<AttachedFile, FileType>> processCommonCells(Insolvency insolvency, Element row) {
		List<Pair<AttachedFile, FileType>> files = new ArrayList<>();
		Elements cells = row.select("td");

		String dateTime = StringUtils.deleteWhitespace(cells.get(1).text() + "-" + cells.get(2).text());
		Timestamp publishDate = parseDocumentPublishingDate(dateTime);

		String fileURLString = cells.get(5).select("a").attr("abs:href");
		URL fileUrl = null;
		if (fileURLString != null && !fileURLString.trim().isEmpty()) {
			fileUrl = stringToUrl(fileURLString);
		}
		
		Elements multiCell = cells.get(3).select("span");
		for (Element element : multiCell) {
            AttachedFile attachedFile = new AttachedFile().setInsolvencyId(insolvency.getId())
                                                          .setPublishDate(publishDate)
                                                          .setUrl(fileUrl);
            FileType fileType = new FileType().setDescription(element.text().trim());
            files.add(new Pair<>(attachedFile, fileType));
		}
		return files;
	}

    /**
     * The enum Document section.
     */
    public static enum DocumentSection {
        /**
         * The A.
         */
        A, /**
         * The B.
         */
        B, /**
         * The C.
         */
        C, /**
         * The D.
         */
        D, /**
         * The P.
         */
        P
	}
}
