package isir.shared.exceptions;

/**
 * Exceptions indicating that the bird number does not have the valid format.
 */
public class InvalidBirdNumberException extends Exception {

	private static final long serialVersionUID = -1252095644105731094L;

}
