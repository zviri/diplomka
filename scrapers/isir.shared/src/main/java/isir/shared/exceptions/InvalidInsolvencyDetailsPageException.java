package isir.shared.exceptions;

/**
 * Exception indicating that the insolvency detail page is not valid.
 */
public class InvalidInsolvencyDetailsPageException extends Exception {

    /**
     * Instantiates a new Invalid insolvency details page exception.
     *
     * @param message the message
     */
    public InvalidInsolvencyDetailsPageException(String message) {
		super(message);
	}

	private static final long serialVersionUID = -4156134762654172683L;

}
