package isir.shared.dataaccess.dao;

import isir.shared.dataaccess.dto.AdministratorType;


/**
 * DAO interface for working with administrator types.
 */
public interface IAdministratorTypesDAO {

    /**
     * Gets administrator type.
     *
     * @param description the description
     * @return the administrator type
     */
    AdministratorType getAdministratorType(String description);

    /**
     * Inserts new administrator type.
     *
     * @param administratorType the administrator type
     */
    void insert(AdministratorType administratorType);

    /**
     * Updates existing administrator type.
     *
     * @param administratorType the administrator type
     */
    void update(AdministratorType administratorType);
}
