package isir.shared.dataaccess.dao;

import isir.shared.dataaccess.dao.impl.DAOFactoryImpl;
import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

import javax.sql.DataSource;
import java.sql.Connection;

import static isir.shared.config.DatabaseConfig.*;


/**
 * The DAO factory for obtaining DAO objects.
 */
public abstract class DAOFactory {
	
	private DataSource dataSource;

    /**
     * Instantiates a new DAO factory.
     *
     * @param driver the driver
     */
    public DAOFactory(String driver) {
		dataSource = setupDataSource(driver);
	}

    /**
     * Gets data source.
     *
     * @return the data source
     */
    protected DataSource getDataSource() {
		return dataSource;
	}

    /**
     * Gets http cache DAO.
     *
     * @return the http cache DAO
     */
    public abstract IHttpCacheDAO getHttpCacheDAO();

    /**
     * Gets insolvency DAO.
     *
     * @return the insolvency DAO
     */
    public abstract IInsolvencyDAO getInsolvencyDAO();

    /**
     * Gets file DAO.
     *
     * @return the file DAO
     */
    public abstract IFileDAO getFileDAO();

    /**
     * Gets insolvency states type DAO.
     *
     * @return the insolvency states type DAO
     */
    public abstract IInsolvencyStatesTypeDAO getInsolvencyStatesTypeDAO();

    /**
     * Gets insolvency states DAO.
     *
     * @return the insolvency states DAO
     */
    public abstract IInsolvencyStatesDAO getInsolvencyStatesDAO();

    /**
     * Gets insolvencies administrators DAO.
     *
     * @return the insolvencies administrators DAO
     */
    public abstract IInsolvenciesAdministratorsDAO getInsolvenciesAdministratorsDAO();

    /**
     * Gets administrators DAO.
     *
     * @return the administrators DAO
     */
    public abstract IAdministratorsDAO getAdministratorsDAO();

    /**
     * Gets ws events DAO.
     *
     * @return the ws events DAO
     */
    public abstract IWsEventsDAO getWsEventsDAO();

    /**
     * Gets file type DAO.
     *
     * @return the file type DAO
     */
    public abstract IFileTypeDAO getFileTypeDAO();

    /**
     * Gets administrator types DAO.
     *
     * @return the administrator types DAO
     */
    public abstract IAdministratorTypesDAO getAdministratorTypesDAO();

    /**
     * Gets subjects DAO.
     *
     * @return the subjects DAO
     */
    public abstract ISubjectsDAO getSubjectsDAO();

    /**
     * Gets DAO factory instance.
     *
     * @return DAO DAO factory
     */
    public static DAOFactory getDAOFactory() {
		return new DAOFactoryImpl("org.postgresql.Driver");
	}

	private DataSource setupDataSource(String driver) {

		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		GenericObjectPool<Connection> connectionPool = new GenericObjectPool<Connection>();
		ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(getDatabaseURL(), getUserName(), getPassword());
		new PoolableConnectionFactory(connectionFactory, connectionPool, null, null, false, true);
		PoolingDataSource poolingDataSource = new PoolingDataSource(connectionPool);
		return poolingDataSource;
	}
}
