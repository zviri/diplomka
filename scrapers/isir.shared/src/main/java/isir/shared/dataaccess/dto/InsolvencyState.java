package isir.shared.dataaccess.dto;

import java.sql.Timestamp;

/**
 * The Insolvency state DTO.
 */
public class InsolvencyState {
	
	Long state;
    Long actionID;
	String insolvencyID;
	Timestamp stateChangeTimestamp;

    /**
     * Gets state.
     *
     * @return the state
     */
    public Long getState() {
		return state;
	}

    /**
     * Gets action iD.
     *
     * @return the action iD
     */
    public Long getActionID() {
		return actionID;
	}

    /**
     * Gets insolvency iD.
     *
     * @return the insolvency iD
     */
    public String getInsolvencyID() {
		return insolvencyID;
	}

    /**
     * Gets state change timestamp.
     *
     * @return the state change timestamp
     */
    public Timestamp getStateChangeTimestamp() {
		return stateChangeTimestamp;
	}

    /**
     * Sets state.
     *
     * @param state the state
     * @return the state
     */
    public InsolvencyState setState(Long state) {
		this.state = state;
		return this;
	}

    /**
     * Sets action iD.
     *
     * @param actionID the action iD
     * @return the action iD
     */
    public InsolvencyState setActionID(Long actionID) {
		this.actionID = actionID;
		return this;
	}

    /**
     * Sets insolvency iD.
     *
     * @param insolvencyID the insolvency iD
     * @return the insolvency iD
     */
    public InsolvencyState setInsolvencyID(String insolvencyID) {
		this.insolvencyID = insolvencyID;
		return this;
	}

    /**
     * Sets state change timestamp.
     *
     * @param stateChangeTimestamp the state change timestamp
     * @return the state change timestamp
     */
    public InsolvencyState setStateChangeTimestamp(Timestamp stateChangeTimestamp) {
		this.stateChangeTimestamp = stateChangeTimestamp;
		return this;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InsolvencyState that = (InsolvencyState) o;

        if (actionID != null ? !actionID.equals(that.actionID) : that.actionID != null) return false;
        if (insolvencyID != null ? !insolvencyID.equals(that.insolvencyID) : that.insolvencyID != null) return false;
        if (state != null ? !state.equals(that.state) : that.state != null) return false;
        if (stateChangeTimestamp != null ? !stateChangeTimestamp.equals(that.stateChangeTimestamp) : that.stateChangeTimestamp != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = state != null ? state.hashCode() : 0;
        result = 31 * result + (actionID != null ? actionID.hashCode() : 0);
        result = 31 * result + (insolvencyID != null ? insolvencyID.hashCode() : 0);
        result = 31 * result + (stateChangeTimestamp != null ? stateChangeTimestamp.hashCode() : 0);
        return result;
    }
}
