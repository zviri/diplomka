package isir.shared.dataaccess.dto;

import org.apache.commons.lang3.StringUtils;

/**
 * The Administrator DTO.
 */
public class Administrator {

	private String administrator;
	private Long id;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
		return id;
	}

    /**
     * Gets administrator.
     *
     * @return the administrator
     */
    public String getAdministrator() {
		return administrator;
	}

    /**
     * Sets administrator.
     *
     * @param administrator the administrator
     * @return the administrator
     */
    public Administrator setAdministrator(String administrator) {
		this.administrator = StringUtils.normalizeSpace(administrator);
		return this;
	}

    /**
     * Sets id.
     *
     * @param id the id
     * @return the id
     */
    public Administrator setId(Long id) {
		this.id = id;
		return this;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Administrator that = (Administrator) o;

        if (administrator != null ? !administrator.equals(that.administrator) : that.administrator != null)
            return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = administrator != null ? administrator.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }
}
