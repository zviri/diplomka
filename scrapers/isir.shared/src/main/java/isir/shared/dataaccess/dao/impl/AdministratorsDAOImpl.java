package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dao.IAdministratorsDAO;
import isir.shared.dataaccess.dto.Administrator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * The AdministratorsDAO PostgreSQL implementation.
 */
public class AdministratorsDAOImpl extends JdbcDaoSupport implements IAdministratorsDAO {

	@Override
	public Administrator getAdministrator(String administrator) {
		List<Administrator> administrators = getJdbcTemplate().query("SELECT * FROM administrators_tab WHERE administrator = ?", new Object[] { administrator }, new RowMapper<Administrator>() {
			@Override
			public Administrator mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Administrator().setId(rs.getLong("id"))
										  .setAdministrator(rs.getString("administrator"));
			}
		});
		
		return administrators.size() == 1 ? administrators.get(0) : null;
	}

    @Override
    public void update(Administrator administrator) {
        getJdbcTemplate().update("UPDATE administrators_tab SET administrator=? WHERE id=?", administrator.getAdministrator(), administrator.getId());
    }

    @Override
	public void insert(Administrator administrator) {
		getJdbcTemplate().update("INSERT INTO administrators_tab (id, administrator) VALUES (DEFAULT, ?)", administrator.getAdministrator());
	}
}
