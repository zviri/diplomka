package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dao.IFileTypeDAO;
import isir.shared.dataaccess.dto.FileType;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * The FileTypeDAOImpl PostgreSQL implementation.
 */
public class FileTypeDAOImpl extends JdbcDaoSupport implements IFileTypeDAO {

    private final FileTypeRowMapper fileTypeRowMapper;

    /**
     * Instantiates a new File type DAO.
     */
    public FileTypeDAOImpl() {
        fileTypeRowMapper = new FileTypeRowMapper();
    }

    @Override
    public FileType getFileType(String description) {
        if (description == null) {
            return null;
        }
        List<FileType> fileTypes = this.getJdbcTemplate().query("SELECT * FROM file_type_tab WHERE description = ?", new Object[]{description}, fileTypeRowMapper);
        return fileTypes.size() > 0 ? fileTypes.get(0) : null;
    }

    @Override
    public void insert(FileType fileType) {
        this.getJdbcTemplate().update("INSERT INTO file_type_tab(id, description) VALUES(DEFAULT, ?)", fileType.getDescription());
    }

    @Override
    public void delete(Long id) {
        if (id == null) {
            return;
        }
        this.getJdbcTemplate().update("DELETE FROM file_type_tab WHERE id=?", id);
    }

    @Override
    public void update(FileType fileType) {
        getJdbcTemplate().update("UPDATE file_type_tab SET description=? WHERE id=?", fileType.getDescription(), fileType.getId());
    }

    @Override
    public FileType getFileType(Long fileTypeId) {
        List<FileType> fileTypes = getJdbcTemplate().query("SELECT * FROM file_type_tab WHERE id=?", new Object[]{fileTypeId}, fileTypeRowMapper);
        return fileTypes.size() > 0 ? fileTypes.get(0) : null;
    }

    private static class FileTypeRowMapper implements RowMapper<FileType> {
        @Override
        public FileType mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new FileType().setId(rs.getLong("id"))
                    .setDescription(rs.getString("description"));
        }
    }
}
