package isir.shared.dataaccess.dto;


import isir.shared.scraping.InsolvencyDetailPage.DocumentSection;
import org.apache.commons.lang3.StringUtils;

import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;

/**
 * The Attached file DTO.
 */
public class AttachedFile {

	private Long id;
	private URL url = null;
	private String insolvencyId = null;
	private Long fileTypeId = null;
	private Timestamp publishDate = null;
	private String creditor = null;
	private DocumentSection documentSection;

    /**
     * Instantiates a new Attached file.
     */
    public AttachedFile() {
		//
	}

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
		return id;
	}

    /**
     * Gets url.
     *
     * @return the url
     */
    public URL getUrl() {
		return url;
	}

    /**
     * Sets url.
     *
     * @param url the url
     * @return the url
     */
    public AttachedFile setUrl(URL url) {
		this.url = url;
		return this;
	}

    /**
     * Gets insolvency id.
     *
     * @return the insolvency id
     */
    public String getInsolvencyId() {
		return insolvencyId;
	}

    /**
     * Sets insolvency id.
     *
     * @param insolvencyId the insolvency id
     * @return the insolvency id
     */
    public AttachedFile setInsolvencyId(String insolvencyId) {
		this.insolvencyId = insolvencyId;
		return this;
	}

    /**
     * Gets file type id.
     *
     * @return the file type id
     */
    public Long getFileTypeId() {
		return fileTypeId;
	}

    /**
     * Gets document section.
     *
     * @return the document section
     */
    public DocumentSection getDocumentSection() {
		return documentSection;
	}

    /**
     * Sets file type id.
     *
     * @param fileTypeId the file type id
     * @return the file type id
     */
    public AttachedFile setFileTypeId(Long fileTypeId) {
		this.fileTypeId = fileTypeId;
		return this;
	}

    /**
     * Gets publish date.
     *
     * @return the publish date
     */
    public Timestamp getPublishDate() {
		return publishDate;
	}

    /**
     * Sets id.
     *
     * @param id the id
     * @return the id
     */
    public AttachedFile setId(Long id) {
		this.id = id;
		return this;
	}

    /**
     * Sets publish date.
     *
     * @param publishDate the publish date
     * @return the publish date
     */
    public AttachedFile setPublishDate(Timestamp publishDate) {
		this.publishDate = publishDate;
		return this;
	}

    /**
     * Gets creditor.
     *
     * @return the creditor
     */
    public String getCreditor() {
		return creditor;
	}

    /**
     * Sets creditor.
     *
     * @param creditor the creditor
     * @return the creditor
     */
    public AttachedFile setCreditor(String creditor) {
		if (creditor != null && creditor.length() < 2) {
			creditor = null;
		}
		this.creditor = StringUtils.normalizeSpace(creditor);
		return this;
	}

    /**
     * Sets document section.
     *
     * @param documentSection the document section
     * @return the document section
     */
    public AttachedFile setDocumentSection(DocumentSection documentSection) {
		this.documentSection = documentSection;
		return this;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AttachedFile that = (AttachedFile) o;

        if (creditor != null ? !creditor.equals(that.creditor) : that.creditor != null) return false;
        if (documentSection != that.documentSection) return false;
        if (fileTypeId != null ? !fileTypeId.equals(that.fileTypeId) : that.fileTypeId != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (insolvencyId != null ? !insolvencyId.equals(that.insolvencyId) : that.insolvencyId != null) return false;
        if (publishDate != null ? !publishDate.equals(that.publishDate) : that.publishDate != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (insolvencyId != null ? insolvencyId.hashCode() : 0);
        result = 31 * result + (fileTypeId != null ? fileTypeId.hashCode() : 0);
        result = 31 * result + (publishDate != null ? publishDate.hashCode() : 0);
        result = 31 * result + (creditor != null ? creditor.hashCode() : 0);
        result = 31 * result + (documentSection != null ? documentSection.hashCode() : 0);
        return result;
    }

    /**
     * Gets null object.
     *
     * @return the null object
     */
    public static AttachedFile getNullObject() {
		return new AttachedFile()
				.setCreditor("")
				.setFileTypeId(null)
				.setInsolvencyId("")
				.setPublishDate(new Timestamp(new Date().getTime()));
	}
}
