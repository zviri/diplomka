package isir.shared.dataaccess.dao;

import isir.shared.dataaccess.dto.Insolvency;

/**
 * DAO interface for working with insolvencies.
 */
public interface IInsolvencyDAO {

    /**
     * Inserts new insolvency.
     *
     * @param insolvency the insolvency
     */
    public void insert(Insolvency insolvency);

    /**
     * Gets insolvency.
     *
     * @param insolvencyId the insolvency id
     * @return the insolvency
     */
    public Insolvency getInsolvency(String insolvencyId);

    /**
     * Update existing insolvency.
     *
     * @param insolvency the insolvency
     */
    public void update(Insolvency insolvency);


    /**
     * Cleans insolvency state duplicates.
     */
    public void cleanInsolvencyStates();
}
