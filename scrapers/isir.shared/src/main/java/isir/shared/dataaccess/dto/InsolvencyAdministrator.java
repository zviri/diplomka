package isir.shared.dataaccess.dto;

/**
 * The Insolvency administrator DTO.
 */
public class InsolvencyAdministrator {

	private String insolvencyId;
	private Long administratorId;
	private Long administratorTypeId;

    /**
     * Gets insolvency id.
     *
     * @return the insolvency id
     */
    public String getInsolvencyId() {
		return insolvencyId;
	}

    /**
     * Gets administrator id.
     *
     * @return the administrator id
     */
    public Long getAdministratorId() {
		return administratorId;
	}

    /**
     * Gets administrator type.
     *
     * @return the administrator type
     */
    public Long getAdministratorType() {
		return administratorTypeId;
	}

    /**
     * Sets insolvency id.
     *
     * @param insolvencyId the insolvency id
     * @return the insolvency id
     */
    public InsolvencyAdministrator setInsolvencyId(String insolvencyId) {
		this.insolvencyId = insolvencyId;
		return this;
	}

    /**
     * Sets administrator id.
     *
     * @param administratorId the administrator id
     * @return the administrator id
     */
    public InsolvencyAdministrator setAdministratorId(Long administratorId) {
		this.administratorId = administratorId;
		return this;
	}

    /**
     * Sets administrator type.
     *
     * @param administratorTypeId the administrator type id
     * @return the administrator type
     */
    public InsolvencyAdministrator setAdministratorType(Long administratorTypeId) {
		this.administratorTypeId = administratorTypeId;
		return this;
	}
}
