package isir.shared.dataaccess.dao.impl;

import com.thoughtworks.xstream.XStream;
import isir.shared.dataaccess.dao.IWsEventsDAO;
import isir.shared.dataaccess.dto.WsEvent;
import isir.shared.isirpub001.IsirPub001Stub.IsirPub001Data;
import isir.shared.utils.TimeUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * The WsEventsDAOImpl PostgreSQL implementation.
 */
public class WsEventsDAOImpl extends JdbcDaoSupport implements IWsEventsDAO {

    private XStream xStream = new XStream();

    @Override
    public void insert(WsEvent wsEvent) {
        this.getJdbcTemplate().update("INSERT INTO ws_events_tab(id, event_timestamp, insolvency_id, event) VALUES (?,?,?,?)",
                wsEvent.getId(),
                wsEvent.getTimestamp(),
                wsEvent.getInsolvencyId(),
                xStream.toXML(wsEvent.getEvent()));
    }

    @Override
    public void update(WsEvent wsEvent) {
        this.getJdbcTemplate().update("UPDATE ws_events_tab SET event = ?, insolvency_id = ?, event_timestamp =? WHERE id=?",
                xStream.toXML(wsEvent.getEvent()),
                wsEvent.getInsolvencyId(),
                wsEvent.getTimestamp(),
                wsEvent.getId());
    }

    @Override
    public WsEvent getEvent(Long eventId) {
        if (eventId == null) {
            return null;
        }
        List<WsEvent> events = this.getJdbcTemplate().query("SELECT * FROM ws_events_tab WHERE id = ?", new Object[]{eventId}, new WsEventRowMapper());
        return events.size() == 1 ? events.get(0) : null;
    }

    @Override
    public List<WsEvent> getWsEventsFromId(Long eventId) {
        if (eventId == null) {
            return null;
        }
        return this.getJdbcTemplate().query("SELECT * FROM ws_events_tab WHERE id >= ?", new Object[]{eventId}, new WsEventRowMapper());
    }

    @Override
    public List<WsEvent> getEvents(Calendar startDate, Calendar endDate) {
        if (startDate == null || endDate == null) {
            return new ArrayList<>();
        }
        Date startSqlDate = TimeUtils.calendarToSqlDate(startDate);
        Date endSqlDate = TimeUtils.calendarToSqlDate(endDate);
        return getJdbcTemplate().query("SELECT * FROM ws_events_tab WHERE event_timestamp::DATE BETWEEN ? AND ?", new Object[]{startSqlDate, endSqlDate}, new WsEventRowMapper());
    }

    @Override
    public List<Long> getEventIds(Calendar startDate, Calendar endDate) {
        if (startDate == null || endDate == null) {
            return new ArrayList<>();
        }
        Date startSqlDate = TimeUtils.calendarToSqlDate(startDate);
        Date endSqlDate = TimeUtils.calendarToSqlDate(endDate);
        return getJdbcTemplate().queryForList("SELECT id FROM ws_events_tab WHERE event_timestamp::DATE BETWEEN ? AND ?", Long.class, startSqlDate, endSqlDate);
    }

    @Override
    public long getMaxId() {
        return this.getJdbcTemplate().queryForLong("SELECT max(id) FROM ws_events_tab");
    }

    private class WsEventRowMapper implements RowMapper<WsEvent> {
        @Override
        public WsEvent mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new WsEvent().setId(rs.getLong("id"))
                    .setTimestamp(rs.getTimestamp("event_timestamp"))
                    .setInsolvencyId(rs.getString("insolvency_id"))
                    .setEvent((IsirPub001Data) xStream.fromXML(rs.getString("event")));
        }
    }
}
