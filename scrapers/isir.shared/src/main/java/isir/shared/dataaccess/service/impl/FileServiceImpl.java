package isir.shared.dataaccess.service.impl;

import isir.shared.dataaccess.dao.IFileDAO;
import isir.shared.dataaccess.dao.IFileTypeDAO;
import isir.shared.dataaccess.dto.AttachedFile;
import isir.shared.dataaccess.dto.FileType;
import isir.shared.dataaccess.service.FilesService;

import java.net.URL;
import java.util.List;

/**
 * The File service impl.
 */
public class FileServiceImpl implements FilesService {

    private IFileDAO fileDAO;
    private IFileTypeDAO fileTypeDAO;

    /**
     * Instantiates a new File service impl.
     *
     * @param fileDAO the file dAO
     * @param fileTypeDAO the file type dAO
     */
    public FileServiceImpl(IFileDAO fileDAO, IFileTypeDAO fileTypeDAO) {
        this.fileDAO = fileDAO;
        this.fileTypeDAO = fileTypeDAO;
    }

    @Override
    public void save(AttachedFile file) {
        if (file == null) {
            return;
        }
        if (file.getFileTypeId() == null && file.getInsolvencyId() == null && file.getPublishDate() == null) {
            throw new IllegalStateException("Cannot save file, file is not uniquely identified!");
        }
        if (file.getId() != null) {
            fileDAO.update(file);
            return;
        }
        AttachedFile existingFile = fileDAO.getFile(file.getFileTypeId(), file.getInsolvencyId(), file.getUrl());
        if (existingFile != null) {
            file.setId(existingFile.getId());
            if (!file.equals(existingFile)) {
                fileDAO.update(file);
            }
            return;
        }
        fileDAO.insert(file);
        existingFile = fileDAO.getFile(file.getFileTypeId(), file.getInsolvencyId(), file.getUrl());
        file.setId(existingFile.getId());


    }

    @Override
    public void save(FileType fileType) {
        if (fileType == null) {
            return;
        }
        if (fileType.getDescription() == null) {
            throw new IllegalStateException("Cannot save fileType without Description!");
        }
        if (fileType.getId() != null) {
            fileTypeDAO.update(fileType);
            return;
        }
        FileType existingFileType = fileTypeDAO.getFileType(fileType.getDescription());
        if (existingFileType != null) {
            fileType.setId(existingFileType.getId());
            if (!fileType.equals(existingFileType)) {
                fileTypeDAO.update(fileType);
            }
            return;
        }
        fileTypeDAO.insert(fileType);
        FileType fileTypeFromDb = fileTypeDAO.getFileType(fileType.getDescription());
        fileType.setId(fileTypeFromDb.getId());
    }

    @Override
    public List<AttachedFile> getFilesByInsolvencyId(String insolvencyId) {
        return fileDAO.getFilesByInsolvencyId(insolvencyId);
    }

    @Override
    public AttachedFile getFile(Long fileId) {
        return fileDAO.getFile(fileId);
    }

    @Override
    public void save(AttachedFile file, FileType fileType) {
        save(fileType);
        file.setFileTypeId(fileType.getId());
        save(file);
    }

    @Override
    public FileType getFileType(Long fileTypeId) {
        return fileTypeDAO.getFileType(fileTypeId);
    }

    @Override
    public AttachedFile getFile(Long documentType, String insolvencyId, URL url) {
        return fileDAO.getFile(documentType, insolvencyId, url);
    }

    @Override
    public FileType getFileType(String description) {
        return fileTypeDAO.getFileType(description);
    }
}
