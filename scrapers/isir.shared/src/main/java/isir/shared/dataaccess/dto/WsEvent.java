package isir.shared.dataaccess.dto;

import isir.shared.isirpub001.IsirPub001Stub.IsirPub001Data;

import java.sql.Timestamp;

/**
 * The Ws event DTO.
 */
public class WsEvent {

	private Long id;
	private String insolvencyId;
	private Timestamp timestamp;
	private IsirPub001Data event;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
		return id;
	}

    /**
     * Gets insolvency id.
     *
     * @return the insolvency id
     */
    public String getInsolvencyId() {
		return insolvencyId;
	}

    /**
     * Gets timestamp.
     *
     * @return the timestamp
     */
    public Timestamp getTimestamp() {
		return timestamp;
	}

    /**
     * Gets event.
     *
     * @return the event
     */
    public IsirPub001Data getEvent() {
		return event;
	}

    /**
     * Sets id.
     *
     * @param id the id
     * @return the id
     */
    public WsEvent setId(Long id) {
		this.id = id;
		return this;
	}

    /**
     * Sets insolvency id.
     *
     * @param insolvencyId the insolvency id
     * @return the insolvency id
     */
    public WsEvent setInsolvencyId(String insolvencyId) {
		this.insolvencyId = insolvencyId;
		return this;
	}

    /**
     * Sets timestamp.
     *
     * @param timestamp the timestamp
     * @return the timestamp
     */
    public WsEvent setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
		return this;
	}

    /**
     * Sets event.
     *
     * @param event the event
     * @return the event
     */
    public WsEvent setEvent(IsirPub001Data event) {
		this.event = event;
		return this;
	}
}
