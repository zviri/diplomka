package isir.shared.dataaccess.dao;

import isir.shared.dataaccess.dto.InsolvencyStateType;

import java.util.List;

/**
 * DAO interface for working with insolvency state types.
 */
public interface IInsolvencyStatesTypeDAO {

    /**
     * Gets all insolvency states types.
     *
     * @return the all insolvency states types
     */
    public abstract List<InsolvencyStateType> getAllInsolvencyStatesTypes();

    /**
     * Updates existing insolvency state type.
     *
     * @param insolvencyStateType the insolvency state type
     */
    void update(InsolvencyStateType insolvencyStateType);

    /**
     * Gets insolvency state type.
     *
     * @param textIdentifier the text identifier
     * @return the insolvency state type
     */
    InsolvencyStateType getInsolvencyStateType(String textIdentifier);

    /**
     * Inserts new insolvency state type.
     *
     * @param insolvencyStateType the insolvency state type
     */
    void insert(InsolvencyStateType insolvencyStateType);
}
