package isir.shared.dataaccess.dao;

import isir.shared.dataaccess.dto.HttpWebsite;

import java.net.URL;


/**
 * DAO interface for working with cached web sites.
 */
public interface IHttpCacheDAO {

    /**
     * Inserts new website.
     *
     * @param httpWebsite the http website
     */
    public void insertWebsite(HttpWebsite httpWebsite);

    /**
     * Gets website.
     *
     * @param url the url
     * @return the website
     */
    public HttpWebsite getWebsite(URL url);

    /**
     * Updates existing website.
     *
     * @param httpWebsite the http website
     */
    public void updateWebsite(HttpWebsite httpWebsite);
}
