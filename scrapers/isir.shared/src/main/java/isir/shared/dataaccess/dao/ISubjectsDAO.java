package isir.shared.dataaccess.dao;

import isir.shared.dataaccess.dto.Subject;

/**
 * DAO interface for working with subjects.
 */
public interface ISubjectsDAO {
    /**
     * Inserts new subject.
     *
     * @param subject the subject
     */
    void insert(Subject subject);

    /**
     * Updates existing subject.
     *
     * @param subject the subject
     */
    void update(Subject subject);

    /**
     * Gets subject.
     *
     * @param name the name
     * @return the subject
     */
    Subject getSubject(String name);
}
