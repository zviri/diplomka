package isir.shared.dataaccess.dao;

import isir.shared.dataaccess.dto.FileType;


/**
 * DAO interface for working with file types.
 */
public interface IFileTypeDAO {

    /**
     * Gets file type.
     *
     * @param description the description
     * @return the file type
     */
    FileType getFileType(String description);

    /**
     * Inserts new file.
     *
     * @param fileType the file type
     */
    void insert(FileType fileType);

    /**
     * Deletes file.
     *
     * @param id the file id
     */
    void delete(Long id);

    /**
     * Updates existing file.
     *
     * @param fileType the file type
     */
    void update(FileType fileType);

    /**
     * Gets file type.
     *
     * @param fileTypeId the file type id
     * @return the file type
     */
    FileType getFileType(Long fileTypeId);
}
