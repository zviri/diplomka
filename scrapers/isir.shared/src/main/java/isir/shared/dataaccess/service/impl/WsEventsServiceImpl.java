package isir.shared.dataaccess.service.impl;

import isir.shared.dataaccess.dao.IWsEventsDAO;
import isir.shared.dataaccess.dto.WsEvent;
import isir.shared.dataaccess.service.WsEventsService;

import java.util.Calendar;
import java.util.List;

public class WsEventsServiceImpl implements WsEventsService {

    private IWsEventsDAO wsEventsDAO;

    public WsEventsServiceImpl(IWsEventsDAO wsEventsDAO) {
        this.wsEventsDAO = wsEventsDAO;
    }

    @Override
    public void save(WsEvent wsEvent) {
        if (wsEvent == null) {
            return;
        }
        Long wsEventId = wsEvent.getId();
        if (wsEventId == null || wsEventsDAO.getEvent(wsEventId) == null) {
            wsEventsDAO.insert(wsEvent);
        } else {
            wsEventsDAO.update(wsEvent);
        }
    }

    @Override
    public void save(List<WsEvent> wsEvents) {
        if (wsEvents == null) {
            return;
        }
        for (WsEvent wsEvent : wsEvents) {
            save(wsEvent);
        }
    }

    @Override
    public long getMaxId() {
        return wsEventsDAO.getMaxId();
    }

    @Override
    public WsEvent getEvent(long eventId) {
        return wsEventsDAO.getEvent(eventId);
    }

    @Override
    public List<WsEvent> getWsEventsFromId(long eventId) {
        return wsEventsDAO.getWsEventsFromId(eventId);
    }

    @Override
    public List<WsEvent> getEvents(Calendar startDate, Calendar endDate) {
        return wsEventsDAO.getEvents(startDate, endDate);
    }

    @Override
    public List<Long> getEventIds(Calendar startDate, Calendar endDate) {
        return wsEventsDAO.getEventIds(startDate, endDate);
    }
}
