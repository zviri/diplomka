package isir.shared.dataaccess.dto;

import org.apache.commons.lang3.StringUtils;

/**
 * The File type DTO.
 */
public class FileType {

    private Long id;
    private String description;

    /**
     * Sets id.
     *
     * @param id the id
     * @return the id
     */
    public FileType setId(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets description.
     *
     * @param description the description
     * @return the description
     */
    public FileType setDescription(String description) {
        this.description = StringUtils.normalizeSpace(description);
        return this;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileType fileType = (FileType) o;

        if (description != null ? !description.equals(fileType.description) : fileType.description != null)
            return false;
        if (id != null ? !id.equals(fileType.id) : fileType.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
