package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dao.ISubjectsDAO;
import isir.shared.dataaccess.dto.Subject;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * The SubjectsDAOImpl PostgreSQL implementation.
 */
public class SubjectsDAOImpl extends JdbcDaoSupport implements ISubjectsDAO {

    @Override
    public void insert(Subject subject) {
        getJdbcTemplate().update("INSERT INTO subjects_tab(name, ico, form, legal_form, address_form, address_city, addres_street, address_description_number, address_country, address_zip_code, degree_before, degree_after) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?)",
                subject.getName(),
                subject.getIco(),
                subject.getForm(),
                subject.getLegalForm(),
                subject.getAddressForm(),
                subject.getAddressCity(),
                subject.getAddresStreet(),
                subject.getAddressDescriptionNumber(),
                subject.getAddressCountry(),
                subject.getAddressZipCode(),
                subject.getDegreeBefore(),
                subject.getDegreeAfter());
    }

    @Override
    public void update(Subject subject) {
        getJdbcTemplate().update("UPDATE subjects_tab SET name=?, ico=?, form=?, legal_form=?, address_form=?, address_city=?, addres_street=?, address_description_number=?, address_country=?, address_zip_code=?, degree_before=?, degree_after=? WHERE name=?",
                subject.getName(),
                subject.getIco(),
                subject.getForm(),
                subject.getLegalForm(),
                subject.getAddressForm(),
                subject.getAddressCity(),
                subject.getAddresStreet(),
                subject.getAddressDescriptionNumber(),
                subject.getAddressCountry(),
                subject.getAddressZipCode(),
                subject.getDegreeBefore(),
                subject.getDegreeAfter(),
                subject.getName());
    }

    @Override
    public Subject getSubject(String name) {
        List<Subject> subjects = getJdbcTemplate().query("SELECT * FROM subjects_tab WHERE name=?", new Object[]{name}, new RowMapper<Subject>() {
            @Override
            public Subject mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new Subject().setName(rs.getString("name"))
                        .setForm(rs.getString("form"))
                        .setIco(rs.getString("ico"))
                        .setLegalForm(rs.getString("legal_form"))
                        .setAddressForm(rs.getString("address_form"))
                        .setAddressCity(rs.getString("address_city"))
                        .setAddresStreet(rs.getString("addres_street"))
                        .setAddressDescriptionNumber(rs.getString("address_description_number"))
                        .setAddressCountry(rs.getString("address_country"))
                        .setAddressZipCode(rs.getString("address_zip_code"))
                        .setDegreeBefore(rs.getString("degree_before"))
                        .setDegreeAfter(rs.getString("degree_after"));
            }
        });
        return subjects.size() > 0 ? subjects.get(0) : null;
    }
}
