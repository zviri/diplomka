package isir.shared.dataaccess.dto;

import isir.shared.exceptions.InvalidBirdNumberException;

/**
 * The Birth number class for validating and working with birth numbers.
 */
public class BirthNumber {

    /**
     * The enum Gender.
     */
    public static enum Gender {
        /**
         * The Male.
         */
        M, /**
         * The Female.
         */
        F
	}

	private String birdNumber;

    /**
     * Instantiates a new Birth number.
     *
     * @param birdNumber the bird number
     * @throws InvalidBirdNumberException the invalid bird number exception
     */
    public BirthNumber(String birdNumber) throws InvalidBirdNumberException {
		this.birdNumber = birdNumber;
		String birdNumberWithoutSeparator = getBirthNumberWithoutSeparator();
		if (birdNumberWithoutSeparator.length() != 9 && birdNumberWithoutSeparator.length() != 10) {
			throw new InvalidBirdNumberException();
		}
	}

	private String getBirthNumberWithoutSeparator() {
		return birdNumber.replace("/", "");
	}

    /**
     * Gets gender.
     *
     * @return the gender
     */
    public Gender getGender() {
		int birdMonth = Integer.parseInt(birdNumber.substring(2,4));
		return ((birdMonth >= 51 && birdMonth <= 62) || (birdMonth >= 71 && birdMonth <= 82)) ? Gender.F : Gender.M;
	}

    /**
     * Gets year of birth.
     *
     * @return the year of birth
     */
    public int getYearOfBirth() {
		int yearOfBirth;
		int birthNumberYear = Integer.parseInt(birdNumber.substring(0, 2));
		if (getBirthNumberWithoutSeparator().length() == 9) {
			yearOfBirth = 1900 + birthNumberYear;
		} else {
			yearOfBirth = ((birthNumberYear < 54) ? 2000 : 1900) + birthNumberYear;
		}
		return yearOfBirth;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((birdNumber == null) ? 0 : birdNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BirthNumber other = (BirthNumber) obj;
		if (birdNumber == null) {
			if (other.birdNumber != null)
				return false;
		} else if (!birdNumber.equals(other.birdNumber))
			return false;
		return true;
	}
}
