package isir.shared.dataaccess.dao;

import isir.shared.dataaccess.dto.InsolvencyAdministrator;


/**
 * DAO interface for working with insolvency-administrator objects.
 */
public interface IInsolvenciesAdministratorsDAO {

    /**
     * Deletes all insolvency-administrator objects for the insolvency.
     *
     * @param insolvecnyId the insolvecny id
     */
    public abstract void delete(String insolvecnyId);

    /**
     * Insert new insolvency-administrator.
     *
     * @param fileAdministrator the file administrator
     */
    public abstract void insert(InsolvencyAdministrator fileAdministrator);

    /**
     * Gets insolvency-administrator administrator.
     *
     * @param insolvencyId the insolvency id
     * @param administratorId the administrator id
     * @param administratorTypeId the administrator type id
     * @return the insolvency-administrator
     */
    public abstract InsolvencyAdministrator getInsolvencyAdministrator(String insolvencyId, Long administratorId, Long administratorTypeId);

}
