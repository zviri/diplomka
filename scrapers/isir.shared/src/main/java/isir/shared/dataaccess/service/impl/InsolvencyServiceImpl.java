package isir.shared.dataaccess.service.impl;

import isir.shared.dataaccess.dao.*;
import isir.shared.dataaccess.dto.*;
import isir.shared.dataaccess.service.InsolvencyService;

import java.util.List;

public class InsolvencyServiceImpl implements InsolvencyService {

    private IInsolvencyDAO insolvencyDAO;
    private IAdministratorsDAO administratorsDAO;
    private IAdministratorTypesDAO administratorTypesDAO;
    private ISubjectsDAO subjectsDAO;
    private IInsolvenciesAdministratorsDAO insolvenciesAdministratorsDAO;
    private IInsolvencyStatesDAO insolvencyStatesDAO;
    private IInsolvencyStatesTypeDAO insolvencyStatesTypeDAO;

    public InsolvencyServiceImpl(IInsolvencyDAO insolvencyDAO, IAdministratorsDAO administratorsDAO, IInsolvenciesAdministratorsDAO insolvenciesAdministratorsDAO, IInsolvencyStatesDAO insolvencyStatesDAO, IInsolvencyStatesTypeDAO insolvencyStatesTypeDAO, IAdministratorTypesDAO administratorTypesDAO, ISubjectsDAO subjectsDAO) {
        this.insolvencyDAO = insolvencyDAO;
        this.administratorsDAO = administratorsDAO;
        this.insolvenciesAdministratorsDAO = insolvenciesAdministratorsDAO;
        this.insolvencyStatesDAO = insolvencyStatesDAO;
        this.insolvencyStatesTypeDAO = insolvencyStatesTypeDAO;
        this.administratorTypesDAO = administratorTypesDAO;
        this.subjectsDAO = subjectsDAO;
    }

    public void save(Insolvency insolvency) {
        if (insolvency == null) {
            return;
        }
        if (insolvency.getId() == null) {
            throw new IllegalStateException("Cannot save insolvency without Id");
        }
        Insolvency existingInsolvency = insolvencyDAO.getInsolvency(insolvency.getId());
        if (existingInsolvency == null) {
            insolvencyDAO.insert(insolvency);
        } else {
            if (!existingInsolvency.equals(insolvency)) {
                insolvencyDAO.update(insolvency);
            }
        }
    }

    @Override
    public void save(Administrator administrator) {
        if (administrator == null) {
            return;
        }
        if (administrator.getAdministrator() == null) {
            throw new IllegalStateException("Cannot save Administrator!");
        }

        if (administrator.getId() != null) {
            administratorsDAO.update(administrator);
            return;
        }
        Administrator existingAdministrator = getAdministrator(administrator.getAdministrator());
        if (existingAdministrator != null) {
            administrator.setId(existingAdministrator.getId());
            if (!administrator.equals(existingAdministrator)) {
                administratorsDAO.update(administrator);
            }
            return;
        }

        administratorsDAO.insert(administrator);
        Administrator administratorFromDb = getAdministrator(administrator.getAdministrator());
        administrator.setId(administratorFromDb.getId());
    }

    @Override
    public void save(InsolvencyState insolvencyState) {
        if (insolvencyState == null) {
            return;
        }
        if (insolvencyState.getInsolvencyID() == null || insolvencyState.getState() == null || insolvencyState.getStateChangeTimestamp() == null || insolvencyState.getActionID() == null) {
            throw new IllegalStateException("Cannot save insolvencyState without actionId, insolvencyId, stateId and stateChangeTimestamp");
        }
        InsolvencyState existingInsolvencyState = insolvencyStatesDAO.getInsolvencyStateByActionId(insolvencyState.getActionID());
        if (existingInsolvencyState != null) {
            if (!existingInsolvencyState.equals(insolvencyState)) {
                insolvencyStatesDAO.update(insolvencyState);
            }
        } else {
            insolvencyStatesDAO.insert(insolvencyState);
        }
    }

    @Override
    public void save(InsolvencyStateType insolvencyStateType) {
        if (insolvencyStateType == null) {
            return;
        }
        if (insolvencyStateType.getTextIdentifier() == null) {
            throw new IllegalStateException("Cannot save InsolvencyStateType without TextIdentifier");
        }
        if (insolvencyStateType.getId() != null) {
            insolvencyStatesTypeDAO.update(insolvencyStateType);
            return;
        }
        InsolvencyStateType existingInsolvencyStateType = insolvencyStatesTypeDAO.getInsolvencyStateType(insolvencyStateType.getTextIdentifier());
        if (existingInsolvencyStateType != null) {
            insolvencyStateType.setId(existingInsolvencyStateType.getId());
            if (!existingInsolvencyStateType.equals(insolvencyStateType)) {
                insolvencyStatesTypeDAO.update(insolvencyStateType);
            }
            return;
        }
        insolvencyStatesTypeDAO.insert(insolvencyStateType);
        existingInsolvencyStateType = insolvencyStatesTypeDAO.getInsolvencyStateType(insolvencyStateType.getTextIdentifier());
        insolvencyStateType.setId(existingInsolvencyStateType.getId());
    }

    @Override
    public void save(AdministratorType administratorType) {
        if (administratorType == null) {
            return;
        }
        if (administratorType.getDescription() == null) {
            throw new IllegalStateException("Cannot save administratorType without Description");
        }
        if (administratorType.getId() != null) {
            administratorTypesDAO.update(administratorType);
            return;
        }
        AdministratorType existingAdministrator = administratorTypesDAO.getAdministratorType(administratorType.getDescription());
        if (existingAdministrator != null) {
            administratorType.setId(existingAdministrator.getId());
            if (!administratorType.equals(existingAdministrator)) {
                administratorTypesDAO.update(administratorType);
            }
            return;
        }
        administratorTypesDAO.insert(administratorType);
        existingAdministrator = administratorTypesDAO.getAdministratorType(administratorType.getDescription());
        administratorType.setId(existingAdministrator.getId());
    }

    @Override
    public void save(Insolvency insolvency, AdministratorType administratorType, Administrator administrator) {
        save(insolvency);
        save(administratorType);
        save(administrator);
        InsolvencyAdministrator insolvencyAdministrator = new InsolvencyAdministrator().setInsolvencyId(insolvency.getId())
                                                                                       .setAdministratorType(administratorType.getId())
                                                                                       .setAdministratorId(administrator.getId());
        InsolvencyAdministrator existingInsolvencyAdministrator = insolvenciesAdministratorsDAO.getInsolvencyAdministrator(insolvencyAdministrator.getInsolvencyId(), insolvencyAdministrator.getAdministratorId(), insolvencyAdministrator.getAdministratorType());
        if (existingInsolvencyAdministrator != null) {
            return;
        }
        insolvenciesAdministratorsDAO.insert(insolvencyAdministrator);
    }

    @Override
    public void save(Insolvency insolvency, InsolvencyState insolvencyState, InsolvencyStateType insolvencyStateType) {
        save(insolvency);
        save(insolvencyStateType);
        insolvencyState.setState(insolvencyStateType.getId());
        save(insolvencyState);
    }

    @Override
    public void save(Subject subject) {
        if (subject.getName() == null) {
            throw new IllegalStateException("Cannot save Subject without name");
        }
        Subject existingSubject = subjectsDAO.getSubject(subject.getName());
        if (existingSubject == null) {
            subjectsDAO.insert(subject);
            return;
        }
        if (!existingSubject.equals(subject)) {
            subjectsDAO.update(subject);
        }
    }

    @Override
    public void cleanInsolvencyStates() {
        insolvencyDAO.cleanInsolvencyStates();
    }

    @Override
    public Insolvency getInsolvency(String insolvencyId) {
        return insolvencyDAO.getInsolvency(insolvencyId);
    }

    @Override
    public Administrator getAdministrator(String administrator) {
        return administratorsDAO.getAdministrator(administrator);
    }

    @Override
    public void delete(String insolvecnyId) {
        insolvenciesAdministratorsDAO.delete(insolvecnyId);
    }

    @Override
    public InsolvencyAdministrator getFileAdministrator(String insolvencyId, Long administratorId, Long administratorTypeId) {
        return insolvenciesAdministratorsDAO.getInsolvencyAdministrator(insolvencyId, administratorId, administratorTypeId);
    }

    @Override
    public Long getLastActionID() {
        return insolvencyStatesDAO.getLastActionID();
    }

    @Override
    public InsolvencyState getStateByActionId(Long actionID) {
        return insolvencyStatesDAO.getInsolvencyStateByActionId(actionID);
    }

    @Override
    public InsolvencyStateType getInsolvencyStateType(String textIdentifier) {
        return insolvencyStatesTypeDAO.getInsolvencyStateType(textIdentifier);
    }

    @Override
    public List<InsolvencyStateType> getAllInsolvencyStatesTypes() {
        return insolvencyStatesTypeDAO.getAllInsolvencyStatesTypes();
    }

    @Override
    public Subject getSubject(String name) {
        return subjectsDAO.getSubject(name);
    }
}
