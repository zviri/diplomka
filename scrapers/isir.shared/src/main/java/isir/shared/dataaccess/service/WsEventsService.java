package isir.shared.dataaccess.service;

import isir.shared.dataaccess.dto.WsEvent;

import java.util.Calendar;
import java.util.List;

/**
 * Facade for working with ws events over the existing DAO objects.
 */
public interface WsEventsService {

    /**
     * Creates new or updates existing ws event.
     *
     * @param wsEvent the ws event
     */
    public void save(WsEvent wsEvent);

    /**
     * Creates new or updates existing ws events.
     *
     * @param wsEvents the ws events
     */
    public void save(List<WsEvent> wsEvents);

    /**
     * Gets max id.
     *
     * @return the max id
     */
    public long getMaxId();

    /**
     * Gets event.
     *
     * @param eventId the event id
     * @return the event
     */
    public WsEvent getEvent(long eventId);

    /**
     * Gets ws events from id.
     *
     * @param eventId the event id
     * @return the ws events from id
     */
    public List<WsEvent> getWsEventsFromId(long eventId);

    /**
     * Gets events.
     *
     * @param startDate the start date
     * @param endDate the end date
     * @return the events
     */
    public List<WsEvent> getEvents(Calendar startDate, Calendar endDate);

    /**
     * Gets event ids.
     * @param startDate the start date
     * @param endDate the end date
     * @return the events
     */
    public List<Long> getEventIds(Calendar startDate, Calendar endDate);
}
