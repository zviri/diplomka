package isir.shared.dataaccess.dto;

import org.apache.commons.lang3.StringUtils;

/**
 * The Subject DTO.
 */
public class Subject {
    private String name;
    private String ico;
    private String form;
    private String legalForm;
    private String addressForm;
    private String addressCity;
    private String addresStreet;
    private String addressDescriptionNumber;
    private String addressCountry;
    private String addressZipCode;
    private String degreeBefore;
    private String degreeAfter;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     * @return the name
     */
    public Subject setName(String name) {
        this.name = StringUtils.normalizeSpace(name);
        return this;
    }

    /**
     * Gets ico.
     *
     * @return the ico
     */
    public String getIco() {
        return ico;
    }

    /**
     * Sets ico.
     *
     * @param ico the ico
     * @return the ico
     */
    public Subject setIco(String ico) {
        this.ico = StringUtils.normalizeSpace(ico);
        return this;
    }

    /**
     * Gets form.
     *
     * @return the form
     */
    public String getForm() {
        return form;
    }

    /**
     * Sets form.
     *
     * @param form the form
     * @return the form
     */
    public Subject setForm(String form) {
        this.form = StringUtils.normalizeSpace(form);
        return this;
    }

    /**
     * Gets legal form.
     *
     * @return the legal form
     */
    public String getLegalForm() {
        return legalForm;
    }

    /**
     * Sets legal form.
     *
     * @param legalForm the legal form
     * @return the legal form
     */
    public Subject setLegalForm(String legalForm) {
        this.legalForm = StringUtils.normalizeSpace(legalForm);
        return this;
    }

    /**
     * Gets address form.
     *
     * @return the address form
     */
    public String getAddressForm() {
        return addressForm;
    }

    /**
     * Sets address form.
     *
     * @param addressForm the address form
     * @return the address form
     */
    public Subject setAddressForm(String addressForm) {
        this.addressForm =  StringUtils.normalizeSpace(addressForm);
        return this;
    }

    /**
     * Gets address city.
     *
     * @return the address city
     */
    public String getAddressCity() {
        return addressCity;
    }

    /**
     * Sets address city.
     *
     * @param addressCity the address city
     * @return the address city
     */
    public Subject setAddressCity(String addressCity) {
        this.addressCity =  StringUtils.normalizeSpace(addressCity);
        return this;
    }

    /**
     * Gets addres street.
     *
     * @return the addres street
     */
    public String getAddresStreet() {
        return addresStreet;
    }

    /**
     * Sets addres street.
     *
     * @param addresStreet the addres street
     * @return the addres street
     */
    public Subject setAddresStreet(String addresStreet) {
        this.addresStreet =  StringUtils.normalizeSpace(addresStreet);
        return this;
    }

    /**
     * Gets address description number.
     *
     * @return the address description number
     */
    public String getAddressDescriptionNumber() {
        return addressDescriptionNumber;
    }

    /**
     * Sets address description number.
     *
     * @param addressDescriptionNumber the address description number
     * @return the address description number
     */
    public Subject setAddressDescriptionNumber(String addressDescriptionNumber) {
        this.addressDescriptionNumber =  StringUtils.normalizeSpace(addressDescriptionNumber);
        return this;
    }

    /**
     * Gets address country.
     *
     * @return the address country
     */
    public String getAddressCountry() {
        return addressCountry;
    }

    /**
     * Sets address country.
     *
     * @param addressCountry the address country
     * @return the address country
     */
    public Subject setAddressCountry(String addressCountry) {
        this.addressCountry =  StringUtils.normalizeSpace(addressCountry);
        return this;
    }

    /**
     * Gets address zip code.
     *
     * @return the address zip code
     */
    public String getAddressZipCode() {
        return addressZipCode;
    }

    /**
     * Sets address zip code.
     *
     * @param addressZipCode the address zip code
     * @return the address zip code
     */
    public Subject setAddressZipCode(String addressZipCode) {
        this.addressZipCode = StringUtils.deleteWhitespace(addressZipCode);
        return this;
    }

    /**
     * Sets degree before.
     *
     * @param degreeBefore the degree before
     * @return the degree before
     */
    public Subject setDegreeBefore(String degreeBefore) {
        this.degreeBefore = StringUtils.normalizeSpace(degreeBefore);
        return this;
    }

    /**
     * Gets degree before.
     *
     * @return the degree before
     */
    public String getDegreeBefore() {
        return degreeBefore;
    }

    /**
     * Sets degree after.
     *
     * @param degreeAfter the degree after
     * @return the degree after
     */
    public Subject setDegreeAfter(String degreeAfter) {
        this.degreeAfter = StringUtils.normalizeSpace(degreeAfter);
        return this;
    }

    /**
     * Gets degree after.
     *
     * @return the degree after
     */
    public String getDegreeAfter() {
        return degreeAfter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subject subject = (Subject) o;

        if (addresStreet != null ? !addresStreet.equals(subject.addresStreet) : subject.addresStreet != null)
            return false;
        if (addressCity != null ? !addressCity.equals(subject.addressCity) : subject.addressCity != null) return false;
        if (addressCountry != null ? !addressCountry.equals(subject.addressCountry) : subject.addressCountry != null)
            return false;
        if (addressDescriptionNumber != null ? !addressDescriptionNumber.equals(subject.addressDescriptionNumber) : subject.addressDescriptionNumber != null)
            return false;
        if (addressForm != null ? !addressForm.equals(subject.addressForm) : subject.addressForm != null) return false;
        if (addressZipCode != null ? !addressZipCode.equals(subject.addressZipCode) : subject.addressZipCode != null)
            return false;
        if (degreeAfter != null ? !degreeAfter.equals(subject.degreeAfter) : subject.degreeAfter != null) return false;
        if (degreeBefore != null ? !degreeBefore.equals(subject.degreeBefore) : subject.degreeBefore != null)
            return false;
        if (form != null ? !form.equals(subject.form) : subject.form != null) return false;
        if (ico != null ? !ico.equals(subject.ico) : subject.ico != null) return false;
        if (legalForm != null ? !legalForm.equals(subject.legalForm) : subject.legalForm != null) return false;
        if (name != null ? !name.equals(subject.name) : subject.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (ico != null ? ico.hashCode() : 0);
        result = 31 * result + (form != null ? form.hashCode() : 0);
        result = 31 * result + (legalForm != null ? legalForm.hashCode() : 0);
        result = 31 * result + (addressForm != null ? addressForm.hashCode() : 0);
        result = 31 * result + (addressCity != null ? addressCity.hashCode() : 0);
        result = 31 * result + (addresStreet != null ? addresStreet.hashCode() : 0);
        result = 31 * result + (addressDescriptionNumber != null ? addressDescriptionNumber.hashCode() : 0);
        result = 31 * result + (addressCountry != null ? addressCountry.hashCode() : 0);
        result = 31 * result + (addressZipCode != null ? addressZipCode.hashCode() : 0);
        result = 31 * result + (degreeBefore != null ? degreeBefore.hashCode() : 0);
        result = 31 * result + (degreeAfter != null ? degreeAfter.hashCode() : 0);
        return result;
    }
}
