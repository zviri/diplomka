package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dao.IHttpCacheDAO;
import isir.shared.dataaccess.dto.HttpWebsite;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static isir.shared.utils.IsirStringUtils.stringToUrl;
import static isir.shared.utils.IsirStringUtils.urlToString;

/**
 * The AdministratorTypesDAOImpl PostgreSQL implementation.
 */
public class HttpCacheDAOImpl extends JdbcDaoSupport implements IHttpCacheDAO {

	@Override
	public void insertWebsite(HttpWebsite httpWebsite) {
		getJdbcTemplate().update("INSERT INTO http_cache_tab (url, content, download_date) VALUES (?,?,?)",
								 httpWebsite.getUrl().toString(),
								 httpWebsite.getContent(),
								 httpWebsite.getDownloadDate());
	}

	@Override
	public HttpWebsite getWebsite(URL url) {
		List<HttpWebsite> websites = getJdbcTemplate().query("SELECT * FROM http_cache_tab WHERE url = ?", new Object[] { urlToString(url) }, new RowMapper<HttpWebsite>() {
			@Override
			public HttpWebsite mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new HttpWebsite().setUrl(stringToUrl(rs.getString("url")))
                                        .setContent(rs.getString("content"))
                                        .setDownloadDate(rs.getTimestamp("download_date"));
			}});
		return websites.size() == 1 ? websites.get(0) : null;

	}

	@Override
	public void updateWebsite(HttpWebsite httpWebsite) {
		getJdbcTemplate().update("UPDATE http_cache_tab SET content = ?, download_date = ? WHERE url = ?",
								 httpWebsite.getContent(),
								 httpWebsite.getDownloadDate(),
								 httpWebsite.getUrl().toString());
	}
}
