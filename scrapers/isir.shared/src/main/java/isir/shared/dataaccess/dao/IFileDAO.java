package isir.shared.dataaccess.dao;

import isir.shared.dataaccess.dto.AttachedFile;

import java.net.URL;
import java.util.List;


/**
 * DAO interface for working with files.
 */
public interface IFileDAO {

    /**
     * Inserts new file.
     *
     * @param file the file
     */
    public void insert(AttachedFile file);

    /**
     * Gets file.
     *
     * @param url the url
     * @return the file
     */
    public List<AttachedFile> getFile(URL url);

    /**
     * Updates existing file.
     *
     * @param file the file
     */
    public void update(AttachedFile file);

    /**
     * Gets file.
     *
     * @param documentType the document type
     * @param insolvencyId the insolvency id
     * @param url the url
     * @return the file
     */
    AttachedFile getFile(Long documentType, String insolvencyId, URL url);

    /**
     * Gets files by insolvency id.
     *
     * @param insolvencyId the insolvency id
     * @return the files by insolvency id
     */
    List<AttachedFile> getFilesByInsolvencyId(String insolvencyId);

    /**
     * Gets file.
     *
     * @param fileId the file id
     * @return the file
     */
    AttachedFile getFile(Long fileId);
}
