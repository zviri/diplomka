package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dao.IFileDAO;
import isir.shared.dataaccess.dto.AttachedFile;
import isir.shared.scraping.InsolvencyDetailPage.DocumentSection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static isir.shared.utils.IsirStringUtils.stringToUrl;
import static isir.shared.utils.IsirStringUtils.urlToString;

/**
 * The FileDAOImpl PostgreSQL implementation.
 */
public class FileDAOImpl extends JdbcDaoSupport implements IFileDAO {

    private static Logger log = LoggerFactory.getLogger(FileDAOImpl.class);

    private final AttachedFileRowMapper fileRowMapper;

    /**
     * Instantiates a new File DAO.
     */
    public FileDAOImpl() {
        fileRowMapper = new AttachedFileRowMapper();
    }

    /**
     * The Attached file row mapper.
     */
    public static class AttachedFileRowMapper implements RowMapper<AttachedFile> {
        @Override
        public AttachedFile mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new AttachedFile().setId(rs.getLong("id"))
                    .setCreditor(rs.getString("creditor"))
                    .setFileTypeId(rs.getLong("file_type_id"))
                    .setInsolvencyId(rs.getString("insolvency_id"))
                    .setPublishDate(rs.getTimestamp("publish_date"))
                    .setUrl(stringToUrl(rs.getString("url")))
                    .setDocumentSection(DocumentSection.valueOf(rs.getString("document_section")));
        }
    }

    @Override
    public void insert(AttachedFile file) {
        getJdbcTemplate().update("INSERT INTO file_tab (id, url, insolvency_id, publish_date, creditor, document_section, file_type_id) VALUES (DEFAULT, ?,?,?,?,?,?)",
                urlToString(file.getUrl()),
                file.getInsolvencyId(),
                file.getPublishDate(),
                file.getCreditor(),
                file.getDocumentSection().name(),
                file.getFileTypeId());
    }

    @Override
    public List<AttachedFile> getFile(URL url) {
        return getJdbcTemplate().query("SELECT * FROM file_tab WHERE url = ?", new Object[]{urlToString(url)}, fileRowMapper);
    }

    @Override
    public void update(AttachedFile attachedFile) {
        getJdbcTemplate().update("UPDATE file_tab SET url=?, insolvency_id=?, publish_date=?, creditor=?, document_section=?, file_type_id=? WHERE id=?",
                urlToString(attachedFile.getUrl()),
                attachedFile.getInsolvencyId(),
                attachedFile.getPublishDate(),
                attachedFile.getCreditor(),
                attachedFile.getDocumentSection().name(),
                attachedFile.getFileTypeId(),
                attachedFile.getId());
    }

    @Override
    public AttachedFile getFile(Long fileTypeId, String insolvencyId, URL url) {
        if (fileTypeId == null || insolvencyId == null || url == null) {
            return null;
        }
        List<AttachedFile> attachedFiles = getJdbcTemplate().query("SELECT * FROM file_tab WHERE file_type_id = ? AND insolvency_id =? AND url=?",
                new Object[]{fileTypeId, insolvencyId, urlToString(url)},
                fileRowMapper);
        return attachedFiles.size() > 0 ? attachedFiles.get(0) : null;
    }

    @Override
    public List<AttachedFile> getFilesByInsolvencyId(String insolvencyId) {
        return getJdbcTemplate().query("SELECT * FROM file_tab WHERE insolvency_id=?", new Object[]{insolvencyId}, fileRowMapper);
    }

    @Override
    public AttachedFile getFile(Long fileId) {
        if (fileId == null) {
            return null;
        }
        List<AttachedFile> attachedFiles = getJdbcTemplate().query("SELECT * FROM file_tab WHERE id = ?",
                new Object[]{fileId},
                fileRowMapper);
        return attachedFiles.size() > 0 ? attachedFiles.get(0) : null;
    }
}
