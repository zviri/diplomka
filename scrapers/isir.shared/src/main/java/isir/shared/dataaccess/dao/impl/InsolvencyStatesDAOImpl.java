package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dao.IInsolvencyStatesDAO;
import isir.shared.dataaccess.dto.InsolvencyState;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

/**
 * The InsolvencyStatesDAOImpl PostgreSQL implementation.
 */
public class InsolvencyStatesDAOImpl extends JdbcDaoSupport implements IInsolvencyStatesDAO {

    @Override
    public void insert(InsolvencyState state) {
        getJdbcTemplate().update("INSERT INTO insolvency_state_tab(insolvency_id, state, state_change_timestamp, action_id) VALUES (?, ?, ?, ?)",
                state.getInsolvencyID(),
                state.getState(),
                state.getStateChangeTimestamp(),
                state.getActionID());
    }

    @Override
    public void update(InsolvencyState state) {
        getJdbcTemplate().update("UPDATE insolvency_state_tab SET insolvency_id=?, state=?, state_change_timestamp=?, action_id=? WHERE action_id = ?",
                state.getInsolvencyID(),
                state.getState(),
                state.getStateChangeTimestamp(),
                state.getActionID(),
                state.getActionID());
    }

    @Override
    public Long getLastActionID() {
        return getJdbcTemplate().queryForLong("SELECT max(action_id) FROM insolvency_state_tab");
    }

    @Override
    public InsolvencyState getInsolvencyStateByActionId(Long actionID) {
        List<InsolvencyState> insolvencyStates = getJdbcTemplate().query("SELECT * FROM insolvency_state_tab WHERE action_id = ?", new Object[]{actionID}, new InsolvencyStateRowMapper());
        return insolvencyStates.size() > 0 ? insolvencyStates.get(0) : null;
    }

    private static class InsolvencyStateRowMapper implements RowMapper<InsolvencyState> {
        @Override
        public InsolvencyState mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new InsolvencyState().setActionID(rs.getLong("action_id"))
                    .setInsolvencyID(rs.getString("insolvency_id"))
                    .setState(rs.getLong("state"))
                    .setStateChangeTimestamp(rs.getTimestamp("state_change_timestamp"));
        }
    }
}
