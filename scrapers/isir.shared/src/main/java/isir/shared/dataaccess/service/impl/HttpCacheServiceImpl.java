package isir.shared.dataaccess.service.impl;

import isir.shared.dataaccess.dao.IHttpCacheDAO;
import isir.shared.dataaccess.dto.HttpWebsite;
import isir.shared.dataaccess.service.HttpCacheService;

import java.net.URL;

public class HttpCacheServiceImpl implements HttpCacheService {
    private IHttpCacheDAO httpCacheDAO;

    public HttpCacheServiceImpl(IHttpCacheDAO httpCacheDAO) {
        this.httpCacheDAO = httpCacheDAO;
    }

    @Override
    public void insertWebsite(HttpWebsite httpWebsite) {
        httpCacheDAO.insertWebsite(httpWebsite);
    }

    @Override
    public HttpWebsite getWebsite(URL url) {
        return httpCacheDAO.getWebsite(url);
    }

    @Override
    public void updateWebsite(HttpWebsite httpWebsite) {
        httpCacheDAO.updateWebsite(httpWebsite);
    }
}
