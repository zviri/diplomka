package isir.shared.dataaccess.dto;

import isir.shared.dataaccess.dto.BirthNumber.Gender;
import org.apache.commons.lang3.StringUtils;

import java.net.URL;
import java.sql.Timestamp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static isir.shared.utils.IsirStringUtils.dbStringValue;

/**
 * The Insolvency DTO.
 */
public class Insolvency {

	private String id;
	private String debtorName;
	private String ico;
    private String referenceNumber;
    private Timestamp proposalTimestamp;
    private URL url;
    private Gender gender;
    private Integer birthNumberHashCode;
    private Integer yearOfBirth;
    private String debtorAddress;
    private Long regionId;

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
		return id;
	}

    /**
     * Gets debtor name.
     *
     * @return the debtor name
     */
    public String getDebtorName() {
		return debtorName;
	}

    /**
     * Gets ico.
     *
     * @return the ico
     */
    public String getIco() {
		return ico;
	}

    /**
     * Gets reference number.
     *
     * @return the reference number
     */
    public String getReferenceNumber() {
		return referenceNumber;
	}

    /**
     * Gets proposal timestamp.
     *
     * @return the proposal timestamp
     */
    public Timestamp getProposalTimestamp() {
		return proposalTimestamp;
	}

    /**
     * Gets url.
     *
     * @return the url
     */
    public URL getUrl() {
		return url;
	}

    /**
     * Gets insolvency vec.
     *
     * @return the insolvency vec
     */
    public int getInsolvencyVec() {
		return Integer.parseInt(id.replaceAll("[a-zA-Z]", "").split("/")[0]);
	}

    /**
     * Gets insolvency rok.
     *
     * @return the insolvency rok
     */
    public int getInsolvencyRok() {
		return Integer.parseInt(id.split("/")[1]);
	}

    /**
     * Gets senat number.
     *
     * @return the senat number
     */
    public int getSenatNumber() {
        Pattern pattern = Pattern.compile("^[a-zA-Z ]+([0-9]+)[a-zA-Z ]+.*$");
        Matcher matcher = pattern.matcher(getReferenceNumber());
        matcher.find();
        return Integer.parseInt(matcher.group(1));
    }

    /**
     * Gets debtor address.
     *
     * @return the debtor address
     */
    public String getDebtorAddress() {
		return debtorAddress;
	}

    /**
     * Gets gender.
     *
     * @return the gender
     */
    public Gender getGender() {
		return gender;
	}

    /**
     * Gets birth number hash code.
     *
     * @return the birth number hash code
     */
    public Integer getBirthNumberHashCode() {
		return birthNumberHashCode;
	}

    /**
     * Gets year of birth.
     *
     * @return the year of birth
     */
    public Integer getYearOfBirth() {
		return yearOfBirth;
	}

    /**
     * Gets region id.
     *
     * @return the region id
     */
    public Long getRegionId() {
        return regionId;
    }

    /**
     * Sets url.
     *
     * @param url the url
     * @return the url
     */
    public Insolvency setUrl(URL url) {
		this.url = url;
		return this;
	}

    /**
     * Sets ico.
     *
     * @param ico the ico
     * @return the ico
     */
    public Insolvency setIco(String ico) {
		this.ico = (ico == null || ico.trim().isEmpty()) ? null : ico.trim();
		return this;
	}

    /**
     * Sets proposal timestamp.
     *
     * @param proposalTimestamp the proposal timestamp
     * @return the proposal timestamp
     */
    public Insolvency setProposalTimestamp(Timestamp proposalTimestamp) {
		this.proposalTimestamp = proposalTimestamp;
		return this;
	}

    /**
     * Sets reference number.
     *
     * @param referenceNumber the reference number
     * @return the reference number
     */
    public Insolvency setReferenceNumber(String referenceNumber) {
		this.referenceNumber = StringUtils.normalizeSpace(dbStringValue(referenceNumber));
		return this;
	}

    /**
     * Sets debtor name.
     *
     * @param debtorName the debtor name
     * @return the debtor name
     */
    public Insolvency setDebtorName(String debtorName) {
		this.debtorName = StringUtils.normalizeSpace(dbStringValue(debtorName));
		return this;
	}

    /**
     * Sets id.
     *
     * @param id the id
     * @return the id
     */
    public Insolvency setId(String id) {
		this.id = dbStringValue(id);
		return this;
	}

    /**
     * Sets gender.
     *
     * @param gender the gender
     * @return the gender
     */
    public Insolvency setGender(Gender gender) {
		this.gender = gender;
		return this;
	}

    /**
     * Sets birth number hash code.
     *
     * @param birdNumberHashCode the bird number hash code
     * @return the birth number hash code
     */
    public Insolvency setBirthNumberHashCode(Integer birdNumberHashCode) {
		this.birthNumberHashCode = birdNumberHashCode;
		return this;
	}

    /**
     * Sets year of birth.
     *
     * @param yearOfBirth the year of birth
     * @return the year of birth
     */
    public Insolvency setYearOfBirth(Integer yearOfBirth) {
		this.yearOfBirth = yearOfBirth;
		return this;

	}

    /**
     * Sets debtor address.
     *
     * @param debtorAddress the debtor address
     * @return the debtor address
     */
    public Insolvency setDebtorAddress(String debtorAddress) {
		this.debtorAddress = StringUtils.normalizeSpace(debtorAddress);
		return this;
	}

    /**
     * Sets region id.
     *
     * @param regionId the region id
     * @return the region id
     */
    public Insolvency setRegionId(Long regionId) {
        this.regionId = regionId;
        return this;
    }

	@Override
	public String toString() {
		return "Insolvency [id=" + id + ", debtorName=" + debtorName + ", ico=" + ico + ", referenceNumber=" + referenceNumber + ", regionId=" + regionId +", proposalTimestamp="
				+ proposalTimestamp + ", url=" + url + ", gender=" + gender + ", birdNumberHashCode=" + birthNumberHashCode + ", yearOfBirth=" + yearOfBirth + ", debtorAddress=" + debtorAddress + "]";
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Insolvency that = (Insolvency) o;

        if (birthNumberHashCode != null ? !birthNumberHashCode.equals(that.birthNumberHashCode) : that.birthNumberHashCode != null)
            return false;
        if (debtorAddress != null ? !debtorAddress.equals(that.debtorAddress) : that.debtorAddress != null)
            return false;
        if (debtorName != null ? !debtorName.equals(that.debtorName) : that.debtorName != null) return false;
        if (gender != that.gender) return false;
        if (ico != null ? !ico.equals(that.ico) : that.ico != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (proposalTimestamp != null ? !proposalTimestamp.equals(that.proposalTimestamp) : that.proposalTimestamp != null)
            return false;
        if (referenceNumber != null ? !referenceNumber.equals(that.referenceNumber) : that.referenceNumber != null)
            return false;
        if (regionId != null ? !regionId.equals(that.regionId) : that.regionId != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        if (yearOfBirth != null ? !yearOfBirth.equals(that.yearOfBirth) : that.yearOfBirth != null) return false;

        return true;
    }


    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (debtorName != null ? debtorName.hashCode() : 0);
        result = 31 * result + (ico != null ? ico.hashCode() : 0);
        result = 31 * result + (referenceNumber != null ? referenceNumber.hashCode() : 0);
        result = 31 * result + (proposalTimestamp != null ? proposalTimestamp.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (birthNumberHashCode != null ? birthNumberHashCode.hashCode() : 0);
        result = 31 * result + (yearOfBirth != null ? yearOfBirth.hashCode() : 0);
        result = 31 * result + (debtorAddress != null ? debtorAddress.hashCode() : 0);
        result = 31 * result + (regionId != null ? regionId.hashCode() : 0);
        return result;
    }
}
