package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dao.IAdministratorTypesDAO;
import isir.shared.dataaccess.dto.AdministratorType;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * The AdministratorTypesDAOImpl PostgreSQL implementation.
 */
public class AdministratorTypesDAOImpl extends JdbcDaoSupport implements IAdministratorTypesDAO {

    @Override
    public AdministratorType getAdministratorType(final String description) {
        List<AdministratorType> administratorTypes = getJdbcTemplate().query("SELECT * FROM administrator_types_tab WHERE description=?", new Object[]{description}, new RowMapper<AdministratorType>() {
            @Override
            public AdministratorType mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new AdministratorType().setId(rs.getLong("id"))
                        .setDescription(description);
            }
        });
        return administratorTypes.size() > 0 ? administratorTypes.get(0) : null;
    }

    @Override
    public void insert(AdministratorType administratorType) {
        getJdbcTemplate().update("INSERT INTO administrator_types_tab(id, description) VALUES (DEFAULT, ?)", administratorType.getDescription());
    }

    @Override
    public void update(AdministratorType administratorType) {
        getJdbcTemplate().update("UPDATE administrator_types_tab SET description=? WHERE id=?", administratorType.getDescription(), administratorType.getId());
    }

}
