package isir.shared.dataaccess.dao;

import isir.shared.dataaccess.dto.WsEvent;

import java.util.Calendar;
import java.util.List;

/**
 * DAO interface for working with ws events.
 */
public interface IWsEventsDAO {

    /**
     * Gets max id.
     *
     * @return the max id
     */
    public abstract long getMaxId();

    /**
     * Insert new ws event.
     *
     * @param wsEvent the ws event
     */
    public abstract void insert(WsEvent wsEvent);

    /**
     * Gets event.
     *
     * @param eventId the event id
     * @return the event
     */
    public abstract WsEvent getEvent(Long eventId);

    /**
     * Update existing event.
     *
     * @param wsEvent the ws event
     */
    public abstract void update(WsEvent wsEvent);

    /**
     * Gets ws events from id.
     *
     * @param eventId the event id
     * @return the ws events from id
     */
    List<WsEvent> getWsEventsFromId(Long eventId);

    /**
     * Gets events.
     *
     * @param startDate the start date
     * @param endDate the end date
     * @return the events
     */
    List<WsEvent> getEvents(Calendar startDate, Calendar endDate);

    /**
     * Gets event ids.
     *
     * @param startDate the start date
     * @param endDate the end date
     * @return the event ids
     */
    List<Long> getEventIds(Calendar startDate, Calendar endDate);
}
