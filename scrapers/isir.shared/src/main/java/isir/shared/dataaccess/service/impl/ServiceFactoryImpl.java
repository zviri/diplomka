package isir.shared.dataaccess.service.impl;

import isir.shared.dataaccess.dao.DAOFactory;
import isir.shared.dataaccess.service.*;
import isir.shared.isirpub001.IIsirPub001Service;
import isir.shared.isirpub001.IsirPub001ServiceImpl;

public class ServiceFactoryImpl extends ServiceFactory {

    DAOFactory daoFactory = DAOFactory.getDAOFactory();

    @Override
    public WsEventsService getWsEventsService() {
        return new WsEventsServiceImpl(daoFactory.getWsEventsDAO());
    }

    @Override
    public FilesService getFilesService() {
        return new FileServiceImpl(daoFactory.getFileDAO(), daoFactory.getFileTypeDAO());
    }

    @Override
    public InsolvencyService getInsolvencyService() {
        return new InsolvencyServiceImpl(daoFactory.getInsolvencyDAO(),
                                         daoFactory.getAdministratorsDAO(),
                                         daoFactory.getInsolvenciesAdministratorsDAO(),
                                         daoFactory.getInsolvencyStatesDAO(),
                                         daoFactory.getInsolvencyStatesTypeDAO(),
                                         daoFactory.getAdministratorTypesDAO(),
                                         daoFactory.getSubjectsDAO());
    }

    @Override
    public HttpCacheService getHttpCacheService() {
        return new HttpCacheServiceImpl(daoFactory.getHttpCacheDAO());
    }

    @Override
    public IIsirPub001Service getIIsirPub001Service() throws Exception {
        return new IsirPub001ServiceImpl();
    }
}
