package isir.shared.dataaccess.service;

import isir.shared.dataaccess.service.impl.ServiceFactoryImpl;
import isir.shared.isirpub001.IIsirPub001Service;

import java.rmi.RemoteException;

/**
 * Factory for obtaining service objects.
 */
public abstract class ServiceFactory {

    /**
     * Gets ws events service.
     *
     * @return the ws events service
     */
    public abstract WsEventsService getWsEventsService();

    /**
     * Gets files service.
     *
     * @return the files service
     */
    public abstract FilesService getFilesService();

    /**
     * Gets insolvency service.
     *
     * @return the insolvency service
     */
    public abstract InsolvencyService getInsolvencyService();

    /**
     * Gets http cache service.
     *
     * @return the http cache service
     */
    public abstract HttpCacheService getHttpCacheService();

    /**
     * Gets i isir pub 001 service.
     *
     * @return the i isir pub 001 service
     * @throws Exception the exception
     */
    public abstract IIsirPub001Service getIIsirPub001Service() throws Exception;

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static ServiceFactory getInstance() {
        return new ServiceFactoryImpl();
    }
}

