package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dao.IInsolvenciesAdministratorsDAO;
import isir.shared.dataaccess.dto.InsolvencyAdministrator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * The InsolvenciesAdministratorsDAOImpl PostgreSQL implementation.
 */
public class InsolvenciesAdministratorsDAOImpl extends JdbcDaoSupport implements IInsolvenciesAdministratorsDAO {

	private final class InsolvenciesAdministratorsRowMapper implements RowMapper<InsolvencyAdministrator> {
		@Override
		public InsolvencyAdministrator mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new InsolvencyAdministrator().setInsolvencyId(rs.getString("insolvency_id"))
										        .setAdministratorId(rs.getLong("administrator_id"))
										        .setAdministratorType(rs.getLong("administrator_type_id"));
		}
	}

	@Override
	public void insert(InsolvencyAdministrator fileAdministrator) {
		getJdbcTemplate().update("INSERT INTO insolvencies_administrators_tab (insolvency_id, administrator_id, administrator_type_id) VALUES (?,?,?)",
				                 fileAdministrator.getInsolvencyId(), 
				                 fileAdministrator.getAdministratorId(),
				                 fileAdministrator.getAdministratorType());
	}
	
	@Override
	public void delete(String insolvecnyId) {
		getJdbcTemplate().update("DELETE FROM insolvencies_administrators_tab WHERE insolvency_id = ?", insolvecnyId);
	}

	@Override
	public InsolvencyAdministrator getInsolvencyAdministrator(String insolvencyId, Long administratorId, Long administratorTypeId) {
		List<InsolvencyAdministrator> filesAdministrators = getJdbcTemplate().query("SELECT * FROM insolvencies_administrators_tab WHERE insolvency_id = ? AND administrator_id = ? AND administrator_type_id = ?",
			                                                                  new Object[] { insolvencyId, administratorId, administratorTypeId },
			                                                                  new InsolvenciesAdministratorsRowMapper());
		return filesAdministrators.size() == 1 ? filesAdministrators.get(0) : null;
	}
}
