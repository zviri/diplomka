package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dao.*;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

/**
 * The DAO factory implementation.
 */
public class DAOFactoryImpl extends DAOFactory {

    /**
     * Instantiates a new DAO factory.
     *
     * @param driver the driver
     */
    public DAOFactoryImpl(String driver) {
		super(driver);
	}

	@Override
	public IHttpCacheDAO getHttpCacheDAO() {
		return (IHttpCacheDAO) setDataSource(new HttpCacheDAOImpl());
	}

	@Override
	public IInsolvencyDAO getInsolvencyDAO() {
		return (IInsolvencyDAO) setDataSource(new InsolvencyDAOImpl());
	}

	@Override
	public IFileDAO getFileDAO() {
		return (IFileDAO) setDataSource(new FileDAOImpl());
	}

	@Override
	public IInsolvencyStatesTypeDAO getInsolvencyStatesTypeDAO() {
		return (IInsolvencyStatesTypeDAO) setDataSource(new InsolvencyStatesTypeDAOImpl());
	}

	@Override
	public IInsolvencyStatesDAO getInsolvencyStatesDAO() {
		return (IInsolvencyStatesDAO) setDataSource(new InsolvencyStatesDAOImpl());
	}

	@Override
	public IInsolvenciesAdministratorsDAO getInsolvenciesAdministratorsDAO() {
		return (IInsolvenciesAdministratorsDAO) setDataSource(new InsolvenciesAdministratorsDAOImpl());
	}

	@Override
	public IAdministratorsDAO getAdministratorsDAO() {
		return (IAdministratorsDAO) setDataSource(new AdministratorsDAOImpl());
	}

	@Override
	public IWsEventsDAO getWsEventsDAO() {
		return (IWsEventsDAO) setDataSource(new WsEventsDAOImpl());
	}

    @Override
    public ISubjectsDAO getSubjectsDAO() {
        return (ISubjectsDAO) setDataSource(new SubjectsDAOImpl());
    }

    @Override
    public IAdministratorTypesDAO getAdministratorTypesDAO() {
        return (IAdministratorTypesDAO) setDataSource(new AdministratorTypesDAOImpl());
    }

    @Override
    public IFileTypeDAO getFileTypeDAO() {
        return (IFileTypeDAO) setDataSource(new FileTypeDAOImpl());
    }

    /**
     * Sets data source.
     *
     * @param daoObject the dao object
     * @return the data source
     */
    public JdbcDaoSupport setDataSource(JdbcDaoSupport daoObject) {
		daoObject.setDataSource(getDataSource());
		return daoObject;
	}
}
