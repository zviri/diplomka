package isir.shared.dataaccess.dao;

import isir.shared.dataaccess.dto.Administrator;


/**
 * DAO interface for working with administrators.
 */
public interface IAdministratorsDAO {

    /**
     * Inserts new administrator.
     *
     * @param administrator the administrator
     */
    public abstract void insert(Administrator administrator);

    /**
     * Gets administrator.
     *
     * @param administrator the administrator
     * @return the administrator
     */
    public abstract Administrator getAdministrator(String administrator);

    /**
     * Updates existing administrator.
     *
     * @param administrator the administrator
     */
    void update(Administrator administrator);
}
