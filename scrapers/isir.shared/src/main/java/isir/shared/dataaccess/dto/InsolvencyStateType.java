package isir.shared.dataaccess.dto;

import org.apache.commons.lang3.StringUtils;

/**
 * The Insolvency state type DTO.
 */
public class InsolvencyStateType {

    private Long id;
    private String textIdentifier;
    private String description;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets text identifier.
     *
     * @return the text identifier
     */
    public String getTextIdentifier() {
        return textIdentifier;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets id.
     *
     * @param id the id
     * @return the id
     */
    public InsolvencyStateType setId(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Sets text identifier.
     *
     * @param textIdentifier the text identifier
     * @return the text identifier
     */
    public InsolvencyStateType setTextIdentifier(String textIdentifier) {
        this.textIdentifier = StringUtils.normalizeSpace(textIdentifier);
        return this;
    }

    /**
     * Sets description.
     *
     * @param description the description
     * @return the description
     */
    public InsolvencyStateType setDescription(String description) {
        this.description = StringUtils.normalizeSpace(description);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InsolvencyStateType that = (InsolvencyStateType) o;

        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (textIdentifier != null ? !textIdentifier.equals(that.textIdentifier) : that.textIdentifier != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (textIdentifier != null ? textIdentifier.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
