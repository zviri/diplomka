package isir.shared.dataaccess.service;

import isir.shared.dataaccess.dto.HttpWebsite;

import java.net.URL;

/**
 * Facade for working with cached web sites over the existing DAO objects.
 */
public interface HttpCacheService {

    /**
     * Insert new website.
     *
     * @param httpWebsite the http website
     */
    void insertWebsite(HttpWebsite httpWebsite);

    /**
     * Gets website.
     *
     * @param url the url
     * @return the website
     */
    HttpWebsite getWebsite(URL url);

    /**
     * Updates existing website.
     *
     * @param httpWebsite the http website
     */
    void updateWebsite(HttpWebsite httpWebsite);
}
