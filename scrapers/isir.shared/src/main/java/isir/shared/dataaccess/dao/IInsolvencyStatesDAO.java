package isir.shared.dataaccess.dao;

import isir.shared.dataaccess.dto.InsolvencyState;

/**
 * DAO interface for working with insolvency states.
 */
public interface IInsolvencyStatesDAO {

    /**
     * Inserts new insolvency state.
     *
     * @param state the state
     */
    public abstract void insert(InsolvencyState state);

    /**
     * Updates existing insolvency state.
     *
     * @param state the state
     */
    public abstract void update(InsolvencyState state);

    /**
     * Gets last action id.
     *
     * @return the last action iD
     */
    public abstract Long getLastActionID();

    /**
     * Gets insolvency state by action id.
     *
     * @param actionID the action iD
     * @return the insolvency state by action id
     */
    public abstract InsolvencyState getInsolvencyStateByActionId(Long actionID);
}
