package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dao.IInsolvencyDAO;
import isir.shared.dataaccess.dto.BirthNumber.Gender;
import isir.shared.dataaccess.dto.Insolvency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static isir.shared.utils.IsirStringUtils.stringToUrl;
import static isir.shared.utils.IsirStringUtils.urlToString;

/**
 * The InsolvencyDAOImpl PostgreSQL implementation.
 */
public class InsolvencyDAOImpl extends JdbcDaoSupport implements IInsolvencyDAO {

    private static Logger log = LoggerFactory.getLogger(InsolvencyDAOImpl.class);

	private final class InsolvencyRowMapper implements RowMapper<Insolvency> {
		@Override
		public Insolvency mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Insolvency().setId(rs.getString("id"))
								   .setDebtorName(rs.getString("debtor_name"))
								   .setIco(rs.getString("ico"))
								   .setReferenceNumber(rs.getString("reference_number"))
								   .setProposalTimestamp(rs.getTimestamp("proposal_timestamp"))
								   .setUrl(stringToUrl(rs.getString("url")))
								   .setBirthNumberHashCode((Integer) rs.getObject("birth_number_hash_code"))
								   .setDebtorAddress(rs.getString("debtor_address"))
								   .setGender(rs.getString("gender") == null ? null : Gender.valueOf(rs.getString("gender")))
								   .setYearOfBirth((Integer) rs.getObject("year_of_birth"))
                                   .setRegionId((Long) rs.getObject("region_id"));
		}
	}

	@Override
	public void insert(Insolvency insolvency) {
		getJdbcTemplate().update("INSERT INTO insolvency_tab (id, debtor_name, ico, reference_number, proposal_timestamp, url, birth_number_hash_code, debtor_address, gender, year_of_birth, region_id) VALUES (?,?,?,?,?,?,?,?,?,?,?)",
								 insolvency.getId(),
								 insolvency.getDebtorName(),
								 insolvency.getIco(),
								 insolvency.getReferenceNumber(),
								 insolvency.getProposalTimestamp(),
								 urlToString(insolvency.getUrl()), 
								 insolvency.getBirthNumberHashCode(),
								 insolvency.getDebtorAddress(),
								 insolvency.getGender() == null ? null : insolvency.getGender().name(),
								 insolvency.getYearOfBirth(),
                                 insolvency.getRegionId());
	}

	@Override
	public Insolvency getInsolvency(String insolvencyId) {
		List<Insolvency> insolvencies = getJdbcTemplate().query("SELECT * FROM insolvency_tab WHERE id = ?", new Object[] { insolvencyId }, new InsolvencyRowMapper());
		return insolvencies.size() == 1 ? insolvencies.get(0) : null;

	}

	@Override
	public void update(Insolvency insolvency) {
		getJdbcTemplate().update("UPDATE insolvency_tab SET debtor_name = ?, ico = ?, reference_number = ?, proposal_timestamp = ?, url = ?, birth_number_hash_code = ?, debtor_address = ?, gender = ?, year_of_birth = ?, region_id=? WHERE id = ?",
								 insolvency.getDebtorName(),
								 insolvency.getIco(),
								 insolvency.getReferenceNumber(),
								 insolvency.getProposalTimestamp(),
								 urlToString(insolvency.getUrl()),
								 insolvency.getBirthNumberHashCode(),
								 insolvency.getDebtorAddress(),
								 insolvency.getGender() == null ? null : insolvency.getGender().name(),
								 insolvency.getYearOfBirth(),
                                 insolvency.getRegionId(),
								 insolvency.getId());
	}

    @Override
    public void cleanInsolvencyStates() {
        try {
            getConnection().prepareStatement("SELECT clean_insolvency_states()").execute();
        } catch (Exception e) {
            log.error("", e);
        }
    }
}
