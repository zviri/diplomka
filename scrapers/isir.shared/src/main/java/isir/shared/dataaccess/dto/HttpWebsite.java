package isir.shared.dataaccess.dto;

import java.net.URL;
import java.sql.Timestamp;

/**
 * The Http website DTO.
 */
public class HttpWebsite {

	private URL url;
	private String content;
	private Timestamp downloadDate;

    /**
     * Gets url.
     *
     * @return the url
     */
    public URL getUrl() {
		return url;
	}

    /**
     * Sets url.
     *
     * @param url the url
     * @return the url
     */
    public HttpWebsite setUrl(URL url) {
		this.url = url;
        return this;
	}

    /**
     * Gets content.
     *
     * @return the content
     */
    public String getContent() {
		return content;
	}

    /**
     * Sets content.
     *
     * @param content the content
     * @return the content
     */
    public HttpWebsite setContent(String content) {
		this.content = content;
        return this;
	}

    /**
     * Gets download date.
     *
     * @return the download date
     */
    public Timestamp getDownloadDate() {
		return downloadDate;
	}

    /**
     * Sets download date.
     *
     * @param downloadDate the download date
     * @return the download date
     */
    public HttpWebsite setDownloadDate(Timestamp downloadDate) {
		this.downloadDate = downloadDate;
        return this;
	}

}
