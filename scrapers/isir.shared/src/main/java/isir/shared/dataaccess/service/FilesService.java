package isir.shared.dataaccess.service;

import isir.shared.dataaccess.dto.AttachedFile;
import isir.shared.dataaccess.dto.FileType;

import java.net.URL;
import java.sql.Date;
import java.util.List;

/**
 * Facade for working with files over the existing DAO objects.
 */
public interface FilesService {

    /**
     * Inserts new or updates existing file.
     *
     * @param file the file
     */
    public void save(AttachedFile file);

    /**
     * Gets file type.
     *
     * @param description the description
     * @return the file type
     */
    FileType getFileType(String description);

    /**
     * Inserts new or updates existing file type.
     *
     * @param fileType the file type
     */
    void save(FileType fileType);

    /**
     * Gets file.
     *
     * @param documentType the document type
     * @param insolvencyId the insolvency id
     * @param url the url
     * @return the file
     */
    AttachedFile getFile(Long documentType, String insolvencyId, URL url);

    /**
     * Inserts new or updates existing file or file type.
     *
     * @param file the file
     * @param fileType the file type
     */
    void save(AttachedFile file, FileType fileType);

    /**
     * Gets file type.
     *
     * @param fileTypeId the file type id
     * @return the file type
     */
    FileType getFileType(Long fileTypeId);

    /**
     * Gets files by insolvency id.
     *
     * @param insolvencyId the insolvency id
     * @return the files by insolvency id
     */
    List<AttachedFile> getFilesByInsolvencyId(String insolvencyId);

    /**
     * Gets file.
     *
     * @param fileId the file id
     * @return the file
     */
    AttachedFile getFile(Long fileId);
}
