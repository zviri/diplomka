package isir.shared.dataaccess.dao.impl;

import isir.shared.dataaccess.dao.IInsolvencyStatesTypeDAO;
import isir.shared.dataaccess.dto.InsolvencyStateType;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * The InsolvencyStatesTypeDAOImpl PostgreSQL implementation.
 */
public class InsolvencyStatesTypeDAOImpl extends JdbcDaoSupport implements IInsolvencyStatesTypeDAO {

    @Override
    public List<InsolvencyStateType> getAllInsolvencyStatesTypes() {
        return getJdbcTemplate().query("SELECT * FROM insolvency_states_types_tab", new InsolvencyStateTypeRowMapper());
    }

    @Override
    public void update(InsolvencyStateType insolvencyStateType) {
        getJdbcTemplate().update("UPDATE insolvency_states_types_tab SET text_identifier=?, description=? WHERE id=?",
                                 insolvencyStateType.getTextIdentifier(),
                                 insolvencyStateType.getDescription(),
                                 insolvencyStateType.getId());
    }

    @Override
    public InsolvencyStateType getInsolvencyStateType(String textIdentifier) {
        if (textIdentifier == null) {
            return null;
        }
        List<InsolvencyStateType> insolvencyStateTypes = getJdbcTemplate().query("SELECT * FROM insolvency_states_types_tab WHERE text_identifier=?", new Object[]{textIdentifier}, new InsolvencyStateTypeRowMapper());
        return insolvencyStateTypes.size() > 0 ? insolvencyStateTypes.get(0) : null;
    }

    @Override
    public void insert(InsolvencyStateType insolvencyStateType) {
        getJdbcTemplate().update("INSERT INTO insolvency_states_types_tab(id, text_identifier, description) VALUES (DEFAULT, ?, ?)",
                                  insolvencyStateType.getTextIdentifier(),
                                  insolvencyStateType.getDescription());
    }

    private static class InsolvencyStateTypeRowMapper implements RowMapper<InsolvencyStateType> {
        public InsolvencyStateType mapRow(ResultSet rs, int rowNum) throws SQLException {
            InsolvencyStateType state = new InsolvencyStateType();
            return state.setId(rs.getLong("id"))
                    .setTextIdentifier(rs.getString("text_identifier"))
                    .setDescription(rs.getString("description"));
        }
    }
}
