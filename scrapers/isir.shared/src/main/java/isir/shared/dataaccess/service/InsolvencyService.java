package isir.shared.dataaccess.service;

import isir.shared.dataaccess.dto.*;

import java.util.List;

/**
 * Facade for working with insolvencies over the existing DAO objects.
 */
public interface InsolvencyService {

    /**
     * Creates new or updates existing insolvency.
     *
     * @param insolvency the insolvency
     */
    void save(Insolvency insolvency);

    /**
     * Gets insolvency.
     *
     * @param insolvencyId the insolvency id
     * @return the insolvency
     */
    Insolvency getInsolvency(String insolvencyId);

    /**
     * Creates new or updates existing insolvency state.
     *
     * @param insolvencyState the insolvency state
     */
    void save(InsolvencyState insolvencyState);

    /**
     * Creates new or updates existing administrator.
     *
     * @param administrator the administrator
     */
    void save(Administrator administrator);

    /**
     * Creates new or updates existing insolvency state type.
     *
     * @param insolvencyStateType the insolvency state type
     */
    void save(InsolvencyStateType insolvencyStateType);

    /**
     * Creates new or updates existing administrator.
     *
     * @param administratorType the administrator type
     */
    void save(AdministratorType administratorType);

    /**
     * Creates new or updates existing insolvency, administrator type, administrator.
     *
     * @param insolvency the insolvency
     * @param administratorType the administrator type
     * @param administrator the administrator
     */
    void save(Insolvency insolvency, AdministratorType administratorType, Administrator administrator);

    /**
     * Creates new or updates existing insolvency, insolvency state, insolvency state type.
     *
     * @param insolvency the insolvency
     * @param insolvencyState the insolvency state
     * @param insolvencyStateType the insolvency state type
     */
    void save(Insolvency insolvency, InsolvencyState insolvencyState, InsolvencyStateType insolvencyStateType);

    /**
     * Gets administrator.
     *
     * @param administrator the administrator
     * @return the administrator
     */
    Administrator getAdministrator(String administrator);

    /**
     * Deletes insolvency.
     *
     * @param insolvecnyId the insolvecny id
     */
    void delete(String insolvecnyId);

    /**
     * Gets file administrator.
     *
     * @param insolvencyId the insolvency id
     * @param administratorId the administrator id
     * @param administratorTypeId the administrator type id
     * @return the file administrator
     */
    InsolvencyAdministrator getFileAdministrator(String insolvencyId, Long administratorId, Long administratorTypeId);

    /**
     * Gets last action iD.
     *
     * @return the last action iD
     */
    Long getLastActionID();

    /**
     * Gets state by action id.
     *
     * @param actionID the action iD
     * @return the state by action id
     */
    InsolvencyState getStateByActionId(Long actionID);

    /**
     * Gets insolvency state type.
     *
     * @param textIdentifier the text identifier
     * @return the insolvency state type
     */
    InsolvencyStateType getInsolvencyStateType(String textIdentifier);

    /**
     * Gets all insolvency states types.
     *
     * @return the all insolvency states types
     */
    List<InsolvencyStateType> getAllInsolvencyStatesTypes();

    /**
     * Creates new or updates existing subject.
     *
     * @param subject the subject
     */
    void save(Subject subject);

    /**
     * Gets subject.
     *
     * @param name the name
     * @return the subject
     */
    Subject getSubject(String name);

    /**
     * Clean insolvency states.
     */
    void cleanInsolvencyStates();
}
