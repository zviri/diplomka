package isir.wscachescraper;

import com.lexicalscope.jewel.cli.CliFactory;
import isir.wscachescraper.cache.ScrapeToCache;
import isir.wscachescraper.config.CommandLineParameters;
import isir.wscachescraper.scrapers.RunScrapers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main class for running ws-cache-scraper.
 */
public class Main {

    private static Logger log = LoggerFactory.getLogger(Main.class);

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {
        CommandLineParameters commandLineParameters = CliFactory.parseArguments(CommandLineParameters.class, args);
        log.info("**************************************************************************");
        log.info("Ws-cache-scraper START!");
        log.info("**************************************************************************");

        log.info("TaskName: {}", commandLineParameters.getTaskName());
        try {
            switch (commandLineParameters.getTaskName()) {
                case "scrapeToCache":
                    new ScrapeToCache().run(commandLineParameters);
                    break;
                case "runScrapers":
                    new RunScrapers().run(commandLineParameters);
                    break;
                default:
                    log.error("Task with name {} is not defined! Shutting down...", commandLineParameters.getTaskName());
            }
        } catch (Exception e) {
            log.error("Ws-cache-scraper FAILED!", e);
            throw e; // throwing for jenkins to notice failure
        }

        log.info("**************************************************************************");
        log.info("Ws-cache-scraper FINISHED!");
        log.info("**************************************************************************");
    }
}
