package isir.wscachescraper.scrapers;

import isir.shared.dataaccess.dto.WsEvent;
import isir.shared.dataaccess.service.FilesService;
import isir.shared.dataaccess.service.InsolvencyService;

/**
 * Interface for all ws scrapers
 */
public interface Scraper {

    /**
     * Scrape single event.
     *
     * @param event the event
     * @throws Exception the exception
     */
    public void scrape(WsEvent event) throws Exception;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName();

    /**
     * Sets insolvency service.
     *
     * @param insolvencyService the insolvency service
     */
    public void setInsolvencyService(InsolvencyService insolvencyService);

    /**
     * Sets file service.
     *
     * @param fileService the file service
     */
    public void setFileService(FilesService fileService);
}
