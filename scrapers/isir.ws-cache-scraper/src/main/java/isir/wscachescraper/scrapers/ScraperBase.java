package isir.wscachescraper.scrapers;

import isir.shared.dataaccess.service.FilesService;
import isir.shared.dataaccess.service.InsolvencyService;

/**
 * Base class with common functionaltiy for all ws scrapers.
 */
public abstract class ScraperBase implements Scraper {

    private InsolvencyService insolvencyService;
    private FilesService fileService;

    @Override
    public void setInsolvencyService(InsolvencyService insolvencyService) {
        this.insolvencyService = insolvencyService;
    }

    @Override
    public void setFileService(FilesService fileService) {
        this.fileService = fileService;
    }

    /**
     * Gets insolvency service.
     *
     * @return the insolvency service
     */
    protected InsolvencyService getInsolvencyService() {
        return insolvencyService;
    }

    /**
     * Gets file service.
     *
     * @return the file service
     */
    protected FilesService getFileService() {
        return fileService;
    }
}
