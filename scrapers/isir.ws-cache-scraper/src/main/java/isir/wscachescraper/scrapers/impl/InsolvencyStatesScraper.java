package isir.wscachescraper.scrapers.impl;

import isir.shared.dataaccess.dto.InsolvencyState;
import isir.shared.dataaccess.dto.InsolvencyStateType;
import isir.shared.dataaccess.dto.WsEvent;
import isir.shared.isirpub001.IsirPub001Stub;
import isir.shared.utils.WsUtils;
import isir.wscachescraper.scrapers.ScraperBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Scraper for extracting and storing insolvency proceedings states.
 */
public class InsolvencyStatesScraper extends ScraperBase {

    private static Logger log = LoggerFactory.getLogger(InsolvencyStatesScraper.class);

    @Override
    public void scrape(WsEvent event) throws Exception {
        IsirPub001Stub.IsirPub001Data isirData = event.getEvent();
        String insolvencyId = event.getInsolvencyId();
        String insolvencyStateTextIdentifier = WsUtils.getInsolvencyState(isirData);
        if (insolvencyStateTextIdentifier == null || insolvencyStateTextIdentifier.trim().isEmpty()) {
            return;
        }

        InsolvencyStateType insolvencyStateType = getInsolvencyService().getInsolvencyStateType(insolvencyStateTextIdentifier);
        if (insolvencyStateType == null) {
            log.warn("Insolvency State Type: {} not found in DB!", insolvencyStateTextIdentifier);
            insolvencyStateType = new InsolvencyStateType().setTextIdentifier(insolvencyStateTextIdentifier);
            getInsolvencyService().save(insolvencyStateType);
        }

        InsolvencyState insolvencyState = new InsolvencyState().setActionID(event.getId())
                                                               .setState(insolvencyStateType.getId())
                                                               .setStateChangeTimestamp(event.getTimestamp())
                                                               .setInsolvencyID(insolvencyId);
        getInsolvencyService().save(insolvencyState);
    }

    @Override
    public String getName() {
        return "InsolvencyStatesScraper";
    }
}
