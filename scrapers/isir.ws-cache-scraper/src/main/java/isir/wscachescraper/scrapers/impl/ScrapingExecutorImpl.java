package isir.wscachescraper.scrapers.impl;

import isir.shared.dataaccess.dto.WsEvent;
import isir.shared.dataaccess.service.FilesService;
import isir.shared.dataaccess.service.InsolvencyService;
import isir.shared.dataaccess.service.WsEventsService;
import isir.wscachescraper.scrapers.Scraper;
import isir.wscachescraper.scrapers.ScrapingExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Implementation of the scraping executor.
 */
public class ScrapingExecutorImpl implements ScrapingExecutor {

    private static Logger log = LoggerFactory.getLogger(ScrapingExecutorImpl.class);

    /**
     * The Scrapers.
     */
    public List<Scraper> scrapers = new ArrayList<>();
    private Calendar startDate;
    private Calendar endDate;
    private WsEventsService wsEventsService;
    private FilesService filesService;
    private InsolvencyService insolvencyService;

    /**
     * Instantiates a new Scraping executor impl.
     *
     * @param startDate the start date
     * @param endDate the end date
     * @param wsEventsService the ws events service
     * @param filesService the files service
     * @param insolvencyService the insolvency service
     */
    public ScrapingExecutorImpl(Calendar startDate, Calendar endDate, WsEventsService wsEventsService, FilesService filesService, InsolvencyService insolvencyService) {
        this.filesService = filesService;
        this.insolvencyService = insolvencyService;
        this.startDate = (Calendar) startDate.clone(); // better save than sorry
        this.endDate = (Calendar) endDate.clone();
        this.wsEventsService = wsEventsService;
    }

    @Override
    public void registerScraper(Scraper scraper) {
        scrapers.add(scraper);
    }

    @Override
    public List<Scraper> getRegisteredScrapers() {
        return Collections.unmodifiableList(scrapers);
    }

    @Override
    public void execute() throws Exception {
        List<Long> events = wsEventsService.getEventIds(startDate, endDate);
        for (int i = 0; i < events.size(); i++) {
            log.info("Processing event: {}/{}", i + 1, events.size());
            for (Scraper scraper : scrapers) {
                WsEvent event = wsEventsService.getEvent(events.get(i));
                scraper.setFileService(filesService);
                scraper.setInsolvencyService(insolvencyService);
                try {
                    scraper.scrape(event);
                } catch (Exception e) {
                    log.error("Scraper " + scraper.getName() + " FAILED! On event: " + event.getId(), e);
                }
            }
        }
    }
}
