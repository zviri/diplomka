package isir.wscachescraper.scrapers.impl;

import isir.shared.dataaccess.dto.Subject;
import isir.shared.dataaccess.dto.WsEvent;
import isir.shared.isirpub001.IsirPub001Stub;
import isir.shared.utils.XMLUtils;
import isir.wscachescraper.scrapers.ScraperBase;
import org.apache.commons.lang3.StringUtils;

/**
 * Scraper for extracting and storing subjects in the Insolvency Register.
 */
public class SubjectsScraper extends ScraperBase {

    @Override
    public void scrape(WsEvent event) throws Exception{
        IsirPub001Stub.IsirPub001Data data = event.getEvent();
        String subjectName = StringUtils.normalizeSpace(XMLUtils.getElementValue("//osoba/nazevOsoby", data.getPoznamka()));
        String ico =  StringUtils.normalizeSpace(XMLUtils.getElementValue("//osoba/ic", data.getPoznamka()));
        String form =  StringUtils.normalizeSpace(XMLUtils.getElementValue("//osoba/druhOsoby", data.getPoznamka()));
        String legalForm =  StringUtils.normalizeSpace(XMLUtils.getElementValue("//osoba/druhPravniForma", data.getPoznamka()));
        String addressForm =  StringUtils.normalizeSpace(XMLUtils.getElementValue("//osoba/adresa/druhAdresy", data.getPoznamka()));
        String addressCity =  StringUtils.normalizeSpace(XMLUtils.getElementValue("//osoba/adresa/mesto", data.getPoznamka()));
        String addressStreet =  StringUtils.normalizeSpace(XMLUtils.getElementValue("//osoba/adresa/ulice", data.getPoznamka()));
        String addressDescriptionNumber =  StringUtils.normalizeSpace(XMLUtils.getElementValue("//osoba/adresa/cisloPopisne", data.getPoznamka()));
        String addressCountry =  StringUtils.normalizeSpace(XMLUtils.getElementValue("//osoba/adresa/zeme", data.getPoznamka()));
        String addressZipCode =  StringUtils.normalizeSpace(XMLUtils.getElementValue("//osoba/adresa/psc", data.getPoznamka()));
        String degreeBefore =  StringUtils.normalizeSpace(XMLUtils.getElementValue("//osoba/titulPred", data.getPoznamka()));
        String degreeAfter =  StringUtils.normalizeSpace(XMLUtils.getElementValue("//osoba/titulZa", data.getPoznamka()));

        if (subjectName != null) {
            if (ico != null) {
                ico = ico.replaceFirst("^0+", "");
            }
            getInsolvencyService().save(new Subject().setName(subjectName)
                                                     .setIco(ico)
                                                     .setForm(form)
                                                     .setLegalForm(legalForm)
                                                     .setAddressCity(addressCity)
                                                     .setAddressCountry(addressCountry)
                                                     .setAddressDescriptionNumber(addressDescriptionNumber)
                                                     .setAddressForm(addressForm)
                                                     .setAddresStreet(addressStreet)
                                                     .setAddressZipCode(addressZipCode)
                                                     .setDegreeBefore(degreeBefore)
                                                     .setDegreeAfter(degreeAfter));
        }
    }

    @Override
    public String getName() {
        return "SubjectsScraper";
    }
}
