package isir.wscachescraper.scrapers;

import isir.shared.dataaccess.service.FilesService;
import isir.shared.dataaccess.service.InsolvencyService;
import isir.shared.dataaccess.service.ServiceFactory;
import isir.shared.dataaccess.service.WsEventsService;
import isir.shared.utils.TimeUtils;
import isir.wscachescraper.config.CommandLineParameters;
import isir.wscachescraper.scrapers.impl.CreditorProposalScraper;
import isir.wscachescraper.scrapers.impl.SubjectsScraper;
import isir.wscachescraper.scrapers.impl.InsolvencyStatesScraper;
import isir.wscachescraper.scrapers.impl.ScrapingExecutorImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;

/**
 * Class for runScraper task.
 */
public class RunScrapers {

    private static Logger log = LoggerFactory.getLogger(RunScrapers.class);

    /**
     * Run runScrapers task.
     *
     * @param commandLineParameters the command line parameters
     * @throws Exception the exception
     */
    public void run(CommandLineParameters commandLineParameters) throws Exception {
        Calendar startDate = TimeUtils.stringToCalendar(commandLineParameters.getStartDate());
        Calendar endDate = TimeUtils.stringToCalendar(commandLineParameters.getEndDate());
        Calendar dayBeforeYesterday = Calendar.getInstance();
        dayBeforeYesterday.add(Calendar.DATE, -2);
        TimeUtils.resetTime(dayBeforeYesterday);
        startDate = startDate == null ? dayBeforeYesterday : startDate;
        endDate = endDate == null ? Calendar.getInstance() : endDate;
        log.info("startDate: {}", TimeUtils.calendarToString(startDate));
        log.info("endDate: {}", TimeUtils.calendarToString(endDate));

        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        WsEventsService wsEventsService = serviceFactory.getWsEventsService();
        InsolvencyService insolvencyService = serviceFactory.getInsolvencyService();
        FilesService filesService = serviceFactory.getFilesService();
        ScrapingExecutor scrapingExecutor = new ScrapingExecutorImpl(startDate, endDate, wsEventsService, filesService, insolvencyService);
        scrapingExecutor.registerScraper(new CreditorProposalScraper());
        scrapingExecutor.registerScraper(new SubjectsScraper());
        scrapingExecutor.registerScraper(new InsolvencyStatesScraper());
        scrapingExecutor.execute();
        insolvencyService.cleanInsolvencyStates();
    }
}
