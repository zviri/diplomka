package isir.wscachescraper.scrapers.impl;

import isir.shared.dataaccess.dto.AttachedFile;
import isir.shared.dataaccess.dto.FileType;
import isir.shared.dataaccess.dto.WsEvent;
import isir.shared.isirpub001.IsirPub001Stub;
import isir.shared.utils.IsirStringUtils;
import isir.shared.utils.XMLUtils;
import isir.wscachescraper.scrapers.ScraperBase;
import org.apache.commons.lang3.StringUtils;

/**
 * Scraper for extracting and storing creditor proposals.
 */
public class CreditorProposalScraper extends ScraperBase {

    @Override
    public void scrape(WsEvent event) throws Exception{
        IsirPub001Stub.IsirPub001Data data = event.getEvent();
        String druhRoleVRizeni = XMLUtils.getElementValue("//osoba/druhRoleVRizeni", data.getPoznamka());
        String subjectName = StringUtils.normalizeSpace(XMLUtils.getElementValue("//osoba/nazevOsoby", data.getPoznamka()));
        if (IsirStringUtils.normalizedCompare("VĚŘIT-NAVR", druhRoleVRizeni)) {
            for (AttachedFile attachedFile : getFileService().getFilesByInsolvencyId(event.getInsolvencyId())) {
                FileType fileType = getFileService().getFileType(attachedFile.getFileTypeId());
                if (StringUtils.stripAccents(fileType.getDescription()).startsWith("Insolvencni navrh")) {
                    getFileService().save(attachedFile.setCreditor(subjectName));
                }
            }
        }

    }

    @Override
    public String getName() {
        return "CreditorProposalScraper";
    }
}
