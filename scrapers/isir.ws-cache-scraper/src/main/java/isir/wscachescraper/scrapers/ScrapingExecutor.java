package isir.wscachescraper.scrapers;

import java.util.List;

/**
 * Interface for the ws scrapers executor.
 */
public interface ScrapingExecutor {

    /**
     * Register new scraper.
     *
     * @param scraper the scraper
     */
    public void registerScraper(Scraper scraper);

    /**
     * Execute scrapers.
     *
     * @throws Exception the exception
     */
    public void execute() throws Exception;

    /**
     * Gets registered scrapers.
     *
     * @return the registered scrapers
     */
    List<Scraper> getRegisteredScrapers();
}
