package isir.wscachescraper.config;

import com.lexicalscope.jewel.cli.Option;

/**
 * Ws cache scraper command line parameters.
 */
public interface CommandLineParameters {

    /**
     * Gets task name.
     *
     * @return the task name
     */
    @Option(description = "Name of the task that should be started (scrapeToCache|runScrapers).")
    public String getTaskName();

    /**
     * For task: scrapeToCache. From which actionId scraping should start, by default the max actionId in the DB is used.
     *
     * @return the action start
     */
    @Option(description = "For task: scrapeToCache. From which actionId scraping should start, by default the max actionId in the DB is used.", defaultToNull = true)
    public Long getActionStart();

    /**
     * For task: scrapeToCache. To which actionId scraping should go, by default goes until there are anymore actionId's.
     *
     * @return the action end
     */
    @Option(description = "For task: scrapeToCache. To which actionId scraping should go, by default goes until there are anymore actionId's.", defaultToNull = true)
    public Long getActionEnd();

    /** For task: scrapeToCache. The politeness interval between downloads in milli seconds. Default:30_000.
     * @return
     */
    @Option(description = "For task: scrapeToCache. The politeness interval between downloads in milli seconds. Default:30_000.", defaultValue = "30000")
    public Long getIntervalBetweenDownloads();

    /**
     * For task: runScrapers. On which minimal date scrapers should be run. By default: day before yesterday.
     *
     * @return the start date in format: 31/12/2012
     */
    @Option(description = "For task: runScrapers. On which minimal date scrapers should be run. By default: day before yesterday. Format: 31/12/2012", pattern = "^[0-9]{2}/[0-9]{2}/[0-9]{4}$", defaultToNull = true)
    public String getStartDate();

    /**
     * For task: runScrapers. On which maximal date scrapers should be run. By default: runs until there are any more actions.
     *
     * @return the end date in format: 31/12/2012
     */
    @Option(description = "For task: runScrapers. On which maximal date scrapers should be run. By default: runs until there are any more actions. Format: 31/12/2012", pattern = "^[0-9]{2}/[0-9]{2}/[0-9]{4}$", defaultToNull = true)
    public String getEndDate();

    /**
     * Gets help.
     *
     * @return the help
     */
    @Option(helpRequest = true, description = "Display help")
    boolean getHelp();
}
