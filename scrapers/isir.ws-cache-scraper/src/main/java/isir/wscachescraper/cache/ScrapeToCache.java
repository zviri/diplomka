package isir.wscachescraper.cache;

import isir.shared.dataaccess.service.ServiceFactory;
import isir.shared.dataaccess.service.WsEventsService;
import isir.shared.isirpub001.IsirPub001ServiceImpl;
import isir.wscachescraper.config.CommandLineParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Runner class for scrapeToCache task.
 */
public class ScrapeToCache {

    private static Logger log = LoggerFactory.getLogger(ScrapeToCache.class);

    /**
     * Run scraperToCacheTask.
     *
     * @param commandLineParameters the command line parameters
     * @throws Exception the exception
     */
    public void run(CommandLineParameters commandLineParameters) throws Exception {
        WsEventsService wsEventsService = ServiceFactory.getInstance().getWsEventsService();
        Long actionStart = commandLineParameters.getActionStart();
        Long actionEnd = commandLineParameters.getActionEnd();
        actionEnd = actionEnd == null ? Long.MAX_VALUE : actionEnd;
        actionStart = actionStart == null ? wsEventsService.getMaxId() : actionStart;

        log.info("actionStart: {}", actionStart);
        log.info("actionEnd: {}", actionEnd);
        log.info("intervalBetweenDownloads: {}", commandLineParameters.getIntervalBetweenDownloads());
        IsirPub001ServiceImpl isirWebService = new IsirPub001ServiceImpl();
        WsCacheScraperImpl wsCacheScraper = new WsCacheScraperImpl(actionStart, actionEnd, isirWebService, wsEventsService);
        wsCacheScraper.setDayTimeRequestInterval(commandLineParameters.getIntervalBetweenDownloads());
        wsCacheScraper.setNightTimeRequestInterval(commandLineParameters.getIntervalBetweenDownloads());
        wsCacheScraper.scrape();
    }


}
