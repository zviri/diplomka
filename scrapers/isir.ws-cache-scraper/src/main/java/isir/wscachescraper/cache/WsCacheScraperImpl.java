package isir.wscachescraper.cache;

import isir.shared.dataaccess.dto.WsEvent;
import isir.shared.dataaccess.service.WsEventsService;
import isir.shared.isirpub001.IIsirPub001Service;
import isir.shared.isirpub001.IsirPub001Stub.IsirPub001Data;
import isir.shared.utils.TimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.sql.Timestamp;

import static isir.shared.utils.WsUtils.getInsolvencyId;

/**
 * Ws cache scraper implementation.
 */
public class WsCacheScraperImpl {

    private static Logger log = LoggerFactory.getLogger(WsCacheScraperImpl.class);

    private long actionIDStart;
    private long actionIDEnd;
    private long dayTimeRequestInterval;
    private long nightTimeRequestInterval;

    private IIsirPub001Service isirWebService;
    private WsEventsService wsEventsService;

    /**
     * Instantiates a new Ws cache scraper impl.
     *
     * @param actionIDStart the action iD start
     * @param actionIDEnd the action iD end
     * @param isirWebService the isir web service
     * @param wsEventsService the ws events service
     * @throws RemoteException the remote exception
     */
    public WsCacheScraperImpl(long actionIDStart, long actionIDEnd, IIsirPub001Service isirWebService, WsEventsService wsEventsService) throws RemoteException {
        this.actionIDStart = actionIDStart;
        this.actionIDEnd = actionIDEnd;
        this.isirWebService = isirWebService;
        this.wsEventsService = wsEventsService;
    }

    /**
     * Starts scraping.
     *
     * @throws Exception the exception
     */
    public void scrape() throws Exception {
        long actionIterator = actionIDStart;
        while (actionIterator <= actionIDEnd) {
            IsirPub001Data[] isirPub0012Response = isirWebService.getIsirPub0012(actionIterator);
            if (isirPub0012Response == null || isirPub0012Response.length == 0) {
                log.info("All events were processed!");
                return;
            }
            log.info("Processing actionID:" + actionIterator);
            for (IsirPub001Data isirPub001Data : isirPub0012Response) {
                actionIterator = isirPub001Data.getId();
                String insolvencyId = getInsolvencyId(isirPub001Data);
                WsEvent wsEvent = new WsEvent().setId(actionIterator)
                        .setInsolvencyId(insolvencyId)
                        .setTimestamp(new Timestamp(isirPub001Data.getCas().getTimeInMillis()))
                        .setEvent(isirPub001Data);
                wsEventsService.save(wsEvent);
                if (actionIterator > actionIDEnd) {
                    break;
                }
            }

            Thread.sleep(TimeUtils.isDayTime() ? dayTimeRequestInterval : nightTimeRequestInterval);
        }
    }

    /**
     * Sets day time request interval.
     *
     * @param dayTimeRequestInterval the day time request interval
     */
    public void setDayTimeRequestInterval(long dayTimeRequestInterval) {
        this.dayTimeRequestInterval = dayTimeRequestInterval;
    }

    /**
     * Sets night time request interval.
     *
     * @param nightTimeRequestInterval the night time request interval
     */
    public void setNightTimeRequestInterval(long nightTimeRequestInterval) {
        this.nightTimeRequestInterval = nightTimeRequestInterval;
    }
}
