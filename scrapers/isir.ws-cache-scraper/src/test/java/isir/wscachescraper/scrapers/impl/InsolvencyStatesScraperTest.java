package isir.wscachescraper.scrapers.impl;

import isir.shared.dataaccess.dto.InsolvencyState;
import isir.shared.dataaccess.dto.InsolvencyStateType;
import isir.shared.dataaccess.dto.WsEvent;
import isir.shared.dataaccess.service.InsolvencyService;
import isir.shared.isirpub001.IsirPub001Stub;
import org.easymock.Capture;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Calendar;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;

public class InsolvencyStatesScraperTest {

    @Test
    public void testScrape() throws Exception {
        InsolvencyStatesScraper insolvencyStatesScraper = new InsolvencyStatesScraper();
        InsolvencyService insolvencyService = createMock(InsolvencyService.class);
        insolvencyStatesScraper.setInsolvencyService(insolvencyService);

        Timestamp timestamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
        IsirPub001Stub.IsirPub001Data pub001Data = new IsirPub001Stub.IsirPub001Data();
        pub001Data.setPoznamka("<druhStavRizeni>ODDLUZENI</druhStavRizeni>");
        WsEvent wsEvent = new WsEvent().setId(1L)
                                       .setInsolvencyId("insolvencyId")
                                       .setTimestamp(timestamp)
                                       .setEvent(pub001Data);
        expect(insolvencyService.getInsolvencyStateType("ODDLUZENI")).andReturn(new InsolvencyStateType().setId(2L));
        Capture<InsolvencyState> insolvencyStateCapture = new Capture<>();
        insolvencyService.save(capture(insolvencyStateCapture));

        replay(insolvencyService);

        insolvencyStatesScraper.scrape(wsEvent);

        verify(insolvencyService);
        InsolvencyState capturedInsolvencyState = insolvencyStateCapture.getValue();

        assertEquals(capturedInsolvencyState.getInsolvencyID(), "insolvencyId");
        assertEquals(capturedInsolvencyState.getStateChangeTimestamp(), timestamp);
        assertEquals(capturedInsolvencyState.getState(), new Long(2));
        assertEquals(capturedInsolvencyState.getActionID(), new Long(1));
    }

    @Test
    public void testGetName() throws Exception {
        assertEquals(new InsolvencyStatesScraper().getName(), "InsolvencyStatesScraper");
    }
}
