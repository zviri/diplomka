package isir.wscachescraper.scrapers.impl;

import isir.shared.dataaccess.dto.WsEvent;
import isir.shared.dataaccess.service.FilesService;
import isir.shared.dataaccess.service.InsolvencyService;
import isir.shared.dataaccess.service.WsEventsService;
import isir.shared.isirpub001.IsirPub001Stub;
import isir.shared.utils.TimeUtils;
import isir.wscachescraper.scrapers.Scraper;
import org.easymock.Capture;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;

public class ScrapingExecutorImplTest {
    @Test
    public void testRegisterScraper() throws Exception {
        ScrapingExecutorImpl scrapingExecutor = new ScrapingExecutorImpl(Calendar.getInstance(), Calendar.getInstance(), createMock(WsEventsService.class), null, null);
        Scraper scraper = createMock(Scraper.class);
        String scraperName = "scraperName";
        expect(scraper.getName()).andReturn(scraperName);
        replay(scraper);

        scrapingExecutor.registerScraper(scraper);
        List<Scraper> scrapers = scrapingExecutor.getRegisteredScrapers();

        assertEquals(scrapers.size(), 1);
        assertEquals(scrapers.get(0).getName(), scraperName);
        verify(scraper);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetRegisteredScrapers() {
        ScrapingExecutorImpl scrapingExecutor = new ScrapingExecutorImpl(Calendar.getInstance(), Calendar.getInstance(), createMock(WsEventsService.class), null, null);
        Scraper scraper = createMock(Scraper.class);
        scrapingExecutor.registerScraper(scraper);

        List<Scraper> scrapers = scrapingExecutor.getRegisteredScrapers();
        scrapers.remove(0);
    }

    @Test
    public void testExecute() throws Exception {
        Calendar startDateEndDate = Calendar.getInstance();
        WsEventsService wsEventsService = createMock(WsEventsService.class);
        Scraper scraper = createMock(Scraper.class);
        final Long eventId = 0L;
        String documentId = "documentId";
        String oddil = "oddil";
        String typ = "typ";
        String typText = "typText";
        String spisZnacka = "INS 2407/2008";
        String poznamka = "<idOsobyPuvodce>KSJIMBM</idOsobyPuvodce>";
        int poradiVOddilu = 11;

        IsirPub001Stub.IsirPub001Data isirPub001Data = new IsirPub001Stub.IsirPub001Data();
        isirPub001Data.setId(eventId);
        Calendar timestamp = Calendar.getInstance();
        timestamp.setTimeInMillis(1000);
        isirPub001Data.setCas(timestamp);
        isirPub001Data.setIdDokument(documentId);
        isirPub001Data.setOddil(oddil);
        isirPub001Data.setPoradiVOddilu(poradiVOddilu);
        isirPub001Data.setTyp(typ);
        isirPub001Data.setTypText(typText);
        isirPub001Data.setSpisZnacka(spisZnacka);
        isirPub001Data.setPoznamka(poznamka);
        final WsEvent event = new WsEvent().setId(eventId)
                                           .setEvent(isirPub001Data)
                                           .setTimestamp(TimeUtils.calendarToTimestamp(timestamp))
                                           .setInsolvencyId("insolvencyId");

        final ScrapingExecutorImpl scrapingExecutor = new ScrapingExecutorImpl(startDateEndDate, startDateEndDate, wsEventsService, null, null);
        scrapingExecutor.registerScraper(scraper);
        expect(wsEventsService.getEventIds(startDateEndDate, startDateEndDate)).andReturn(new ArrayList<Long>() {{ add(eventId); }});
        expect(wsEventsService.getEvent(eventId)).andReturn(event);
        Capture<WsEvent> wsEventCapture = new Capture<>();
        scraper.setFileService(anyObject(FilesService.class));
        scraper.setInsolvencyService(anyObject(InsolvencyService.class));
        scraper.scrape(capture(wsEventCapture));
        replay(wsEventsService, scraper);

        scrapingExecutor.execute();

        WsEvent capturedWsEvent = wsEventCapture.getValue();
        assertEquals(capturedWsEvent.getId(), eventId);
        assertEquals(capturedWsEvent.getInsolvencyId(), "insolvencyId");
        assertEquals(capturedWsEvent.getTimestamp().getTime(), 1000);
        IsirPub001Stub.IsirPub001Data data = capturedWsEvent.getEvent();
        assertEquals(data.getCas().getTimeInMillis(), 1000);
        assertEquals((Long) data.getId(), eventId);
        assertEquals(data.getIdDokument(), documentId);
        assertEquals(data.getOddil(), oddil);
        assertEquals(data.getPoradiVOddilu(), poradiVOddilu);
        assertEquals(data.getPoznamka(), poznamka);
        assertEquals(data.getSpisZnacka(), spisZnacka);
        assertEquals(data.getTyp(), typ);
        assertEquals(data.getTypText(), typText);

        verify(wsEventsService, scraper);

    }
}
