package isir.wscachescraper.scrapers.impl;

import isir.shared.dataaccess.dto.AttachedFile;
import isir.shared.dataaccess.dto.FileType;
import isir.shared.dataaccess.dto.WsEvent;
import isir.shared.dataaccess.service.FilesService;
import isir.shared.isirpub001.IsirPub001Stub;
import org.easymock.Capture;
import org.junit.Test;

import java.util.ArrayList;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;

public class CreditorProposalScraperTest {

    @Test
    public void testScrape() throws Exception {
        FilesService filesService = createMock(FilesService.class);
        CreditorProposalScraper creditorProposalScraper = new CreditorProposalScraper();
        creditorProposalScraper.setFileService(filesService);


        String poznamka = "<udalost >" +
                "        <osoba>" +
                "            <nazevOsoby>COFIDIS s. r. o.</nazevOsoby>" +
                "            <ic>27179907</ic>" +
                "            <druhRoleVRizeni>VĚŘIT-NAVR</druhRoleVRizeni>" +
                "        </osoba>" +
                "    </udalost>";
        IsirPub001Stub.IsirPub001Data isirPub001Data = new IsirPub001Stub.IsirPub001Data();
        isirPub001Data.setPoznamka(poznamka);
        WsEvent wsEvent = new WsEvent().setEvent(isirPub001Data)
                                       .setInsolvencyId("insolvencyId");
        final AttachedFile attachedFile = new AttachedFile().setId(1L)
                                                      .setFileTypeId(2L);
        expect(filesService.getFilesByInsolvencyId(wsEvent.getInsolvencyId())).andReturn(new ArrayList<AttachedFile>() {{ add(attachedFile); }});
        expect(filesService.getFileType(attachedFile.getFileTypeId())).andReturn(new FileType().setDescription("Insolvenční návrh"));
        Capture<AttachedFile> attachedFileCapture = new Capture<>();
        filesService.save(capture(attachedFileCapture));
        replay(filesService);

        creditorProposalScraper.scrape(wsEvent);

        verify(filesService);
        AttachedFile capturedFile = attachedFileCapture.getValue();
        assertEquals(capturedFile.getId(), new Long(1));
        assertEquals(capturedFile.getFileTypeId(), new Long(2));
        assertEquals(capturedFile.getCreditor(), "COFIDIS s. r. o.");
    }

    @Test
    public void testGetName() throws Exception {
        assertEquals(new CreditorProposalScraper().getName(), "CreditorProposalScraper");
    }
}
