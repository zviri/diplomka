package isir.wscachescraper.scrapers.impl;

import isir.shared.dataaccess.dto.Subject;
import isir.shared.dataaccess.dto.WsEvent;
import isir.shared.dataaccess.service.FilesService;
import isir.shared.dataaccess.service.InsolvencyService;
import isir.shared.isirpub001.IsirPub001Stub;
import org.easymock.Capture;
import org.junit.Test;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;

public class IcoScraperTest {

    @Test
    public void testScrape() throws Exception {
        InsolvencyService insolvencyService = createMock(InsolvencyService.class);
        SubjectsScraper subjectsScraper = new SubjectsScraper();
        subjectsScraper.setInsolvencyService(insolvencyService);

        String poznamka = "<udalost >" +
                     "        <osoba>" +
                     "            <nazevOsoby>COFIDIS s. r. o.</nazevOsoby>" +
                     "            <ic>27179907</ic>" +
                     "            <druhOsoby>F</druhOsoby>" +
                     "            <druhPravniForma>a.s.</druhPravniForma>" +
                     "        </osoba>" +
                     "    </udalost>";
        IsirPub001Stub.IsirPub001Data isirPub001Data = new IsirPub001Stub.IsirPub001Data();
        isirPub001Data.setPoznamka(poznamka);
        WsEvent wsEvent = new WsEvent().setEvent(isirPub001Data);
        Capture<Subject> subjectCapture = new Capture<>();
        insolvencyService.save(capture(subjectCapture));

        replay(insolvencyService);

        subjectsScraper.scrape(wsEvent);

        verify(insolvencyService);
        Subject capturedSubject = subjectCapture.getValue();
        assertEquals(capturedSubject.getName(), "COFIDIS s. r. o.");
        assertEquals(capturedSubject.getIco(), "27179907");
        assertEquals(capturedSubject.getForm(), "F");
        assertEquals(capturedSubject.getLegalForm(), "a.s.");
    }

    @Test
    public void testScrape2() throws Exception {
        FilesService filesService = createMock(FilesService.class);
        SubjectsScraper icoScraper = new SubjectsScraper();
        icoScraper.setFileService(filesService);

        String poznamka = "<udalost ></udalost>";
        IsirPub001Stub.IsirPub001Data isirPub001Data = new IsirPub001Stub.IsirPub001Data();
        isirPub001Data.setPoznamka(poznamka);
        WsEvent wsEvent = new WsEvent().setEvent(isirPub001Data);
        replay(filesService);

        icoScraper.scrape(wsEvent);

        verify(filesService);
    }

    @Test
    public void testGetName() throws Exception {
        assertEquals(new SubjectsScraper().getName(), "SubjectsScraper");
    }
}
