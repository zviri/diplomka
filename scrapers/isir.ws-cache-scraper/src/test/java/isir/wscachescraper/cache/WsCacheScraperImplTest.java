package isir.wscachescraper.cache;

import isir.shared.dataaccess.dto.WsEvent;
import isir.shared.dataaccess.service.WsEventsService;
import isir.shared.isirpub001.IIsirPub001Service;
import isir.shared.isirpub001.IsirPub001Stub;
import isir.shared.utils.TimeUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.easymock.Capture;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Calendar;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.easymock.EasyMock.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({TimeUtils.class})
public class WsCacheScraperImplTest {

    @Test
    public void testScrape1() throws Exception {
        Long eventId = 0L;
        String documentId = "documentId";
        String oddil = "oddil";
        String typ = "typ";
        String typText = "typText";
        String spisZnacka = "INS 2407/2008";
        String poznamka = "<idOsobyPuvodce>KSJIMBM</idOsobyPuvodce>";
        int poradiVOddilu = 11;

        WsEventsService wsEventsService = createMock(WsEventsService.class);
        IIsirPub001Service isirPub001Service = createNiceMock(IIsirPub001Service.class);
        WsCacheScraperImpl wsCacheScraper = new WsCacheScraperImpl(0, 1, isirPub001Service, wsEventsService);
        IsirPub001Stub.IsirPub001Data isirPub001Data = new IsirPub001Stub.IsirPub001Data();
        isirPub001Data.setId(eventId);
        Calendar timestamp = Calendar.getInstance();
        timestamp.setTimeInMillis(1000);
        isirPub001Data.setCas(timestamp);
        isirPub001Data.setIdDokument(documentId);
        isirPub001Data.setOddil(oddil);
        isirPub001Data.setPoradiVOddilu(poradiVOddilu);
        isirPub001Data.setTyp(typ);
        isirPub001Data.setTypText(typText);
        isirPub001Data.setSpisZnacka(spisZnacka);
        isirPub001Data.setPoznamka(poznamka);
        expect(isirPub001Service.getIsirPub0012(eventId)).andReturn(new IsirPub001Stub.IsirPub001Data[]{isirPub001Data});
        Capture<WsEvent> wsEventCapture = new Capture<>();
        wsEventsService.save(capture(wsEventCapture));
        replay(isirPub001Service, wsEventsService);

        wsCacheScraper.scrape();

        verify(isirPub001Service, wsEventsService);
        WsEvent wsEvent = wsEventCapture.getValue();
        assertEquals(wsEvent.getId(), eventId);
        assertEquals(wsEvent.getInsolvencyId(), "ksbrins2407/2008");
        assertEquals(wsEvent.getTimestamp().getTime(), 1000);
        IsirPub001Stub.IsirPub001Data data = wsEvent.getEvent();
        assertEquals(data.getCas().getTimeInMillis(), 1000);
        assertEquals((Long) data.getId(), eventId);
        assertEquals(data.getIdDokument(), documentId);
        assertEquals(data.getOddil(), oddil);
        assertEquals(data.getPoradiVOddilu(), poradiVOddilu);
        assertEquals(data.getPoznamka(), poznamka);
        assertEquals(data.getSpisZnacka(), spisZnacka);
        assertEquals(data.getTyp(), typ);
        assertEquals(data.getTypText(), typText);
    }

    @Test
    public void testDayTimeScrape() throws Exception {
        PowerMock.mockStatic(TimeUtils.class);
        expect(TimeUtils.isDayTime()).andReturn(true);
        PowerMock.replay(TimeUtils.class);
        IIsirPub001Service isirPub001Service = createNiceMock(IIsirPub001Service.class);
        WsEventsService wsEventsService = createMock(WsEventsService.class);
        WsCacheScraperImpl wsCacheScraper = new WsCacheScraperImpl(0, 1, isirPub001Service, wsEventsService);
        int dayTimeRequestInterval = 1_000;
        wsCacheScraper.setDayTimeRequestInterval(dayTimeRequestInterval);
        IsirPub001Stub.IsirPub001Data isirPub001Data = new IsirPub001Stub.IsirPub001Data();
        isirPub001Data.setId(0);
        isirPub001Data.setSpisZnacka("INS 2407/2008");
        isirPub001Data.setPoznamka("<idOsobyPuvodce>KSJIMBM</idOsobyPuvodce>");
        isirPub001Data.setCas(Calendar.getInstance());
        expect(isirPub001Service.getIsirPub0012(0)).andReturn(new IsirPub001Stub.IsirPub001Data[]{isirPub001Data});
        replay(isirPub001Service);

        StopWatch watch = new StopWatch();
        watch.start();
        wsCacheScraper.scrape();
        watch.stop();

        assertTrue(watch.getTime() >= 1000);
    }

    @Test
    public void testNightTimeScrape() throws Exception {
        PowerMock.mockStatic(TimeUtils.class);
        expect(TimeUtils.isDayTime()).andReturn(false);
        PowerMock.replay(TimeUtils.class);
        IIsirPub001Service isirPub001Service = createNiceMock(IIsirPub001Service.class);
        WsEventsService wsEventsService = createMock(WsEventsService.class);
        WsCacheScraperImpl wsCacheScraper = new WsCacheScraperImpl(0, 1, isirPub001Service, wsEventsService);
        int nightTimeRequestInterval = 1_000;
        wsCacheScraper.setNightTimeRequestInterval(nightTimeRequestInterval);
        IsirPub001Stub.IsirPub001Data isirPub001Data = new IsirPub001Stub.IsirPub001Data();
        isirPub001Data.setId(0);
        isirPub001Data.setSpisZnacka("INS 2407/2008");
        isirPub001Data.setPoznamka("<idOsobyPuvodce>KSJIMBM</idOsobyPuvodce>");
        isirPub001Data.setCas(Calendar.getInstance());
        expect(isirPub001Service.getIsirPub0012(0)).andReturn(new IsirPub001Stub.IsirPub001Data[]{isirPub001Data});
        replay(isirPub001Service);

        StopWatch watch = new StopWatch();
        watch.start();
        wsCacheScraper.scrape();
        watch.stop();

        assertTrue(watch.getTime() >= nightTimeRequestInterval);
    }
}
